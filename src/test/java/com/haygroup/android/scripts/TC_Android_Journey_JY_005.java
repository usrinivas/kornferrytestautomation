package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.JourneyAndroidLib;

public class TC_Android_Journey_JY_005 extends JourneyAndroidLib{


	@DataProvider
	public Object[][] getTestDataFor_Journey() {

		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}

	@Test(dataProvider = "getTestDataFor_Journey")
	public void Journey005(Hashtable<String, String> data){
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Priorities");				
				loginToJourneyMobileApplication(data.get("UserName"), data.get("Password"));				
				clickGetStartedButton();			
				swipeUpscreen();			
				wipeUserdata();			
				loginToJourneyMobileApplication(data.get("UserName"), data.get("Password"));			
				clickGetStartedButton();			
				swipe("android:id/list","Launch Priorities");			
				launchPriorities();			
				enterTask(data.get("Task1"), data.get("Task2"));			
				swipe("com.haygroup.journey.android:id/viewPrioritiesPage1","Next");			
				tapNext();			
				tapRatingForEachTask(3);			
				tapRatingForEachTask(7);			
				tapNextPriorities();			
				tapConfirm();			
				tapRatingForEachTask(4);			
				tapRatingForEachTask(8);			
				tapNextOnPriorities();			
				tapConfirm();			
				swipePrioritesScreen("com.haygroup.journey.android:id/widgetResultsGrid","Low priority/Enjoy doing");			
				priorityDone();			
				enterEmailAddress(data.get("Email"));			
				sendMail();			
				clickSettingsButton();

			}

		}
		catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void  logOutFunc()throws Throwable {

		//Logout from the application
		logout();

	}
}
