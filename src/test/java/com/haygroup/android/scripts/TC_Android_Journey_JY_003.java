package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.JourneyAndroidLib;

public class TC_Android_Journey_JY_003 extends JourneyAndroidLib{

	@DataProvider
	public Object[][] getTestDataFor_Journey() {

		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}

	@Test(dataProvider = "getTestDataFor_Journey")
	public void Journey003(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Start Survey");	
				/* Login to the application */
				loginToJourneyMobileApplication(data.get("UserName").trim(), data.get("Password").trim());
				clickSettings();
				swipeUpscreen();
				wipeUserdata();
				loginToJourneyMobileApplication(data.get("UserName").trim(), data.get("Password").trim());
				clickSettings();
				Launchstart();            
				clickstart();
				scrollDownToElement("com.haygroup.journey.android:id/viewSelfAssessmentPart2Page1","Impulse control");
				clickstart();             
				scrollDownToElement("com.haygroup.journey.android:id/viewSelfAssessmentPart2Page1","Understanding what matters to others");
				clickstart();
				scrollDownToElement("com.haygroup.journey.android:id/viewSelfAssessmentPart2Page1","Understanding team norms and unspoken expectations");
				clickstart();            
				scrollDownToElement("com.haygroup.journey.android:id/viewSelfAssessmentPart2Page1","Next");
				clickstart();        
				clicknext();            
				clickconfirm();
				clickstart();
				scrollDownToElement("com.haygroup.journey.android:id/viewSelfAssessmentPart2Page2","Impulse control");
				clickstart();             
				scrollDownToElement("com.haygroup.journey.android:id/viewSelfAssessmentPart2Page2","Understanding what matters to others");
				clickstart();
				scrollDownToElement("com.haygroup.journey.android:id/viewSelfAssessmentPart2Page2","Understanding team norms and unspoken expectations");
				clickstart();          
				scrollDownToElement("com.haygroup.journey.android:id/viewSelfAssessmentPart2Page2","Done");
				clickstart();            
				clickDone();      
				confirm();
				Thread.sleep(5000);           
				clickSettingsButton();

			}
		}

		//throw new RuntimeException(e);
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 

		logout();

	}
}



