package com.haygroup.android.scripts;

import java.util.Hashtable;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.JourneyAndroidLib;

public class TC_Android_Journey_JY_002 extends JourneyAndroidLib{

	@DataProvider
	public Object[][] getTestDataFor_Journey() {
		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}

	@Test(dataProvider = "getTestDataFor_Journey")

	public void Journey002Test(Hashtable<String, String> data) {

		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Friendly Observations");	
				/* Login to the application */
				loginToJourneyMobileApplication(data.get("UserName").trim(), data.get("Password").trim());
				clickSettings();
				swipeUpscreenFriendly();
				userFriendlyData(data.get("Meeting name").trim(),data.get("userfriendlyemail").trim());


			}
		}

		catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		logout();

	}
}



