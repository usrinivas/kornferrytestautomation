package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.StylesClimateAndroidLib;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.page.StylesClimatePageAndroid;

public class TC_Android_StylesClimate_003 extends StylesClimateAndroidLib{

	@DataProvider
	public Object[][] getTestDataFor_StylesAndClimate() {
		return TestUtil.getData("Create_Styles_And_Climate", TestData, "StylesAndClimate");
	}

	@Test(dataProvider = "getTestDataFor_StylesAndClimate")
	public void styleandClimate003(Hashtable<String, String> data) {

		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Complete and edit tips");	


				loginToMobileApplication(data.get("UserNameMobile").trim(), data.get("PasswordMobile").trim());

				clearScreenHideOnHomePage();

				openStylesAndClimate();

				removeScreenHider(StylesClimatePageAndroid.levelUpPageHideOne);

				//Click on Scan QR code button
				scrollDownToElement("com.haygroup.activate:id/scrollView", "OR CREATE AN ACTION PLAN");

				//Remove hiding screen
				removeScreenHider(StylesClimatePageAndroid.levelUpPageHideTwo);

				//Click Scan QR Code
				clickScanQRCode();

				clickEnterFeedBackManuallyButton();

				enterUinqueCode(data.get("UniqueCode").trim());

				scrollDownToElement("com.haygroup.activate:id/scrollView","Continue");

				clickContinueButton();

				clickDesignMyPlanButton();
				//clickCreatePlanForMe();
				clickDesignMyOwnPlanButton();		
				selectFiveTips();
				selectfourtips();
				clickContinueButton();            
				
				Waittime();
				tap();
				Waittime();
				scrollUp(2);
				selectAllDetails();
				openOverLayMenu();
				mobileSettingsPageButton();
				Waittime();

			}
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutFromApplication() throws Throwable {
		//Logout from the application 
		logout();
	}
}


