package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.EcoSystemAndroidLib;

public class TC_Android_EcoSystem_ECO_003 extends EcoSystemAndroidLib {


	@DataProvider
	public Object[][] getTestDataFor_EcoSystem() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}


	@Test(dataProvider = "getTestDataFor_EcoSystem")
	public void ecoSystem003(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Contact Administrator");	
				/* Login to the application */
				loginToMobileApplication(data.get("UserNameMobile").trim(), data.get("PasswordMobile").trim());
				clearhomepage();
				openOverLayMenu();
				mobileSettingsPageButton();
				contactAdministrator(data.get("Enter your message"));				
			}

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 

		logout();


	}
}



