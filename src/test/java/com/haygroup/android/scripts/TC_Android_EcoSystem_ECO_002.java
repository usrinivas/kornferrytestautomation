package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.EcoSystemAndroidLib;


public class TC_Android_EcoSystem_ECO_002 extends EcoSystemAndroidLib {

	@DataProvider
	public Object[][] getTestDataFor_EcoSystem() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_EcoSystem")
	public void ecoSystem002(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Edit profile");	
				/* Login to the application */
				loginToMobileApplication(data.get("UserNameMobile").trim(), data.get("PasswordMobile").trim());
				clearhomepage();

				/*Open OverLay Menu*/
				openOverLayMenu();

				/*click on settings*/
				mobileSettingsPageButton();
				homeLocation();
				changeLocation(data.get("Location"));
				swipe("android:id/list","User Preferred Currency");
				userPreferredCurrency(data.get("Currency").trim());
				userPreferredTimeZone(data.get("Timezone").trim());
				userpreferencetoreceiveemailnotifications(data.get("Emailnotification").trim());
				userpreferenceforActionPlanreminderEmailNotifications(data.get("Actionreminernoficiation").trim());
				swipe("android:id/list","My Plan Preference for Week Ending Day");
				MyPlanPreferenceforWeekStartingDay(data.get("WeekStartingDay").trim());
				MyplanpreferenceforWeekEndingDay(data.get("WeekEndingDay").trim());
				swipe("android:id/list","My Plan Preference for Team Meeting Day");
				MyplanpreferencesforteammeetingDay(data.get("MeetingDay").trim());
				swipe("android:id/list","Team Meeting Time");
				Teammeetingtime();				
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 

		logout();


	}
}


