package com.haygroup.android.scripts;

import java.util.Hashtable;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.page.StylesClimatePageAndroid;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.ActivateMobileAndroidLib;

public class TC_Android_ActivateMobile_ACT_001 extends ActivateMobileAndroidLib{

	@DataProvider
	public Object[][] getTestDataFor_ActivateMobile() {
		return TestUtil.getData("ActivateMobile", TestData, "ActivateMobile");
	}

	@Test(dataProvider = "getTestDataFor_ActivateMobile")
	public void activateMobile001Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Scan QR code");	

				loginToMobileApplication(data.get("UserNameMobile").trim(), data.get("PasswordMobile").trim());

				//Remove hiding screen
				clearScreenHideOnHomePage();		 			
				//Click on Styles&Climate Tab
				openStylesAndClimate();
				//Remove hiding screen
				removeScreenHider(ActivateMobilePageAndroid.tblLevelUpPageHideOne);							
				//Verify user is on LevelUp Screen 	
				//Click on Scan QR code button
				scrollDownToElement("com.haygroup.activate:id/scrollView", "Scan QR Code");				
				//Remove hiding screen
				removeScreenHider(ActivateMobilePageAndroid.tblLevelUpPageHideTwo);				
				//Click Scan QR Code
				clickScanQRCode();	
				openQRcode();
				Thread.sleep(5000);
				quitQRcode();
				scrollDownToElement("com.haygroup.activate:id/scrollView","Continue");			
				clickContinueButton();
				clickDesignMyPlanButton();
				clickCreatePlanForMe();
				removeScreenHider(StylesClimatePageAndroid.levelUpPageHidefour);
				clickOneOneContinueButton();
				clickContinueButton();
				clickContinueButton();
				Myplanpage();
				openOverLayMenu();
				mobileSettingsPageButton();

			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutFromApplication() throws Throwable {
		//Logout from the application 
		logout();

	}
}
