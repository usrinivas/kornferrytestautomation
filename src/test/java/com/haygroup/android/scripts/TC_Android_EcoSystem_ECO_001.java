package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.EcoSystemAndroidLib;
import com.haygroup.android.page.EcoSystemPageAndroid;
import com.haygroup.web.page.EcoSystemPage;

public class TC_Android_EcoSystem_ECO_001 extends EcoSystemAndroidLib {

	@DataProvider
	public Object[][] getTestDataFor_EcoSystem() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_EcoSystem")
	public void ecoSystem001(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Languagechange");	

				/* Login to the application */
				loginToMobileApplication(data.get("UserNameMobile").trim(), data.get("PasswordMobile").trim());
				clearhomepage();
				/*Open OverLay Menu*/
				openOverLayMenu();
				mobileSettingsPageButton();
				/*click on settings*/
				languageChange_mobile(data.get("LanguageMobile").trim(),data.get("DefaultLanguage").trim());
				loginToMobileApplication(data.get("UserNameMobile").trim(), data.get("PasswordMobile").trim());
				openOverLayMenu();
				mobileSettingsPageButton();
				languagechangeforChina_mobile(data.get("LanguageMobile").trim(),data.get("DefaultLanguage").trim());

			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@AfterMethod
	public void logoutApplication() throws Throwable {

		//logOutMobileApplication();
	}
}


