package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.JourneyAndroidLib;

public class TC_Android_Journey_JY_006 extends JourneyAndroidLib{

	@DataProvider
	public Object[][] getTestDataFor_Journey() {
		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}

	@Test(dataProvider = "getTestDataFor_Journey")
	public void Journey006(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Fly on the wall");	
				loginToJourneyMobileApplication(data.get("UserName").trim(), data.get("Password").trim());
				clickSettings();
				swipeUpscreen();
				wipeUserdata();
				loginToJourneyMobileApplication(data.get("UserName").trim(), data.get("Password").trim());
				clickSettings();
				swipeUpscreenFlyonthewall();
				playvideo();				
				clickonnextbutton();
				rating();
				swipe("com.haygroup.journey.android:id/viewFlyOnTheWallPage2","Put the needs of the team ahead of your own needs");
				rating();
				swipe("com.haygroup.journey.android:id/viewFlyOnTheWallPage2","Next");				 
				rating();
				clickNext();
				clickonconfirmbutton();
				emailaddressSend(data.get("Email_id_1"),data.get("Email_id_2"),data.get("Email_id_3"));
				swipe("com.haygroup.journey.android:id/viewFlyOnTheWallPage3","Done");				 
				clickdonebutton();
				clickSettingsButton();


			}
		}					
		catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 

		logout();
	}
}



