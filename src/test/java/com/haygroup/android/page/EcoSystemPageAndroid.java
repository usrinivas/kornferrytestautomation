package com.haygroup.android.page;

import org.openqa.selenium.By;

public class EcoSystemPageAndroid {

	public static By btnMobileSettings;
	public static By btnBack;
	public static By lnkOverLayMenu;
	public static By txtHomelocation;
	public static By txtEditlocation;
	public static By ddnSelectPreferedLanguage;
	public static By ddnselectPreferedCurrency;
	public static By btnOk;
	public static By ddnSelectPreferredTimezone;
	public static By ddnActionPlanremindersByEmailnotifications;
	public static By ddnMyPlanPreferenceforWeekStartingDay;
	public static By ddnMyplanpreferenceforWeekEndingDay;
	public static By ddnMyplanpreferencesforteammeetingDay;
	public static By Teammeetingtime;
	public static By lnkLogout;
	public static By btnPopLogout;
	public static By txtClickmylocation;
	public static By sendmenotifications;
	public static By img_Home;
	public static By lblPreferenceTeammeetingtime;	
	public static By userpreferencetoreceiveemailnotifications;
	public static By btnWeekEndingDayOk;
	public static By currentLocation;
	public static By selectCurrency;
	public static By selectTimezone;
	public static By selectemailnotifications;
	public static By selectActionPlanreminders;
	public static By selectWeekStaringDay;
	public static By selectWeekEndingDay;	
	public static By lnkContactAdmin;
	public static By chkAdminSelect;
	public static By btnNext;
	public static By txtEnter;
	public static By btnSend;
	public static By lnktouch;
	public static By logout;
	public static By logoutbutton;
	public static By languageIcon;
	public static By homeText;
	public static By btnOkPopup;

	static {

		btnMobileSettings=By.xpath("//android.widget.RelativeLayout[7]/android.widget.LinearLayout/android.widget.TextView");
		btnBack = By.xpath("//android.widget.ImageButton[contains(@content-desc,'Navigate up')]");
		lnkOverLayMenu=By.className("android.widget.ImageButton");
		img_Home=By.className("android.widget.ImageView");
		ddnSelectPreferedLanguage = By.name("User Preferred Language");
		btnOk = By.id("android:id/button1");
		txtHomelocation = By.xpath("//android.widget.RelativeLayout[1]/android.widget.TextView");
		txtEditlocation=By.className("android.widget.EditText");
		ddnselectPreferedCurrency=By.xpath("//android.widget.TextView[contains(@text,'User Preferred Currency')]");
		ddnSelectPreferredTimezone=By.xpath("//android.widget.TextView[contains(@text,'User Preferred Time Zone')]");
		userpreferencetoreceiveemailnotifications=By.xpath("//android.widget.TextView[contains(@text,'User preference to receive email notifications')]");
		ddnActionPlanremindersByEmailnotifications =By.xpath("//android.widget.TextView[contains(@text,'User Preference for Action Plan Reminders by Email Notifications')]");
		sendmenotifications=By.xpath("//android.widget.TextView[contains(@text,'Send me notifications on a weekly basis')]");
		ddnMyPlanPreferenceforWeekStartingDay=By.xpath("//android.widget.TextView[contains(@text,'My Plan Preference for Week Starting Day')]");	
		ddnMyplanpreferenceforWeekEndingDay=By.xpath("//android.widget.TextView[contains(@text,'My Plan Preference for Week Ending Day')]");
		btnWeekEndingDayOk=By.xpath("//android.widget.Button[contains(@resource-id,'com.haygroup.activate:id/btnOk')]");
		ddnMyplanpreferencesforteammeetingDay=By.xpath("//android.widget.TextView[contains(@text,'My Plan Preference for Team Meeting Day')]");
		Teammeetingtime=By.xpath("//android.widget.TextView[contains(@text,'Team Meeting Time')]");
		lblPreferenceTeammeetingtime=By.id("android:id/button1");				
		txtClickmylocation=By.xpath("//android.widget.ListView/android.widget.LinearLayout/android.widget.TextView");
		lnkLogout=By.xpath("//android.widget.TextView[contains(@text,'Log Out')]");
		btnPopLogout=By.xpath("//android.widget.Button[contains(@text,'Log Out')]");
		lnkContactAdmin=By.xpath("//android.widget.TextView[contains(@text,'Contact')]");
		chkAdminSelect=By.xpath("//android.widget.CheckedTextView[contains(@text,'Auto')]");
		btnNext=By.id("android:id/button1");
		txtEnter=By.id("com.haygroup.activate:id/adminMessage");
		btnSend=By.id("android:id/button1");
		lnktouch=By.id("com.haygroup.activate:id/rowIcon");
		logout=By.xpath("//android.widget.TextView[contains(@text,'Log Out')]");
		logoutbutton=By.id("android:id/button1");
		btnOkPopup=By.id("com.haygroup.activate:id/btnOk");

	}


	public static By inputCurrency(String currency){
		return selectCurrency = By.xpath("//android.widget.TextView[@text='"+currency+"']");
	}
	public static By inputTimezone(String timezone){
		return selectTimezone = By.xpath("//android.widget.TextView[@text='"+timezone+"']");
	}
	public static By inputemailnotifications (String emailnotifications)
	{
		return selectemailnotifications=By.xpath("//android.widget.TextView[@text='"+emailnotifications+"']");

	}
	public static By inputActionPlanreminder (String emailnotifications)
	{
		return selectActionPlanreminders=By.xpath("//android.widget.TextView[@text='"+emailnotifications+"']");	
	}
	public static By inputWeekStaringDay (String WeekStaringDay)
	{
		return selectWeekStaringDay=By.xpath("//android.widget.TextView[@text='"+WeekStaringDay+"']");		
	}
	public static By inputWeekEndingDay (String WeekEndingDay)
	{
		return selectWeekEndingDay=By.xpath("//android.widget.TextView[@text='"+WeekEndingDay+"']");	
	}

	public static By inputTeammeeting (String Teammeeting)
	{
		return selectWeekEndingDay=By.xpath("//android.widget.TextView[@text='"+Teammeeting+"']");	
	}

	public static By inputLanguage(String languagechange)
	{
		return languageIcon = By.xpath("//android.widget.TextView[@text='"+languagechange+"']");
	}

}
