package com.haygroup.android.page;

import org.openqa.selenium.By;

public class ActivateMobilePageAndroid {

	public static By chkChinaMobile;
	public static By txtUser_Mobile;
	public static By tabStylesAndClimate;
	public static By lblYourRangeofStyles;
	public static By tblHomePageGridView;
	public static String txtScanQRCode;
	public static By tblLevelUpPageHideOne;
	public static By swipToScanQRCode;
	public static By tblLevelUpPageHideTwo;
	public static By btnScanCodeOK;
	public static By btnContinue;
	public static By btnCreatePlanForMe;
	public static By btnDesignMyPlan;
	public static By btnDesignMyOwnPlan;
	public static By chkVisionaryStyleFirst;
	public static By chkVisionaryStyleSecond;
	public static By lblMyPlan;
	public static By lblOneOnOne;
	public static By lblteam;
	public static By lbltxtoneTime;
	public static By lblroadMap;


	static
	{

		chkChinaMobile             = By.id("com.haygroup.activate:id/cbUseChinaServer");
		txtUser_Mobile             = By.id("com.haygroup.activate:id/account_header_drawer_email");
		tabStylesAndClimate        = By.xpath("//android.widget.AbsListView/android.widget.LinearLayout[1]");
		lblYourRangeofStyles       = By.id("com.haygroup.activate:id/yourRange_tv");
		tblHomePageGridView        = By.id("com.haygroup.activate:id/grid_view");
		swipToScanQRCode           = By.id("com.haygroup.activate:id/btnScan");
		txtScanQRCode              = "Scan QR Code";
		tblLevelUpPageHideOne      = By.id("com.haygroup.activate:id/llLevelUp");
		tblLevelUpPageHideTwo      = By.className("android.widget.LinearLayout");
		btnScanCodeOK              = By.id("android:id/button1");
		btnContinue                = By.id("com.haygroup.activate:id/btnContinue");
		btnCreatePlanForMe         = By.id("com.haygroup.activate:id/btnCreateMyPlan");
		btnDesignMyPlan            = By.id("com.haygroup.activate:id/designMyPlan");
		lblMyPlan  				   = By.xpath("//android.widget.TextView[contains(@text,'My Plan')]");
		lblOneOnOne                = By.xpath("//android.widget.TextView[@text='One-on-One (5/5)']");
		lblteam                    = By.xpath("//android.widget.TextView[@text='Team (4/4)']");
		lbltxtoneTime              = By.xpath("//android.widget.TextView[@text='One-Time (0/0)']");
		lblroadMap                 = By.xpath("//android.widget.TextView[@text='Road Map']");
	}
	public static By inputUser(String user){

		return txtUser_Mobile = By.name(user);
	}
}
