package com.haygroup.android.page;

import org.openqa.selenium.By;

public class JourneyPageAndroid {

	public static By preferencesIcon;
	public static By settingsBtn;
	public static By selectMyTitle;
	public static By myTitleTextBox;
	public static By okBtn;
	public static By currentTitle;
	public static By getStrartedBtn;
	public static By logOutJourney;
	public static By selectPreferedLanguage;
	public static By languageIcon;
	public static By languageOkBtn;
	public static By journeyTextInChina;
	public static By unlock;
	public static By btn_getStarted;
	public static By img_up;
	public static By text_Unlock;
	public static By text_UnlockAll; 
	public static By text_ResetUserdata; 
	public static By btn_Ok;
	public static By swipe_locator;
	public static By text_friendly;
	public static By date_picker;
	public static By But_Done;
	public static By Meetingname;
	public static By email;
	public static By Next;
	public static By btn_Done;
	public static By txt_Star;
	public static By Nextmonth; 
	public static By LaunchStart;
	public static By Ratingbutton;
	public static By perceivedbyothers;
	public static By impactyourperformance;
	public static By Image;
	public static By Nextbtn;
	public static By Confirmbtn1;
	public static By Donebtn;
	public static By Confirmbtn2;
	public static By text1;
	public static By text_launchelevatorpitch;
	public static By btn_record;
	public static By btn_play;
	public static By btn_done;
	public static By btn_done_2;
	public static By congpopup;
	public static By LaunchPriorities;
	public static By taskfield;
	public static By nextBtn;
	public static By confirmbtn;
	public static By entertask1;
	public static By nextBtn1;
	public static By nextBtn2;
	public static By Highprioritytext;
	public static By donebtn;
	public static By emailBtn;
	public static By sendMailBtn;
	public static By congratulationsPopup;
	public static By text_launchFlyonthewall;
	public static By playvideo;
	public static By seekBar;
	public static By btn_next;
	public static By rating_first;
	public static By btn_confirm;
	public static By emailaddress_1;
	public static By emailaddress_2;
	public static By emailaddress_3;
	public static By donebutton;
	public static By btn_next_2;
	public static By videoCarousel;
	public static By videoCarouselplay;
	public static By Videoload;

	static{


		preferencesIcon = By.className("android.widget.ImageButton");
		settingsBtn = By.xpath("//android.widget.LinearLayout/android.widget.RelativeLayout");
		myTitleTextBox = By.id("android:id/edit");
		okBtn = By.id("android:id/button1");
		getStrartedBtn = By.id("com.haygroup.journey.android:id/btnGetStarted");
		logOutJourney=By.xpath("//*[contains(@text,'Log Out')]");
		selectMyTitle = By.xpath("//android.widget.TextView[contains(@text,'My Title')]");
		currentTitle = By.xpath("//android.widget.TextView[@text ='My Title']/following-sibling::android.widget.TextView");
		selectPreferedLanguage = By.xpath("//android.widget.TextView[contains(@text,'User Preferred Language')]");
		languageOkBtn = By.id("android:id/button1");
		journeyTextInChina = By.id("android:id/action_bar_title");
		logOutJourney=By.xpath("//*[contains(@text,'Log Out')]");
		unlock=By.name("Unlock All Exercises");
		btn_getStarted=By.id("com.haygroup.journey.android:id/btnGetStarted");
		img_up=By.id("android:id/up");
		text_Unlock=By.xpath("//android.widget.TextView[@text='Unlock All Exercises']");
		text_ResetUserdata=By.xpath("//android.widget.TextView[@text='Reset User Data']");
		btn_Ok=By.id("android:id/button1");
		text_friendly=By.xpath("//android.widget.TextView[@text='Friendly Observation']");
		date_picker=By.id("com.haygroup.journey.android:id/btnDate");
		But_Done=By.id("android:id/button1");
		Meetingname=By.id("com.haygroup.journey.android:id/txtInput");
		email=By.id("com.haygroup.journey.android:id/txtEmail");
		Next=By.id("com.haygroup.journey.android:id/btnNext");
		btn_Done=By.id("com.haygroup.journey.android:id/btnDone");
		txt_Star=By.id("com.haygroup.journey.android:id/lblPoints");
		Nextmonth=By.id("android:id/next");
		LaunchStart=By.xpath("//android.widget.TextView[contains(@text,'Launch Start')]");
		Ratingbutton=By.id("com.haygroup.journey.android:id/llRating");
		perceivedbyothers=By.xpath("//android.widget.LinearLayout[2]/android.widget.LinearLayout[1]//android.widget.ImageButton[3]");
		impactyourperformance=By.xpath("//android.widget.LinearLayout[1]/android.widget.ImageButton[@index='3']");
		LaunchPriorities=By.xpath("//android.widget.TextView[contains(@text,'Launch Priorities')]");
		Highprioritytext=By.id("com.haygroup.journey.android:id/lblGridHeader");
		taskfield=By.id("com.haygroup.journey.android:id/txtEmail");
		entertask1=By.xpath("//android.widget.FrameLayout[1]/android.widget.ScrollView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.EditText");
		nextBtn = By.id("com.haygroup.journey.android:id/btnNext");
		confirmbtn = By.id("com.haygroup.journey.android:id/btnConfirm");
		nextBtn1 = By.id("com.haygroup.journey.android:id/btnNext3");
		nextBtn2 = By.id("com.haygroup.journey.android:id/btnNext2");
		donebtn = By.id("com.haygroup.journey.android:id/btnDone");
		emailBtn = By.id("com.haygroup.journey.android:id/txtEmail");
		sendMailBtn = By.id("com.haygroup.journey.android:id/btnDone2");
		congratulationsPopup = By.id("com.haygroup.journey.android:id/lblObjective");
		LaunchStart=By.xpath("//android.widget.TextView[contains(@text,'Launch Start')]");
		perceivedbyothers=By.xpath("//android.widget.LinearLayout[2]/android.widget.LinearLayout[1]//android.widget.ImageButton[3]");
		impactyourperformance=By.xpath("//android.widget.LinearLayout[1]/android.widget.ImageButton[3]");
		Image=By.xpath("//android.widget.LinearLayou[2]/android.widget.TextView");
		text1=By.name("Knowing your personal strengths and weaknesses");
		Nextbtn=By.xpath("//android.widget.Button[@text='Next']");
		Confirmbtn1=By.xpath("//android.widget.Button[@text='Confirm']");
		Donebtn=By.xpath("//android.widget.Button[@text='Done']");
		Confirmbtn2=By.xpath("//android.widget.Button[@text='Confirm']");
		text_launchelevatorpitch=By.xpath("//android.widget.TextView[@text='Launch Elevator Pitch']");
		btn_record=By.id("com.haygroup.journey.android:id/ibRecord");
		btn_play=By.id("com.haygroup.journey.android:id/ibPlay");
		btn_done=By.id("com.haygroup.journey.android:id/btnNext");
		btn_done_2=By.id("com.haygroup.journey.android:id/btnNext2");
		congpopup=By.id("com.haygroup.journey.android:id/lblTitle");
		text_launchFlyonthewall=By.xpath("//android.widget.TextView[@text='Launch Fly On the Wall']");
		playvideo=By.id("com.haygroup.journey.android:id/video");
		seekBar=By.id("android:id/mediacontroller_progress");
		videoCarousel = By.id("com.haygroup.journey.android:id/vvVideoCarousel");
		videoCarouselplay = By.id("com.haygroup.journey.android:id/ibPlay");
		btn_next=By.id("com.haygroup.journey.android:id/btnNext1");
		rating_first=By.xpath("//com.haygroup.journey.android:id/llTeamwork/com.haygroup.journey.android:id/llCustomRatingBar[0]/com.haygroup.journey.android:id/llRating[1]/android.widget.ImageButton[3]");
		btn_confirm=By.id("com.haygroup.journey.android:id/btnConfirm");
		donebutton=By.id("com.haygroup.journey.android:id/btnDone");
		btn_next_2=By.id("com.haygroup.journey.android:id/btnNext2");
		Videoload=By.id("com.haygroup.journey.android:id/progressBar");
	}

	public static By inputLanguage(String language){
		return languageIcon = By.xpath("//android.widget.TextView[@text='"+language+"']");

	}







}
