package com.haygroup.android.libs;

import com.haygroup.android.page.EcoSystemPageAndroid;
import com.haygroup.android.page.HomePageAndroid;
import com.haygroup.android.page.JourneyPageAndroid;
import com.haygroup.android.page.LoginPageAndroid;

import java.util.concurrent.TimeUnit;

import com.gallop.accelerators.ActionEngine;
import com.haygroup.web.page.EcoSystemPage;
import com.haygroup.web.page.HomePage;

import io.appium.java_client.android.AndroidDriver;



public class HayGroupAndroidCommonLib extends ActionEngine{

	/**
	 * @author: cigniti
	 * @description This function login To Mobile Application
	 * 
	 */

	public void loginToMobileApplication(String UserName, String Password)  {	
		try{
			waitTillElementToBeClickble(LoginPageAndroid.userName_mobile, "entering login username");
			type(LoginPageAndroid.userName_mobile, UserName, "entering login username");
			type(LoginPageAndroid.password_mobile, Password, "login password");
			click(LoginPageAndroid.loginBtn_mobile, "Login button");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function login To Journey Mobile Application
	 * 
	 */

	public void loginToJourneyMobileApplication(String UserName, String Password)  {
		try {
			type(LoginPageAndroid.userNameJourney_mobile,UserName, "entering login username");
			type(LoginPageAndroid.passwordJourney_mobile, Password,"login password");
			click(LoginPageAndroid.journeySignInBtn, "Login button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on mobileSettingsPageButton
	 * 
	 */

	public void mobileSettingsPageButton() {
		try {
			waitTillElementToBeClickble(EcoSystemPageAndroid.btnMobileSettings, "Settings button");
			click(EcoSystemPageAndroid.btnMobileSettings, "Settings button");
			Waittime();
			Waittime();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function navigate Back to page
	 * 
	 */

	public void navigateBack(){
		try {
			click(EcoSystemPageAndroid.btnBack, "Back Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks on overlay menu
	 * 
	 */

	public void openOverLayMenu(){
		try {
			waitTillElementToBeClickble(EcoSystemPageAndroid.lnkOverLayMenu,"click on menu");
			click(EcoSystemPageAndroid.lnkOverLayMenu,"click on menu");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function logOutFromApplication
	 * 
	 */

	public void logOutFromApplication() {
		try {
			JSClick(HomePage.logOutButton, "Log out");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function logOutMobileApplication	 
	 *  
	 */

	public void logOutMobileApplication(){
		try {
			waitTillElementToBeClickble(HomePageAndroid.logOut_mobile, "Log out Mobile");
			click(HomePageAndroid.logOut_mobile, "Log out Mobile");
			waitTillElementToBeClickble(LoginPageAndroid.confirmLogOut_mobile, "Log out Mobile");
			click(LoginPageAndroid.confirmLogOut_mobile, "Log out Mobile");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs swipe operation
	 * 
	 */

	public  void swipe(String locator,String text) {
		try
		{
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			AndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\""+locator+"\")).scrollIntoView(UiSelector().textContains(\""+text+"\"))");
		}
		catch(Throwable e)
		{
			System.out.println(e);
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs swipe operation on PrioritesScreen
	 * 
	 */

	public  void swipePrioritesScreen(String locator,String text) {
		try
		{
			waitForVisibilityOfElement(JourneyPageAndroid.Highprioritytext,"highpriritytext",180 );	
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			Thread.sleep(5000);
			AndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\""+locator+"\")).scrollIntoView(UiSelector().textContains(\""+text+"\"))");
		}
		catch(Throwable e)
		{
			System.out.println(e);
		}
	}

	/**
	 * @author: cigniti
	 * @description This function perfomrs scroll operation to particular element
	 * 
	 */

	public void scrollDownToElement(String locator, String text) {
		try {
			isApplicationReady();
			Waittime();
			AndroidDriver AndroidDriver = (AndroidDriver) this.appiumDriver;
			AndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\""+ locator
					+ "\")).scrollIntoView(UiSelector().textContains(\""
					+ text + "\"))");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs tap operation
	 * 
	 */

	public void tap()
	{
		try
		{
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;

			AndroidDriver.tap(1,134,526,1);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs tap operationon video
	 * 
	 */

	public void tapvideo(int x,int y)
	{
		try
		{
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;

			AndroidDriver.tap(1,x,y,1);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

	/**
	 * @author: cigniti
	 * @description This function log out from application
	 * 
	 */

	public void logout() throws Throwable {

		try {
			Waittime();
			swipe("android:id/list", "Log Out");
			click(EcoSystemPageAndroid.logout, "click on logout");
			click(EcoSystemPageAndroid.logoutbutton, "click on logout button");
		} catch (Exception e) {

		}

	}
}	

