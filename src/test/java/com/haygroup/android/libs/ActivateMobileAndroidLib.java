package com.haygroup.android.libs;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.page.LoginPageAndroid;
import com.haygroup.android.page.StylesClimatePageAndroid;

import io.appium.java_client.android.AndroidDriver;

public class ActivateMobileAndroidLib extends HayGroupAndroidCommonLib{
	ChromeDriver chdriver = null;

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	/**
	 * @author: cigniti
	 * @description This function logins to china server
	 * 
	 */
	public void loginToMobileChinaServer(String UserName, String Password){
		try {
			waitTillElementToBeClickble(LoginPageAndroid.userName_mobile, "entering login username");
			type(LoginPageAndroid.userName_mobile, UserName, "entering login username");
			type(LoginPageAndroid.password_mobile, Password, "login password");
			hideKeyBoard();
			click(ActivateMobilePageAndroid.chkChinaMobile, "select checkbox");
			click(LoginPageAndroid.loginBtn_mobile, "Login button");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function clears ScreenHide On HomePage
	 * 
	 */
	public void clearScreenHideOnHomePage(){
		try{
			waitForVisibilityOfElement(ActivateMobilePageAndroid.tblHomePageGridView, "Waiting for Styles&Climate Tab", 80);
			click(ActivateMobilePageAndroid.tblHomePageGridView, "Click on Styles&Climate Tab");
		}catch(Throwable e){}
	}
	
	/**
	 * @author: cigniti
	 * @description This function clicks on StylesAndClimate
	 * 
	 */

	public void openStylesAndClimate() {
		try{
			waitForVisibilityOfElement(ActivateMobilePageAndroid.tabStylesAndClimate, "Waiting for Styles&Climate Tab", 80);
			click(ActivateMobilePageAndroid.tabStylesAndClimate, "Click on Styles&Climate Tab");
		}catch(Throwable e){}

	}

	/**
	 * @author: cigniti
	 * @description This function clears ScreenHider 
	 * 
	 */

	public void removeScreenHider(By screenHider) throws Throwable{
		Waittime();
		try{
			waitForVisibilityOfElement(screenHider, "Waiting for Styles&Climate Tab", 80);
			click(screenHider, "Click on Styles&Climate Tab");
		}catch(Throwable e){}
	}

	/**
	 * @author: cigniti
	 * @description This function open the QR code
	 * 
	 */

	public void openQRcode() {
		try { 
			Waittime();
			String executionPath = System.getProperty("user.dir");
			  System.out.println(" executionPath "+executionPath);
			  System.setProperty("webdriver.chrome.driver", executionPath+"/Drivers/chromedriver.exe"); 
			  Waittime();
			  Waittime();
			chdriver = new ChromeDriver();
			chdriver.get("file://"+executionPath+"/TestData/PROD_QR_CODE.PNG");
		}catch(Throwable e){}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks on scan QR code
	 * 
	 */

	public void clickScanQRCode() {
		try {
			waitForVisibilityOfElement(ActivateMobilePageAndroid.swipToScanQRCode, "Waiting for Scan QR Code Text", 80);
			click(ActivateMobilePageAndroid.swipToScanQRCode, "Click on Scan QR Code Text");
			if(getElementsSize(StylesClimatePageAndroid.scanCodeOKButton)>0)
			click(ActivateMobilePageAndroid.btnScanCodeOK, "Click OK Button");
			Waittime();
			isApplicationReady();
			Waittime();

		}catch(Throwable e){}
	}

	/**
	 * @author: cigniti
	 * @description This function quits the QR code
	 * 
	 */

	public void quitQRcode() {
		try{
			chdriver.quit();
			Waittime();
			Waittime();
			Thread.sleep(10000);
			isApplicationReady();	
		}catch(Throwable e){}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks One OneContinue Button
	 * 
	 */

	public void clickOneOneContinueButton() {
		try{ 
			Waittime();
			isApplicationReady();
			waitForVisibilityOfElement(StylesClimatePageAndroid.continueBtn, "Click continue Button", 80);
			waitTillElementToBeClickble(StylesClimatePageAndroid.continueBtn, "continue Button");
			click(StylesClimatePageAndroid.continueBtn, "Click continue Button");
			Waittime();
			isApplicationReady();
			Thread.sleep(5000);
		}catch(Throwable e){}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks on Continue Button
	 * 
	 */

	public void clickContinueButton() {
		try {
			Waittime();
			isApplicationReady();
			waitTillElementToBeClickble(StylesClimatePageAndroid.continueBtn, "continue Button");
			click(StylesClimatePageAndroid.continueBtn, "Click continue Button");
			Waittime();
			Thread.sleep(8000);
		}catch(Throwable e){}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks on DesignMyPlan Button
	 * 
	 */

	public void clickDesignMyPlanButton() {
		try {
			isApplicationReady();
			Waittime();
			waitForVisibilityOfElement(ActivateMobilePageAndroid.btnDesignMyPlan, "Click design My Plan Button", 80);
			click(ActivateMobilePageAndroid.btnDesignMyPlan, "Click design MyPlan Button");
			click(ActivateMobilePageAndroid.btnDesignMyPlan, "Click design MyPlan Button");

		}catch(Throwable e){}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks on CreatePlanForMe Button
	 * 
	 */

	public void clickCreatePlanForMe() {
		try{
			waitForVisibilityOfElement(ActivateMobilePageAndroid.btnCreatePlanForMe, "Click Create Plan For Me Button", 80);
			click(ActivateMobilePageAndroid.btnCreatePlanForMe, "Click design My Own Plan Button");
			Waittime();
			isApplicationReady();
			Thread.sleep(5000);
		}catch(Throwable e){}
	}

	/**
	 * @author: cigniti
	 * @description This function  clicks on DesignMyOwnPlan button
	 * 
	 */

	public void clickDesignMyOwnPlanButton(){
		try{
			waitForVisibilityOfElement(ActivateMobilePageAndroid.btnDesignMyOwnPlan, "Click design My Own Plan Button", 80);
			click(ActivateMobilePageAndroid.btnDesignMyOwnPlan, "Click design My Own Plan Button");
			Waittime();
			isApplicationReady();
		}catch(Throwable e){}
	}		

	/**
	 * @author: cigniti
	 * @description This function verifies the My plan page
	 * 
	 */

	public void Myplanpage(){
		try{ 

			Thread.sleep(4000);
			removeScreenHider(StylesClimatePageAndroid.levelUpPageHidefive);
		}catch(Throwable e){}
	}

}
