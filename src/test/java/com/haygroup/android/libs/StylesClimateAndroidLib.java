package com.haygroup.android.libs;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.page.JourneyPageAndroid;
import com.haygroup.android.page.StylesClimatePageAndroid;
import com.haygroup.web.page.HomePage;


public class StylesClimateAndroidLib extends HayGroupAndroidCommonLib{

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	/**
	 * @author: cigniti
	 * @description This function clears ScreenHide On HomePage
	 * 
	 */

	public void clearScreenHideOnHomePage(){
		try{
			waitForVisibilityOfElement(StylesClimatePageAndroid.homePageGridView, "Waiting for Styles&Climate Tab", 80);
			click(StylesClimatePageAndroid.homePageGridView, "Click on Styles&Climate Tab");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks on styles and climate field
	 * 
	 */

	public void openStylesAndClimate() {
		try {
			waitForVisibilityOfElement(StylesClimatePageAndroid.stylesAndClimateTab, "Waiting for Styles&Climate Tab", 80);
			click(StylesClimatePageAndroid.stylesAndClimateTab, "Click on Styles&Climate Tab");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function removes the screen hider
	 * 
	 */

	public void removeScreenHider(By screenHider){
		try{
			waitForVisibilityOfElement(screenHider, "Waiting for Styles&Climate Tab", 80);
			click(screenHider, "Click on Styles&Climate Tab");
		}catch(Throwable e){}
	}

	/**
	 * @author: cigniti
	 * @description This function verifies the level up page
	 * 
	 */

	public void verifyLevelUpPage() {
		try {
			waitForVisibilityOfElement(StylesClimatePageAndroid.yourRangeofStylesText, "Waiting for LevelUp Page", 80);
			assertTextMatching(StylesClimatePageAndroid.yourRangeofStylesText, "Your Range of Styles", "Verify user is on LevelUp Screen");
			click(StylesClimatePageAndroid.yourRangeofStylesText, "Click on Your Range of Styles Text");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs swipe operation
	 * 
	 */

	public void scrollDownToElement(String locator, String text) {
		try
		{
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			AndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\""+locator+"\")).scrollIntoView(UiSelector().textContains(\""+text+"\"))");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on check boxes
	 * 
	 */

	public void clickCheckboxes() 
	{
		try {
			Waittime();
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			List<WebElement> elements = AndroidDriver.findElementsById("com.haygroup.activate:id/selected");
			System.out.println(elements.size());

			for (WebElement element : elements)
			{
				Thread.sleep(3000);

				element.click();
			}

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function returns current the date
	 * 
	 */

	public  int  returnDate()
	{
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year  = localDate.getYear();
		int month = localDate.getMonthValue();
		int day   = localDate.getDayOfMonth();

		return day;
	}

	/**
	 * @author: cigniti
	 * @description This function returns the current month
	 * 
	 */

	public  int  returnMonth()
	{
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year  = localDate.getYear();
		int month = localDate.getMonthValue();
		int day   = localDate.getDayOfMonth();

		return month;
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on date field
	 * 
	 */


	public void  clickdate(int index,int month) throws Throwable
	{
		if(index==31 ||index==32)
		{

			index=2;
			click(StylesClimatePageAndroid.Nextmonth,"click on next month");

		}
		try
		{

			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			WebElement ele =AndroidDriver.findElement(By.xpath("//android.view.View[@index='"+index+"']"));
			ele.click();
		}
		catch(Exception e)
		{

			System.out.println(e);

		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on scan QR code
	 * 
	 */

	public void clickScanQRCode() {
		try {
			waitForVisibilityOfElement(StylesClimatePageAndroid.swipToScanQRCode, "Waiting for Scan QR Code Text", 80);
			click(StylesClimatePageAndroid.swipToScanQRCode, "Click on Scan QR Code Text");
			if(getElementsSize(StylesClimatePageAndroid.scanCodeOKButton)>0)
			click(StylesClimatePageAndroid.scanCodeOKButton, "Click OK Button");
			Waittime();
			isApplicationReady();
			Thread.sleep(3000);
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on enter feedback manually field
	 * 
	 */

	public void clickEnterFeedBackManuallyButton() {
		try {
			Waittime();
			isApplicationReady();
			waitForVisibilityOfElement(StylesClimatePageAndroid.clickEnterFeedBackManually, "Click Enter feedback manually Button", 80);
			waitTillElementToBeClickble(StylesClimatePageAndroid.clickEnterFeedBackManually, "Click Enter feedback manually Button");
			click(StylesClimatePageAndroid.clickEnterFeedBackManually, "Click Enter feedback manually Button");
			waitForVisibilityOfElement(StylesClimatePageAndroid.YesButton, "Click yes Button", 80);
			click(StylesClimatePageAndroid.YesButton, "Click yes Button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function enters the unique code
	 * 
	 */

	public void enterUinqueCode(String qrcode) {
		try {
			isApplicationReady();
			Waittime();
			waitForVisibilityOfElement(StylesClimatePageAndroid.uniqueCodeTextBox, "unique code textbox", 80);
			Waittime();
			type(StylesClimatePageAndroid.uniqueCodeTextBox, qrcode, "Enter qr code");
			Waittime();
			waitTillElementToBeClickble(StylesClimatePageAndroid.enterQrcodeContinueBtn, "Click continue Button");
			click(StylesClimatePageAndroid.enterQrcodeContinueBtn, "Click continue Button");
			isApplicationReady();	
			Waittime();
			waitForInVisibilityOfElement(JourneyPageAndroid.Videoload, "wait foe video to load");
			Waittime();
			Waittime();
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on one one continue button
	 * 
	 */

	public void clickOneOneContinueButton() {
		try {
			Waittime();
			isApplicationReady();
			waitForVisibilityOfElement(StylesClimatePageAndroid.continueBtn, "Click continue Button", 80);
			waitTillElementToBeClickble(StylesClimatePageAndroid.continueBtn, "continue Button");
			click(StylesClimatePageAndroid.continueBtn, "Click continue Button");
			Waittime();
			isApplicationReady();
			Thread.sleep(5000);
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on continue button
	 * 
	 */

	public void clickContinueButton() {
		try {
			Waittime();
			isApplicationReady();
			waitTillElementToBeClickble(StylesClimatePageAndroid.continueBtn, "continue Button");
			click(StylesClimatePageAndroid.continueBtn, "Click continue Button");
			Waittime();
			Thread.sleep(16000);
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on DesignMyPlanButton
	 * 
	 */

	public void clickDesignMyPlanButton() {
		try {
			isApplicationReady();
			Waittime();
			waitForVisibilityOfElement(ActivateMobilePageAndroid.btnDesignMyPlan, "Click design My Plan Button", 80);
			click(ActivateMobilePageAndroid.btnDesignMyPlan, "Click design MyPlan Button");
			click(ActivateMobilePageAndroid.btnDesignMyPlan, "Click design MyPlan Button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on CreatePlanForMe button
	 * 
	 */

	public void clickCreatePlanForMe() {
		try {
			waitForVisibilityOfElement(StylesClimatePageAndroid.createPlanForMeBtn, "Click Create Plan For Me Button", 80);
			click(StylesClimatePageAndroid.createPlanForMeBtn, "Click design My Own Plan Button");
			Waittime();
			isApplicationReady();
			Thread.sleep(15000);
			waitForInVisibilityOfElement(JourneyPageAndroid.Videoload, "wait foe video to load");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on DesignMyOwnPlanButton
	 * 
	 */

	public void clickDesignMyOwnPlanButton() {
		try {
			waitForVisibilityOfElement(StylesClimatePageAndroid.designMyOwnPlanBtn, "Click design My Own Plan Button", 80);
			click(StylesClimatePageAndroid.designMyOwnPlanBtn, "Click design My Own Plan Button");
			Waittime();
			isApplicationReady();
			waitForInVisibilityOfElement(JourneyPageAndroid.Videoload, "wait foe video to load");

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects five tips
	 * 
	 */

	public void selectFiveTips() {
		try {
			Waittime();
			waitForVisibilityOfElement(StylesClimatePageAndroid.levelUpPageHidefour, "wait for page hider", 180);
			removeScreenHider(StylesClimatePageAndroid.levelUpPageHidefour);
			waitForVisibilityOfElement(StylesClimatePageAndroid.vStyle, "wait for v style  visibility", 180);
			click(StylesClimatePageAndroid.vStyle,"click on visionary style");
			Waittime();
			click(StylesClimatePageAndroid.vStyle,"click on visionary style");
			clickCheckboxes();
			Waittime();
			click(StylesClimatePageAndroid.aStyle,"click on Affiliative style");
			Waittime();
			clickCheckboxes();
			click(StylesClimatePageAndroid.But_Continue,"performing click on continue button");
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects four tips
	 * 
	 */

	public void selectfourtips()  {
		try {
			waitForInVisibilityOfElement(JourneyPageAndroid.Videoload, "wait foe video to load");
			Waittime();
			Waittime();
			scrollUp(1);
			//scrollDownToElement("com.haygroup.activate:id/carddemo_list_expand","Using the Visionary Style");
			click(StylesClimatePageAndroid.vStyle,"click on visionary style");
			waitForVisibilityOfElement(StylesClimatePageAndroid.aStyle, "wait for a style  visibility", 180);
			Waittime();
			click(StylesClimatePageAndroid.aStyle,"click on Affiliative style");
			clickCheckboxes();
			Waittime();
			click(StylesClimatePageAndroid.aStyle,"click on Affiliative style");
			Waittime();
			//scrollDownToElement("com.haygroup.activate:id/carddemo_list_expand","Using the Pacesetting Style");
			Waittime();
			click(StylesClimatePageAndroid.pstyle,"click on Pacesetting style");
			clickCheckboxes();	
			Waittime();
			click(StylesClimatePageAndroid.But_Continue,"performing click on continue button");
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects the date
	 * 
	 */

	public void selectdate() {
		try {
			waitForInVisibilityOfElement(JourneyPageAndroid.Videoload, "wait foe video to load");
			waitTillElementToBeClickble(StylesClimatePageAndroid.vStyle,"click on visionary style");
			click(StylesClimatePageAndroid.vStyle,"click on visionary style");
			Waittime();
			scrollUp(1);
			//scrollDownToElement("com.haygroup.activate:id/carddemo_list_expand","Using the Participative Style");
			Waittime();
			click(StylesClimatePageAndroid.participativestyle,"click on Participative style");
			clickCheckboxes();
			Waittime();
			int d=returnDate();
			int m=returnMonth();
			if(d==30&& m==1 ||d==30&& m==3||d==30&& m==5||d==30&& m==7||d==30&& m==8||d==30&& m==10||d==30&& m==12)
			{
				clickdate(d,m);
			}
			else
			{
				clickdate(d+1,m);
			}
			click(StylesClimatePageAndroid.But_OK,"");
			Waittime();
			waitTillElementToBeClickble(StylesClimatePageAndroid.But_Continue,"performing click on continue button");
			click(StylesClimatePageAndroid.But_Continue,"performing click on continue button");
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function removes screen hider on my plan page
	 * 
	 */

	public void Myplanpage(){
		try {
			Waittime();
			waitTillElementToBeClickble(StylesClimatePageAndroid.levelUpPageHidefive,"performing click on page hider");
			removeScreenHider(StylesClimatePageAndroid.levelUpPageHidefive);
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects the tip
	 * 
	 */

	public void selectTheTip() {
		try {
			tap();
			Waittime();
			appiumDriver.swipe(0, 700, 0, 100, 2000);
			click(StylesClimatePageAndroid.selectTipCheckBox, "Select the Tip");
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects all the tips
	 * 
	 */

	public void selectAllDetails() {
		try {
			
			int eleSize = Driver.findElements(By.xpath("//android.widget.CheckBox")).size();
			System.out.println("Number of check boxes:" + eleSize);
			
				
			if (eleSize==1) {
				selectTheTip1();
				
				}
			else if (eleSize==2) {
				selectTheTip1();
				selectTheTip2();
				
			}
			else if (eleSize==3) {
				selectTheTip1();
				selectTheTip2();
				Waittime();
				selectMyPlanLink3();
			}

			
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * @author: cigniti
	 * @description This function selects the tip1
	 * 
	 */

	public void selectTheTip1() {

		try {
			Waittime();
			click(StylesClimatePageAndroid.selectTipCheckBox, "Select the Tip");
			Waittime();
			ratingBar();
			enterFeedback();
			clickOkOnPopup();
			
		
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects the tip2
	 * 
	 */

	public void selectTheTip2() {
		try {
			Waittime();
			selectMyPlanLink2();
			Waittime();
			//saveDetails();

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs rating operation 
	 * 
	 */

	public void ratingBar() {
		try {
			Waittime();
			click(StylesClimatePageAndroid.ratingBar, "Give the rating");
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function enters the Feedback
	 * 
	 */

	public void enterFeedback() {
		try {
			Waittime();
			type(StylesClimatePageAndroid.enterCommentstxtbox, "rating", "Enter the feedback");
			hideKeyBoard();
			click(StylesClimatePageAndroid.ratebtn, "Click on rate button");
			isApplicationReady();	

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on pop up 
	 * 
	 */

	public void clickOkOnPopup() {
		try {
			Waittime();
			click(StylesClimatePageAndroid.clickOkPopup, "Click on rate button");

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on calendar to edit details
	 * 
	 */

	public void calenderEdit(){
		try {

			click(StylesClimatePageAndroid.Editbtn, "Click on Calender Edit button");
			waitTillElementToBeClickble(StylesClimatePageAndroid.Editbtn, "Click on Calender Edit button");

		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on my plan field
	 * 
	 */

	public void selectMyPlan() {
		try {
			Waittime();
			waitTillElementToBeClickble(StylesClimatePageAndroid.selectMyPlan, "Select the plan");
			Thread.sleep(4000);
			click(StylesClimatePageAndroid.selectMyPlan, "Click on rate button");
			//isApplicationReady();	

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects MyPlan Link
	 * 
	 */

	public void selectMyPlanLink() {
		try {
			Waittime();
			waitForVisibilityOfElement(StylesClimatePageAndroid.selectLink, "Select the plan", 80);
			click(StylesClimatePageAndroid.selectLink, "Click on rate button");
			isApplicationReady();

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects the date
	 * 
	 */

	public void selectDate() {
		try {
			Waittime();
			click(StylesClimatePageAndroid.selectDate, "Click on rate button");
			isApplicationReady();

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects MyPlanLink2 
	 * 
	 */
	public void selectMyPlanLink2() throws Throwable
	{
		
		Waittime();
		
		scrollUp(2);
		click(StylesClimatePageAndroid.selectTipText, "Click on selectTipText");
		Waittime();
		click(StylesClimatePageAndroid.Editbtn, "Click on Editbtn");
		Waittime();
            //click on date picker
		click(StylesClimatePageAndroid.selectDate,"Selected the new date");
		
		
		int d=returnDateStyles();
		int m=returnMonthStyles();
		if(d==30&& m==1 ||d==30&& m==3||d==30&& m==5||d==30&& m==7||d==30&& m==8||d==30&& m==10||d==30&& m==12)
		{
		clickdateStyles(d,m);
		}
		else
		{
		clickdateStyles(d+1,m);
		}
		
		
		click(StylesClimatePageAndroid.calenderOKbtn, "Click on Savebtn");
		Waittime();
		click(StylesClimatePageAndroid.Editbtn, "Click on Editbtn");
		Waittime();
		appiumDriver.swipe(0, 700, 0, 100, 2000);
		click(StylesClimatePageAndroid.savebtn, "Click on Savebtn");
		
	}
	
	/**
	 * @author: cigniti
	 * @description This function returns date
	 * 
	 */
	public  int  returnDateStyles()
	{
	Date date = new Date();
	LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	int year  = localDate.getYear();
	int month = localDate.getMonthValue();
	int day   = localDate.getDayOfMonth();
	return day;
	}
	
	/**
	 * @author: cigniti
	 * @description This function returns Month
	 * 
	 */
	public  int  returnMonthStyles()
	{
	Date date = new Date();
	LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	int year  = localDate.getYear();
	int month = localDate.getMonthValue();
	int day   = localDate.getDayOfMonth();
	return month;
	}
	
	/**
	 * @author: cigniti
	 * @description This function selects date
	 * 
	 */

	public void  clickdateStyles(int index,int month)
	{
	if(index==31 ||index==32)
	{
	index=2;
	}
	try
	{
	AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
	WebElement ele =AndroidDriver.findElement(By.xpath("//android.view.View[@index='"+index+"']"));
	ele.click();
	}
	catch(Exception e)
	{
	System.out.println(e);
	}
	}
	
	/**
	 * @author: cigniti
	 * @description This function scrolls up
	 * 
	 */
	public void scrollUp(int count) throws Throwable
	{
		int sCount=0;
		AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
		Dimension dimensions = AndroidDriver.manage().window().getSize();
		Double screenHeightStart = dimensions.getHeight() * 0.5;
		int scrollStart = screenHeightStart.intValue();
		System.out.println("s="+scrollStart);
		Double screenHeightEnd = dimensions.getHeight() * 0.2;
		int scrollEnd = screenHeightEnd.intValue();
		for (int i = 0; i < dimensions.getHeight(); i++) {
		AndroidDriver.swipe(0,scrollStart,0,scrollEnd,2000);
		sCount++;
		if(sCount==count)
		{
			break;
		}
		
		}
	}

	

	/**
	 * @author: cigniti
	 * @description This function selects MyPlanLink3
	 * 
	 */


	public void selectMyPlanLink3() 
	{
		try {
			
			Waittime();
			scrollUp(2);
			Thread.sleep(1000);
			click(StylesClimatePageAndroid.selectTipText2, "Click on selectTipText2");
			Waittime();
			click(StylesClimatePageAndroid.Editbtn, "Click on Editbtn");
			Waittime();
			click(StylesClimatePageAndroid.nameDdbtn, "Click on name dropdown");
			Waittime();
			click(StylesClimatePageAndroid.DdText, "Click on name dropdown");
			Waittime();
			click(StylesClimatePageAndroid.Editbtn, "Click on Editbtn");
			appiumDriver.swipe(0, 900, 0, 100, 2000);
			appiumDriver.findElement(By.id("save_btn")).click();


		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * @author: cigniti
	 * @description This function selects MyPlanLink4
	 * 
	 */

	public void selectMyPlanLink4() 
	{
		try {
			Waittime();
			appiumDriver.swipe(0, 700, 0, 100, 2000);
			appiumDriver.swipe(0, 700, 0, 100, 2000);
			appiumDriver.swipe(0, 700, 0, 100, 2000);
			click(StylesClimatePageAndroid.selectTipText3, "Click on selectTipText3");
			Waittime();
			click(StylesClimatePageAndroid.Editbtn, "Click on Editbtn");
			appiumDriver.swipe(0, 700, 0, 100, 2000);
			Driver.findElement(By.id("remove_btn")).click();
			Driver.findElement(By.id("button2")).click();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function saves the details
	 * 
	 */

	public void saveDetails() 
	{
		try {
			appiumDriver.findElement(By.id("button1")).click();
			click(StylesClimatePageAndroid.Editbtn, "Click on Editbtn");
			appiumDriver.swipe(0, 500, 0, 100, 2000);
			appiumDriver.findElement(By.id("save_btn")).click();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function gets the date
	 * 
	 */

	public void getDate()
	{
		try {
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			String date=AndroidDriver.findElement(By.id("android:id/date_picker_header_date")).getText();
			int score = Integer.parseInt(date.substring(4,2));
			System.out.println(score);
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * @author: cigniti
	 * @description This function logins to china server
	 * 
	 */

	public void searchGoogle()
	{
		try {
			Driver.get("http://www.google.com");
			System.out.println("Search Test");
			Driver.findElement(By.xpath("//input[@class='lst lst-tbb gsfi']")).sendKeys("Automation testing");
			Driver.findElement(By.xpath("//button[@class='lsbb']/div")).click();

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
