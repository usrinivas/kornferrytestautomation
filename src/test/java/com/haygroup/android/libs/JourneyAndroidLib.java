package com.haygroup.android.libs;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.page.JourneyPageAndroid;
import com.haygroup.android.page.LoginPageAndroid;
import com.haygroup.web.page.EcoSystemPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.SwipeElementDirection;
import io.appium.java_client.android.AndroidDriver;

public class JourneyAndroidLib extends HayGroupAndroidCommonLib{

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");


	/**
	 * @author: cigniti
	 * @description This function performs click operation on SettingsButton
	 * 
	 */

	public void clickSettingsButton() {

		try {
			isApplicationReady();
			tap();
			waitTillElementToBeClickble(JourneyPageAndroid.preferencesIcon,"click preferences icon");
			click(JourneyPageAndroid.preferencesIcon, "click on preferences Icon");
			click(JourneyPageAndroid.settingsBtn, "click on settings Button");
			isApplicationReady();

		}  catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function logins To Journey MobileChina Server
	 * 
	 */

	public void loginToJourneyMobileChinaServer(String UserName, String Password) {
		try {
			type(LoginPageAndroid.userNameJourney_mobile,UserName, "entering login username");
			type(LoginPageAndroid.passwordJourney_mobile, Password,"login password");
			hideKeyBoard();
			click(LoginPageAndroid.chinaServerCheckBox, "Select china server checkbox");
			click(LoginPageAndroid.journeySignInBtn, "Login button");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	/**
	 * @author: cigniti
	 * @description This function clicks on GetStarted Button
	 * 
	 */

	public void clickGetStartedButton(){
		try{
			isApplicationReady();
			Waittime();
			waitTillElementToBeClickble(JourneyPageAndroid.getStrartedBtn,"click ok Button");
			click(JourneyPageAndroid.getStrartedBtn, "click on getStarted button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs logout operation from journey application
	 * 
	 */

	public void logOutJourney(){
		try {

			click(JourneyPageAndroid.logOutJourney, "Logout button");
			waitTillElementToBeClickble(LoginPageAndroid.journeySignInBtn,"click logout Button");

		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects MyTitle
	 * 
	 */

	public void selectMyTitle(String name){
		try{
			isApplicationReady();
			waitTillElementToBeClickble(JourneyPageAndroid.selectMyTitle,"select my title Icon");
			click(JourneyPageAndroid.selectMyTitle, "select my title Icon");
			type(JourneyPageAndroid.myTitleTextBox, name, "Entering a Title");
			waitTillElementToBeClickble(JourneyPageAndroid.okBtn,"click ok Button");
			click(JourneyPageAndroid.okBtn, "click ok Button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs languagechange_mobile operation
	 * 
	 */

	public void languagechange_mobile(String language) {
		try{
			waitTillElementToBeClickble(JourneyPageAndroid.selectPreferedLanguage,"click on Preffered languages");
			click(JourneyPageAndroid.selectPreferedLanguage,"click on Preffered languages");
			click(JourneyPageAndroid.inputLanguage(language),"click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageAndroid.languageOkBtn,"OK Button");
			click(JourneyPageAndroid.languageOkBtn,"OK Button");
			Waittime();
			isApplicationReady();
		}
		catch (Throwable e) {
		}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks on Settings Button
	 * 
	 */

	public void clickSettings () {

		try {
			isApplicationReady();
			try
			{ 
				Thread.sleep(5000);
				tap();
				waitTillElementToBeClickble(JourneyPageAndroid.btn_getStarted,"performing click on get started button");
				click(JourneyPageAndroid.btn_getStarted,"performing click on get started button");
				Thread.sleep(5000);
				click(JourneyPageAndroid.img_up,"performing click on up button");

			}

			catch(Exception e )
			{
				System.out.println(e);
			}
			Thread.sleep(5000);
			tap();
			waitTillElementToBeClickble(JourneyPageAndroid.preferencesIcon,"performing click on get started button");
			click(JourneyPageAndroid.preferencesIcon, "click on preferences Icon");
			Thread.sleep(5000);

			click(JourneyPageAndroid.settingsBtn, "click on settings Button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs swipe operation
	 * 
	 */

	public void swipeUpscreen() throws Throwable

	{
		try
		{
			isApplicationReady();
			swipe("android:id/list","Reset User Data");

		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	} 

	/**
	 * @author: cigniti
	 * @description This function performs swipe operation
	 * 
	 */

	public void swipeUpscreenFriendly() throws Throwable

	{
		try
		{
			isApplicationReady();
			swipe("android:id/list","Friendly Observation");

		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	} 

	/**
	 * @author: cigniti
	 * @description This function performs wipes the user data
	 * 
	 */

	public void wipeUserdata() throws Throwable
	{
		try
		{

			click(JourneyPageAndroid.text_Unlock,"performing click on text unlock");
			click(JourneyPageAndroid.text_ResetUserdata,"performing click on text unlock");
			click(JourneyPageAndroid.btn_Ok,"performing click on OK button");
			click(JourneyPageAndroid.btn_Ok,"performing click on OK button");

		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	} 

	/**
	 * @author: cigniti
	 * @description This function performs operations on user friendly data field
	 * 
	 */

	public void userFriendlyData(String meeting,String email) throws Throwable
	{
		try
		{
			click(JourneyPageAndroid.text_friendly,"performing click on text friendly observation");
			click(JourneyPageAndroid.btn_Ok,"performing click on OK button");
			click(JourneyPageAndroid.btn_Ok,"performing click on yes button of user feedback pop up");
			click(JourneyPageAndroid.date_picker,"performing click on date");
			//pickDate();
			int d=returnDate();
			int m=returnMonth();

			if(d==30&& m==1 ||d==30&& m==3||d==30&& m==5||d==30&& m==7||d==30&& m==8||d==30&& m==10||d==30&& m==12)
			{
				clickdate(d,m);
			}
			else
			{

				clickdate(d+1,m);
			}
			click(JourneyPageAndroid.But_Done,"performing click on done button");
			type(JourneyPageAndroid.Meetingname,meeting,"entering meeting name");
			hideKeyBoard();
			type(JourneyPageAndroid.email,email,"entering email");
			hideKeyBoard();
			click(JourneyPageAndroid.Next,"performing click on next button");
			click(JourneyPageAndroid.btn_Done,"performing click on done button");	
			Waittime();
			tap();
			click(JourneyPageAndroid.preferencesIcon, "click on preferences Icon");
			click(JourneyPageAndroid.settingsBtn, "click on settings Button");
		}
		catch(Exception e)
		{
			click(JourneyPageAndroid.preferencesIcon, "click on preferences Icon");
			click(JourneyPageAndroid.settingsBtn, "click on settings Button");
			System.out.println(e);
		}

	} 

	/**
	 * @author: cigniti
	 * @description This function returns the current date
	 * 
	 */

	public  int  returnDate()
	{
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year  = localDate.getYear();
		int month = localDate.getMonthValue();
		int day   = localDate.getDayOfMonth();

		return day;
	}

	/**
	 * @author: cigniti
	 * @description This function returns the current month
	 * 
	 */

	public  int  returnMonth()
	{
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year  = localDate.getYear();
		int month = localDate.getMonthValue();
		int day   = localDate.getDayOfMonth();
		return month;
	}

	/**
	 * @author: cigniti
	 * @description This function clicks on date
	 * 
	 */

	public void  clickdate(int index,int month)
	{
		if(index==31 ||index==32)
		{
			index=2;
		}
		try
		{
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			WebElement ele =AndroidDriver.findElement(By.xpath("//android.view.View[@index='"+index+"']"));
			ele.click();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs logout operation
	 * 
	 */

	public void logout()
	{
		try {
			Waittime();
			swipe("android:id/list","Log Out");
			click(JourneyPageAndroid.logOutJourney,"performing click on logout");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs languagechange_mobile operation
	 * 
	 */

	public void languagechange_mobile(String languagechange, String defaultLanguage)  {
		try {
			Waittime();
			waitTillElementToBeClickble(JourneyPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
			click(JourneyPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageAndroid.inputLanguage(languagechange), "click on Preffered languages");
			click(JourneyPageAndroid.inputLanguage(languagechange), "click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageAndroid.okBtn, "OK Button");
			click(JourneyPageAndroid.okBtn, "OK Button");
			Waittime();
			Waittime();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs languagechangeforChina_mobile operation
	 * 
	 */

	public void languagechangeforChina_mobile(String changedLanguage, String defaultLanguage) {
		try {
			Thread.sleep(4000);
			waitTillElementToBeClickble(JourneyPageAndroid.inputLanguage(changedLanguage), "click on Preffered languages");
			click(JourneyPageAndroid.inputLanguage(changedLanguage), "click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
			click(JourneyPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageAndroid.okBtn, "OK Button");
			click(JourneyPageAndroid.okBtn, "OK Button");
			Waittime();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on launch start button
	 * 
	 */

	public void Launchstart() 	{
		try
		{
			swipe("android:id/list","Launch Start");
			click(JourneyPageAndroid.LaunchStart,"LaunchStart");
			Waittime();
		}

		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on launch priorities button
	 * 
	 */

	public void launchPriorities() 
	{
		try {
			isApplicationReady();
			Waittime();
			waitTillElementToBeClickble(JourneyPageAndroid.LaunchPriorities,"Launch Priorities");
			click(JourneyPageAndroid.LaunchPriorities,"Launch Priorities");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function enters the task details
	 * 
	 */

	public void enterTask(String data,String data1) 
	{
		try {
			Waittime();
			waitForVisibilityOfElement(JourneyPageAndroid.taskfield, "enter task", 180);
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			List<WebElement> elements = AndroidDriver.findElementsById("com.haygroup.journey.android:id/txtEmail");
			System.out.println(elements.size());
			Waittime();
			for(int i=0;i<=1;i++)
			{
				elements.get(i).sendKeys(data);
				hideKeyBoard();
				elements.get((i+1)).sendKeys(data1);
				hideKeyBoard();
				break;
			}
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function gives rating by tapping on each task
	 * 
	 */

	public void tapRatingForEachTask(int i) 
	{
		try {
			Waittime();
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			List<WebElement> elements = AndroidDriver.findElementsByClassName("android.widget.ImageButton");
			System.out.println(elements.size());
			elements.get(i).click();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks next button
	 * 
	 */

	public void tapNext() 
	{
		try {
			Waittime();
			click(JourneyPageAndroid.nextBtn,"Click next button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on confirm pop up
	 * 
	 */

	public void tapConfirm() 
	{
		try {
			Waittime();
			click(JourneyPageAndroid.confirmbtn,"Click confirm button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on next button in priorities
	 * 
	 */

	public void tapNextOnPriorities() 
	{
		try {
			Waittime();
			click(JourneyPageAndroid.nextBtn1,"Click next button");

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs again a  click operation on next button in priorities
	 * 
	 */

	public void tapNextPriorities() 
	{
		try {
			Waittime();
			click(JourneyPageAndroid.nextBtn2,"Click next button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function enters the email addresses
	 * 
	 */

	public void enterEmailAddress(String emailid) 
	{
		try{
			isApplicationReady();
			Waittime();
			type(JourneyPageAndroid.emailBtn,emailid,"Enter email");
			hideKeyBoard();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on send button
	 * 
	 */

	public void sendMail() 
	{
		try {
			isApplicationReady();
			Waittime();
			click(JourneyPageAndroid.sendMailBtn,"Click next button");
			Waittime();
			tap();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on congratulationsPopup
	 * 
	 */

	public void cogratulationsPopup() 
	{
		try {
			Waittime();
			click(JourneyPageAndroid.congratulationsPopup,"Click next button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on priority done button
	 * 
	 */

	public void priorityDone() 
	{
		try {
			Waittime();
			click(JourneyPageAndroid.donebtn,"Click next button");

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function wipes the elevator pitch data
	 * 
	 */

	public void wipeElevatorpitchdata() 
	{
		try
		{
			click(JourneyPageAndroid.text_ResetUserdata,"performing click on text unlock");
			click(JourneyPageAndroid.btn_Ok,"performing click on OK button");
			click(JourneyPageAndroid.btn_Ok,"performing click on OK button");

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs swipe operation to elevator pitch
	 * 
	 */

	public void swipeUpscreenElevatorpitch() 
	{
		try
		{
			isApplicationReady();
			swipe("android:id/list","Launch Elevator Pitch");
			click(JourneyPageAndroid.text_launchelevatorpitch,"click on elevatorpitch");
			Waittime();

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on rating fields
	 * 
	 */

	@SuppressWarnings({"unchecked"})
	public void clickstart() 
	{
		try{
			waitTillElementToBeClickble(JourneyPageAndroid.Ratingbutton,"click on rating");
			List<MobileElement> list = appiumDriver.findElements(By.id("com.haygroup.journey.android:id/llRating"));
			System.out.println("****************"+list.size());
			for(int i=0;i<list.size();i++){
				//     ratingTitlesSelected.add(ratingTitlesList.get(i).getText());
				list.get(i).click();
			}
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on next button
	 * 
	 */

	public void  clicknext() 
	{
		try {
			click(JourneyPageAndroid.Nextbtn,"Click on Next button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on confirm popup
	 * 
	 */

	public void clickconfirm() 
	{
		try{
			click(JourneyPageAndroid.Confirmbtn1,"Click on confirm popup");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on done button
	 * 
	 */

	public void  clickDone() 
	{
		try {
			click(JourneyPageAndroid.Donebtn,"Click on Done button");

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on confirm popup
	 * 
	 */

	public void confirm() 
	{
		try{
			click(JourneyPageAndroid.Confirmbtn2,"Click on confirm popup");
			waitTillElementToBeClickble(JourneyPageAndroid.congpopup,"click on congratulations");
			click(JourneyPageAndroid.congpopup,"click on congratulations");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on record button
	 * 
	 */

	public void clickonrecord()  
	{
		try
		{
			Waittime();
			waitTillElementToBeClickble(JourneyPageAndroid.btn_record,"click on record");
			click(JourneyPageAndroid.btn_record,"click on record");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on play button
	 * 
	 */

	public void clickonplaybutton()
	{
		try
		{
			Waittime();
			waitTillElementToBeClickble(JourneyPageAndroid.btn_play,"click on play button");
			click(JourneyPageAndroid.btn_play,"click on play button");
			WebDriverWait wait = new WebDriverWait(Driver,40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.haygroup.journey.android:id/ibPlay"))); 
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on done button
	 * 
	 */

	public void clickondonebutton()
	{
		try
		{
			Waittime();
			swipe("com.haygroup.journey.android:id/viewTeamPlayerPage1","Done");
			waitTillElementToBeClickble(JourneyPageAndroid.btn_done,"click on play button");
			click(JourneyPageAndroid.btn_done,"click on done");
			Waittime();
			waitTillElementToBeClickble(JourneyPageAndroid.btn_done_2,"click on done button");
			click(JourneyPageAndroid.btn_done_2,"click on done");
			Waittime();
			waitTillElementToBeClickble(JourneyPageAndroid.congpopup,"click on congratulations");
			click(JourneyPageAndroid.congpopup,"click on congratulations");
			tap();
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function wipes the fly on the wall data
	 * 
	 */

	public void wipeFlyonthewalldata() 
	{
		try {

			click(JourneyPageAndroid.text_ResetUserdata, "performing click on text unlock");
			click(JourneyPageAndroid.btn_Ok, "performing click on OK button");
			click(JourneyPageAndroid.btn_Ok, "performing click on OK button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs the swipe operation to fly on the wall field
	 * 
	 */


	public void swipeUpscreenFlyonthewall() 

	{
		try {
			isApplicationReady();
			swipe("android:id/list", "Launch Fly On the Wall");
			click(JourneyPageAndroid.text_launchFlyonthewall, "click on fly on the wall");
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on play video button
	 * 
	 */

	public void playvideo() 
	{
		try {
			click(JourneyPageAndroid.playvideo, "Click to play video");
			Waittime();
			Waittime();
			waitForInVisibilityOfElement(JourneyPageAndroid.Videoload, "wait foe video to load");
			MobileElement videoCarousel = (MobileElement) appiumDriver.findElement(JourneyPageAndroid.videoCarousel);
			int videoCarouselx=videoCarousel.getLocation().x+200;
			int videoCarousely=videoCarousel.getLocation().y+200;
			System.out.println("video x,y "+videoCarousel.getLocation().x+" , "+videoCarousel.getLocation().y);
			click(JourneyPageAndroid.videoCarousel,"Click on Video Player");
			MobileElement seek= (MobileElement) appiumDriver.findElement(JourneyPageAndroid.seekBar);
			seek.swipe(SwipeElementDirection.RIGHT, 5000);
			Waittime();
			tapvideo(videoCarouselx,videoCarousely);
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on next button
	 * 
	 */

	public void clickonnextbutton() {
		try {
			waitForVisibilityOfElement(JourneyPageAndroid.playvideo, "click to play video", 180);
			swipe("com.haygroup.journey.android:id/viewFlyOnTheWallPage1", "Next");
			click(JourneyPageAndroid.btn_next, "click on next button");
		}

		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on rating fields
	 * 
	 */

	@SuppressWarnings({ "unchecked" })
	public void rating() throws Throwable {

		waitTillElementToBeClickble(JourneyPageAndroid.Ratingbutton,"click on rating");
		List<MobileElement> list = appiumDriver.findElements(By.id("com.haygroup.journey.android:id/llRating"));
		System.out.println("****************" + list.size());
		for (int i = 0; i < list.size(); i++) {
			list.get(i).click();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on next button
	 * 
	 */

	public void clickNext() 

	{
		try {
			click(JourneyPageAndroid.btn_next_2, "click on next button");
		}

		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on confirm pop up
	 * 
	 */

	public void clickonconfirmbutton()  {
		try {
			click(JourneyPageAndroid.btn_confirm, "click on confirm button");
		}

		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function enters the email addresses
	 * 
	 */

	public void emailaddressSend(String data,String data1,String data2) 
	{
		try {
			Waittime();
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			List<WebElement> elements = AndroidDriver.findElementsById("com.haygroup.journey.android:id/txtEmail");
			System.out.println(elements.size());
			elements.get(0).sendKeys(data);
			hideKeyBoard();
			elements.get(1).sendKeys(data1);
			hideKeyBoard();
			elements.get(2).sendKeys(data2);
			hideKeyBoard();

		}

		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on dones button
	 * 
	 */

	public void clickdonebutton()  
	{
		try {
			click(JourneyPageAndroid.donebutton, "click on done");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}







