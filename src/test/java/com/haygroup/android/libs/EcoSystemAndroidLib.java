package com.haygroup.android.libs;

import java.util.concurrent.TimeUnit;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.android.page.EcoSystemPageAndroid;
import com.haygroup.android.page.JourneyPageAndroid;
import com.haygroup.web.page.EcoSystemPage;
import com.haygroup.web.page.HomePage;


public class EcoSystemAndroidLib extends HayGroupAndroidCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	/**
	 * @author: cigniti
	 * @description This function performs click operation on settings button
	 * 
	 */
	public void settingsPageButton() {

		try {
			click(EcoSystemPageAndroid.btnMobileSettings, "click on settings");
			isApplicationReady();

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on home location dropdown
	 * 	 
	 * 
	 */

	public void homeLocation()  {
		try {
			isApplicationReady();
			waitTillElementToBeClickble(EcoSystemPageAndroid.txtHomelocation, "Homelocationclicked");
			click(EcoSystemPageAndroid.txtHomelocation, "Homelocationclicked");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on home location dropdown
	 * 	 
	 * 
	 */

	public void changeLocation(String location) {
		try {

			type(EcoSystemPageAndroid.txtEditlocation,location,"enter location");
			Waittime();
			Thread.sleep(5000);
			click(EcoSystemPageAndroid.txtClickmylocation, "click location");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on userPreferredCurrency dropdown
	 * 	 
	 * 
	 */

	public void userPreferredCurrency(String currency)  
	{
		try {

			Waittime();
			waitTillElementToBeClickble(EcoSystemPageAndroid.ddnselectPreferedCurrency, "selectPreferedCurrency");
			click(EcoSystemPageAndroid.ddnselectPreferedCurrency ,"");
			click(EcoSystemPageAndroid.inputCurrency(currency),"click on currency");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on userPreferredTimeZone dropdown
	 * 	 
	 * 
	 */

	public void userPreferredTimeZone(String timezone)  
	{
		try {
			Waittime();
			click(EcoSystemPageAndroid.ddnSelectPreferredTimezone ,"selectedCurrency");
			click(EcoSystemPageAndroid.inputTimezone(timezone),"click ontimezone");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on userpreferencetoreceiveemailnotifications dropdown
	 * 	 
	 * 
	 */

	public void userpreferencetoreceiveemailnotifications(String emailnotifications) 
	{
		try
		{
			Waittime();
			isApplicationReady();
			click(EcoSystemPageAndroid.userpreferencetoreceiveemailnotifications,"clickonreceiveemailnotifications");
			click(EcoSystemPageAndroid.inputemailnotifications(emailnotifications), "clickemailnotifications");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on userpreferenceforActionPlanreminderEmailNotifications dropdown
	 * 	 
	 * 
	 */

	public void userpreferenceforActionPlanreminderEmailNotifications(String ActionPlanremindernotification) 

	{
		try
		{		
			click(EcoSystemPageAndroid.ddnActionPlanremindersByEmailnotifications,"Click on Email notifications");
			click(EcoSystemPageAndroid.inputActionPlanreminder(ActionPlanremindernotification),"clickemailnotification ");
			Waittime();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on MyPlanPreferenceforWeekStartingDay dropdown
	 * 	 
	 * 
	 */

	public void MyPlanPreferenceforWeekStartingDay(String WeekStaringDay) 

	{
		try
		{
			click(EcoSystemPageAndroid.ddnMyPlanPreferenceforWeekStartingDay,"Click on select day");
			click(EcoSystemPageAndroid.inputWeekStaringDay(WeekStaringDay)," click weekstarting day");
			Waittime();
			if(getElementsSize(EcoSystemPageAndroid.btnOkPopup)>0)
			click(EcoSystemPageAndroid.btnOkPopup,"Ok button");	
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on MyplanpreferenceforWeekEndingDay dropdown
	 * 	 
	 * 
	 */

	public void MyplanpreferenceforWeekEndingDay(String WeekEndingDay) 	
	{
		try
		{
			Waittime();
			click(EcoSystemPageAndroid.ddnMyplanpreferenceforWeekEndingDay,"Click on select day");
			click(EcoSystemPageAndroid.inputWeekEndingDay(WeekEndingDay),"Click on Wekendingday");
			Waittime();
			if(getElementsSize(EcoSystemPageAndroid.btnOkPopup)>0)
			click(EcoSystemPageAndroid.btnOkPopup,"Ok button");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on MyplanpreferencesforteammeetingDay dropdown
	 * 	 
	 * 
	 */

	public void MyplanpreferencesforteammeetingDay(String teammeeting) 
	{
		try
		{
			Waittime();
			click(EcoSystemPageAndroid.ddnMyplanpreferencesforteammeetingDay,"Click on select day");
			click(EcoSystemPageAndroid.inputTeammeeting(teammeeting),"Click onTeammeeting");
			Waittime();
			if(getElementsSize(EcoSystemPageAndroid.btnOkPopup)>0)
			click(EcoSystemPageAndroid.btnOkPopup,"Ok button");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on Teammeetingtime dropdown
	 * 	 
	 * 
	 */

	public void Teammeetingtime() 
	{

		try
		{
			click(EcoSystemPageAndroid.Teammeetingtime,"Click on select day");
            click(EcoSystemPageAndroid.lblPreferenceTeammeetingtime,"Click on okbutton");
            if(getElementsSize(EcoSystemPageAndroid.btnWeekEndingDayOk)>0)
            click(EcoSystemPageAndroid.btnWeekEndingDayOk, "click ok button");
            Waittime();
            if(getElementsSize(EcoSystemPageAndroid.btnOkPopup)>0)
            click(EcoSystemPageAndroid.btnOkPopup, "click ok button");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs log out operation 
	 * 	 
	 * 
	 */

	public void Logout() 
	{	

		try 
		{
			click(EcoSystemPageAndroid.lnkLogout," MenuLogoutbutton");
			click(EcoSystemPageAndroid.btnPopLogout,"Logoutbuttonclicked");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}


	}

	/**
	 * @author: cigniti
	 * @description This function navigates to back screen
	 * 	 
	 * 
	 */

	public void navigateBack(){
		try {
			click(EcoSystemPageAndroid.btnBack, "Back Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation OverLayMenu
	 * 	 
	 * 
	 */

	public void openOverLayMenu(){
		try {
			waitTillElementToBeClickble(EcoSystemPageAndroid.lnkOverLayMenu,"click on menu");
			click(EcoSystemPageAndroid.lnkOverLayMenu,"click on menu");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on contactAdministrator 
	 * 	 
	 * 
	 */

	public void contactAdministrator(String message)  {

		try { 
			waitTillElementToBeClickble(EcoSystemPageAndroid.lnkContactAdmin, "click on contact admin");
			click(EcoSystemPageAndroid.lnkContactAdmin, "click on contact admin");
			waitTillElementToBeClickble(EcoSystemPageAndroid.chkAdminSelect, "click on admin");
			click(EcoSystemPageAndroid.chkAdminSelect, "click on admin");
			waitTillElementToBeClickble(EcoSystemPageAndroid.btnNext, "click on next button");
			click(EcoSystemPageAndroid.btnNext, "click on admin");
			waitForVisibilityOfElement(EcoSystemPageAndroid.txtEnter, message, 30);
			type(EcoSystemPageAndroid.txtEnter, message, "enter text");
			waitTillElementToBeClickble(EcoSystemPageAndroid.btnSend, "wait for send button");
			click(EcoSystemPageAndroid.btnSend, "click on send");
			Waittime();
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function clears the home page
	 * 	 
	 * 
	 */

	public void clearhomepage() {

		try {
			waitForInVisibilityOfElement(JourneyPageAndroid.Videoload, "wait foe video to load");
			click(EcoSystemPageAndroid.lnktouch, "click on settings");
			isApplicationReady();

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs languageChange_mobile
	 * 	 
	 * 
	 */

	public void languageChange_mobile(String languagechange, String defaultLanguage)  {
		try{
            Waittime();
            Waittime();
			swipe("android:id/list", "User Preferred Language");
			waitTillElementToBeClickble(EcoSystemPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
			click(EcoSystemPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
			waitTillElementToBeClickble(EcoSystemPageAndroid.inputLanguage(languagechange), "click on Preffered languages");
			click(EcoSystemPageAndroid.inputLanguage(languagechange), "click on Preffered languages");
			waitTillElementToBeClickble(EcoSystemPageAndroid.btnOk, "OK Button");
			click(EcoSystemPageAndroid.btnOk, "OK Button");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs languagechange for China_mobile
	 * 	 
	 * 
	 */

	public void languagechangeforChina_mobile(String changedLanguage, String defaultLanguage)  {
		try {
             Waittime();
             Waittime();
			swipe("android:id/list", changedLanguage);
			waitTillElementToBeClickble(EcoSystemPageAndroid.inputLanguage(changedLanguage), "click on Preffered languages");
			click(EcoSystemPageAndroid.inputLanguage(changedLanguage), "click on Preffered languages");
			waitTillElementToBeClickble(EcoSystemPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
			click(EcoSystemPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
			waitTillElementToBeClickble(EcoSystemPageAndroid.btnOk, "OK Button");
			click(EcoSystemPageAndroid.btnOk, "OK Button");
			Waittime();

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs logout operation
	 * 	 
	 * 
	 */

	public void logout()  {

		try {
			Waittime();
			swipe("android:id/list", "Log Out");
			click(EcoSystemPageAndroid.logout, "click on logout");
			click(EcoSystemPageAndroid.logoutbutton, "click on logout button");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}
}
