package com.haygroup.ios.libs;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import com.gallop.utilities.Xls_Reader;
import com.haygroup.ios.page.JobGradingPageIOS;

public class JobGradingIOSLib extends HayGroupIOSCommonLib{
	
	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	
	public void selectJobGrading(String Data){
		
		try {
			click(JobGradingPageIOS.jobGradingIcon, "click jobgrading icon");
			click(JobGradingPageIOS.enterJobtxtbox, "click textbox");
			type(JobGradingPageIOS.enterJobtxtbox, Data, "Entered the text");
			click(JobGradingPageIOS.enterJob,"Click the text");
			
		} 
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void selectjobfamily()
	{
		try {
			
			click(JobGradingPageIOS.selectJobFamily,"Click Jobfamily");
			click(JobGradingPageIOS.selectSubFamily,"Click Subfamily");
			click(JobGradingPageIOS.selectJobType,"Click Jobtype");
			appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			click(JobGradingPageIOS.gradeJobBtn, "apply grade button on Job factors page");
		}
		
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sliderModalDialogPopUp(String firstHeaderName,Boolean applyGrade) {
		try{
		isApplicationReady();

		//Locate element for which you wants to retrieve x y coordinates.
        WebElement policy = Driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='Policy development']//following-sibling::XCUIElementTypeOther//XCUIElementTypeSlider"));
       Point p = policy.getLocation();
       int Xcord = p.getX();
       System.out.println("Xcoordinates" + Xcord);
       int Ycord = p.getY();
       System.out.println("Ycoordinates" + Ycord);
       
	}catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
		
		public void clickApplyGradeBtn() {
			try {
			appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			//click(JobGradingPageIOS.gradeJobBtn,"Click Grade Job button");
			
			Waittime();
			appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			click(JobGradingPageIOS.applyGradeBtn,"Click ApplyGradeButton");
			}
	catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	public void selectEmployeeApplyGrade()
	{
		try {
			
			click(JobGradingPageIOS.selectEmpCheckBox,"Select Checkbox");
			click(JobGradingPageIOS.applyGradetoEmpBtn,"Apply Grade to Employee");
			Waittime();
			appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			click(JobGradingPageIOS.savethisGradeScenarioBtn, "Save this scenario  button");
		
			
			
	}
	catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
		
		public void saveThisGradescenarioDetails(String text)
		{
			try {
				 text =text+generateRandomNumber();
				type(JobGradingPageIOS.enterScenarioName,text, "Entertext");
				click(JobGradingPageIOS.selectEmpCheckBox,"Select Checkbox");
		click(JobGradingPageIOS.savethisGradeScenarioBtn, "Save this scenario  button");
				
				
				
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
		
			public void validateEmployeeGrade() 
			
			{
				try {
				 
					//waitTillElementToBeClickble(JobGradingPageIOS.homeIcon, "Home Icon");
					click(JobGradingPageIOS.homeIcon,"click Homeicon");
					//waitTillElementToBeClickble(JobGradingPageIOS.myPeople, "My People");
					click(JobGradingPageIOS.myPeople,"click MyPeople");
					waitTillElementToBeClickble(JobGradingPageIOS.byname, "By Name");
					click(JobGradingPageIOS.byname,"By Name");
					click(JobGradingPageIOS.employeeAtGradeValidation,"Grade validation details");
					click(JobGradingPageIOS.homeIcon,"click Homeicon");				
					
			}
			catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
			
			
	
}

