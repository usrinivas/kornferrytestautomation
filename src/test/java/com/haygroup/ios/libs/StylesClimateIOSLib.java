package com.haygroup.ios.libs;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import com.gallop.utilities.Xls_Reader;
import com.haygroup.ios.page.StylesClimatePageIOS;
import io.appium.java_client.ios.IOSDriver;

public class StylesClimateIOSLib extends HayGroupIOSCommonLib {
	ChromeDriver chdriver = null;

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");
	
	/**
	 * Author:@cigniti
	 * This function performs click operartion on Enter Feedback Manually button
	 */
	public void clickEnterFeedBackManuallyButton() {
		try{
		Waittime();
		click(StylesClimatePageIOS.enterManualFeedback, "Enter Feedback Manually button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Author:@cigniti
	 * This function performs click operartion on Yes button
	 * @throws Throwable 
	 */
	public void clickYesButton() {
		try{
		waitTillElementToBeClickble(StylesClimatePageIOS.yesButton, "yesButton button");
		click(StylesClimatePageIOS.yesButton, "Enter Feedback Manually button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
	/**
	 * Author:@cigniti
	 * This function performs enters the unique code into unique code textbox
	 */
	public void enterUinqueCode(String qrcode){
		try{
		Waittime();
		waitForVisibilityOfElement(StylesClimatePageIOS.uniqueCodeTextBox, "unique code textbox", 80);
		Waittime();
		type(StylesClimatePageIOS.uniqueCodeTextBox, qrcode, "Enter qr code");
		Waittime();
		waitTillElementToBeClickble(StylesClimatePageIOS.enterQrcodeContinueBtn, "Click continue Button");
		click(StylesClimatePageIOS.enterQrcodeContinueBtn, "Click continue Button");
		Waittime();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	

	/**
	 * Author:@cigniti
	 * This function performs click opertaion on Continue button
	 */
	public void clickContinueButton() {
		try{
		waitForVisibilityOfElement(StylesClimatePageIOS.continueBtn, "", 30);
		click(StylesClimatePageIOS.continueBtn, "Continue Button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}

	/**
	 * Author:@cigniti
	 * This function performs click opertaion on OK button
	 */
	public void clickOKButton() {
		try{
		waitForVisibilityOfElement(StylesClimatePageIOS.OKButton, "", 30);
		click(StylesClimatePageIOS.OKButton, "Continue Button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Author:@cigniti
	 * This function performs click opertaion on Done button
	 */
	public void clickDoneBtn() {
		try{
		waitForVisibilityOfElement(StylesClimatePageIOS.doneBtn, "", 30);
		click(StylesClimatePageIOS.doneBtn, "Continue Button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Author:@cigniti
	 * This function performs click opertaion on Done button in onetime screen
	 */
	public void clickDoneButtonInOneTimeText() {
		try{
		waitForVisibilityOfElement(StylesClimatePageIOS.doneBtn, "", 30);
		click(StylesClimatePageIOS.doneBtn, "Continue Button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		
	/**
	 * Author:@cigniti
	 * This function performs click opertaion on Desingn My Plan button
	 */
	public void clickDesignMyPlanButton(){
		try{
		Waittime();
		waitForVisibilityOfElement(StylesClimatePageIOS.designMyPlanBtn, "Click design My Plan Button", 80);
		click(StylesClimatePageIOS.designMyPlanBtn, "Click design MyPlan Button");
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Author:@cigniti
	 * This function performs click opertaion on create a plan for me button
	 */
	public void clickCreateAPlanForMeButton(){
		try{
		Waittime();
		waitForVisibilityOfElement(StylesClimatePageIOS.createAPlanForMeBtn, "Click create a Plan for me Button", 80);
		click(StylesClimatePageIOS.createAPlanForMeBtn, "Click create a Plan for me Button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	/**
	 * Author:@cigniti
	 * This function performs click opertaion on desing my own plan button
	 */
	public void clickDesignMyOwnPlanButton(){
		try{
		waitForVisibilityOfElement(StylesClimatePageIOS.designMyOwnPlanBtn, "Click design My Own Plan Button", 80);
		click(StylesClimatePageIOS.designMyOwnPlanBtn, "Click design My Own Plan Button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

	}
	
	/**
	 * Author:@cigniti
	 * This function performs click opertaion on Styles and climate
	 */
	public void openStylesAndClimate(){
		try{
		waitForVisibilityOfElement(StylesClimatePageIOS.stylesAndClimateTab, "Waiting for Styles&Climate Tab", 80);
		click(StylesClimatePageIOS.stylesAndClimateTab, "Click on Styles&Climate Tab");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	/**
	 * Author:@cigniti
	 * This function clicks scan QR code
	 */
	public void clickScanQRCode(){
	try{	
	waitForVisibilityOfElement(StylesClimatePageIOS.clickToScanQRCode, "Click OK Button", 80);
	click(StylesClimatePageIOS.clickToScanQRCode, "Click OK Button");
	}
	
	catch(Exception e)
	{
		e.printStackTrace();
	} catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}			
	}
	
	/**
	 * Author:@cigniti
	 * This function clicks Home button
	 */
	public void clickHomeButton(){
		try{
		waitForVisibilityOfElement(StylesClimatePageIOS.clickHomeBtn, "Click home Button", 80);
		click(StylesClimatePageIOS.clickHomeBtn, "Click home Button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}
	
	/**
	 * Author:@cigniti
	 * This function clicks all available chceck boxes on the screen
	 */
	public void clickCheckboxes() 
	{
		try{
		IOSDriver<WebElement> IosDriver=(IOSDriver)this.appiumDriver;
		List<WebElement> elements = IosDriver.findElements(By.xpath("//XCUIElementTypeCell/XCUIElementTypeButton[6]"));

		for (WebElement element : elements)
		{
			Thread.sleep(3000);

			element.click();
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}
	
	/**
	 * Author:@cigniti
	 * This function selects Five tips on the screen
	 */
	public void selectFiveTips(){
		try{
		click(StylesClimatePageIOS.visionaryStyle,"click on visionary Style");
		click(StylesClimatePageIOS.visionaryStyle,"click on visionary Style");
		clickCheckboxes();
		click(StylesClimatePageIOS.visionaryStyle,"click on visionary Style");
		click(StylesClimatePageIOS.affiliateStyle,"click on selectTip2");
		appiumDriver.swipe(5, 350, 5, 100, 300);
		clickCheckboxes();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}
	
	/**
	 * Author:@cigniti
	 * This function selects Four tips on the screen
	 */
	public void selectFourTips(){
		try{
		Waittime();
		click(StylesClimatePageIOS.visionaryStyle,"click on visionary Style");
		click(StylesClimatePageIOS.visionaryStyle,"click on visionary Style");
		appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-300, 5, 100, 300);
		clickCheckboxes();
		click(StylesClimatePageIOS.visionaryStyle,"click on visionary Style");
		click(StylesClimatePageIOS.pacesettingStyle,"click on pacesetting style");
		appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-450, 5, 100, 300);
		clickCheckboxes();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}

	/**
	 * Author:@cigniti
	 * This function selects one tip on the screen
	 */
	public void selectOneTip() {
		try{
		Waittime();
		click(StylesClimatePageIOS.visionaryStyle,"click on visionary style");
		click(StylesClimatePageIOS.participativeStyle,"click on Participative style");
		clickCheckboxes();
		Waittime();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}
	
	/**
	 * Author:@cigniti
	 * This function opens the QR code in chrome browser
	 */
	public void openQRcode(){
		try{
		Waittime();
		  String executionPath = System.getProperty("user.dir");
		  System.out.println(" executionPath "+executionPath);
		  System.setProperty("webdriver.chrome.driver", executionPath+"/Drivers/chromedriver_Mac"); 
		chdriver = new ChromeDriver();
		chdriver.get("file://"+executionPath+"/TestData/PROD_QR_CODE.PNG");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

		}
	
	/**
	 * Author:@cigniti
	 * This function quit the chrome browser
	 */
	public void quitQRcode(){
		try{
		chdriver.quit();
		Waittime();
		Waittime();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}
	
	public void selectTipsInMyPlan() 
	{
	try{
	IOSDriver<WebElement> IosDriver=(IOSDriver)this.appiumDriver;
	int elements = IosDriver.findElements(By.xpath("//XCUIElementTypeStaticText[contains(@name,'WEEK')]/../following::XCUIElementTypeCell/XCUIElementTypeButton")).size();
	System.out.println(elements);
	if (elements==1) {
	selectTip(); 
	}
	else if (elements==2) {
	selectTip();
	selectMyPlanLink1();

	}
	else if (elements==3) {
	selectTip();
	selectMyPlanLink1();
	selectMyPlanLink2(); 
	}
	}
	catch(Exception e)
	{
	e.printStackTrace();
	} catch (Throwable e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	} 
	}
	
	public void selectTip(){
		try{
		waitTillElementToBeClickble(StylesClimatePageIOS.selectCheckbox, "Click checkbox");
		click(StylesClimatePageIOS.selectCheckbox, "Click checkbox");
		Waittime();
		type(StylesClimatePageIOS.entertext,"Given rating","Enter comments for rating");
		Waittime();
		click(StylesClimatePageIOS.ratebtn, "Click rate button");
		click(StylesClimatePageIOS.congratsTipPopup, "Close Congrats popup");	
		Waittime();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	}

	public void selectMyPlanLink1(){
		try{
		Waittime();
		appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
		waitForVisibilityOfElement(StylesClimatePageIOS.selectLink1, "Click checkbox", 80);
		click(StylesClimatePageIOS.selectLink1, "Click the link");
		click(StylesClimatePageIOS.editbtn, "Click edit button");
		click(StylesClimatePageIOS.datedrpdwn, "Click the date dropdown");
		//select the new date from calender
		click(StylesClimatePageIOS.selectokbtn, "Click ok button");
		click(StylesClimatePageIOS.calenderDone, "Click done button");
		Waittime();
		appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
		click(StylesClimatePageIOS.savebtn, "Click save button");
		Waittime();
		waitTillElementToBeClickble(StylesClimatePageIOS.navigatebackbtn, "Click back button");
		click(StylesClimatePageIOS.navigatebackbtn, "Click back button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	
	public void selectMyPlanLink2(){
		try{
		Waittime();
		appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);

		click(StylesClimatePageIOS.selectLink2, "Click the link");
		click(StylesClimatePageIOS.editbtn, "Click edit button");
		click(StylesClimatePageIOS.namedrpdwn, "Select the name dropdown");
		//select the name from dropdown
		click(StylesClimatePageIOS.selectokbtn, "Click ok button");
		click(StylesClimatePageIOS.calenderDone, "Click done button");
		Waittime();
		appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
		click(StylesClimatePageIOS.savebtn, "Click save button");
		Waittime();
		click(StylesClimatePageIOS.navigatebackbtn, "Click back button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	}

}