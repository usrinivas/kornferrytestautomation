package com.haygroup.ios.libs;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import com.gallop.utilities.Xls_Reader;
import com.haygroup.ios.page.JourneyPageIOS;
import io.appium.java_client.MobileElement;

public class JourneyIOSLib extends HayGroupIOSCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");


	/**
	 * Author:@cigniti
	 * This function resets user data on preferences screen
	 */

	public void wipeUserdata() throws Throwable

	{
		try
		{
			click(JourneyPageIOS.unlockUserData,"performing click on text unlock");
			click(JourneyPageIOS.resetUserData,"performing click on text unlock");
			click(JourneyPageIOS.btnReset,"performing click on Reset button");
			Waittime();
			click(JourneyPageIOS.btnOK,"performing click on OK button");
			Waittime();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	} 

	/**
	 * Author:@cigniti
	 * This function performs click operartion on Yes button
	 */
	public void clickYesButton() {

		try {
			Waittime();
			click(JourneyPageIOS.btnYes, "click Yes  button");
			Waittime();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Author:@cigniti
	 * This function swipes down to window size on IOS device
	 */
	public void swipeResetUserData(){
		for(int i=0;i<=5;i++){
			try {
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 50);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	/**
	 * Author:@cigniti
	 * This function performs click operation on FriendlyObservations Link
	 */
	public void clickFriendlyObservations() {

		try {
			click(JourneyPageIOS.friendlyObservationsLink, "click friendly link");
			Waittime();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function peform a tap operatio on the screen
	 */
	public void tap()
	{
		WebElement ele=appiumDriver.findElement(By.xpath("//XCUIElementTypeStaticText[contains(@name,'When is the')]"));
		Point p=ele.getLocation();
		int x=p.getX();
		int y=p.getY();
		appiumDriver.tap(x, ele, y);
	}

	/**
	 * Author:@cigniti
	 * This function enters the meeting name into Meeting textbox
	 */
	public void enterMeetingName(String meetingName) throws Throwable
	{
		try
		{
			type(JourneyPageIOS.meetingNameTextBox, meetingName, "Entering a meeting name");
		}
		catch(Exception e)
		{
			e.printStackTrace();	
		}
	}

	/**
	 * Author:@cigniti
	 * This function enters the email into email textbox
	 */
	public void enterEmail(String email) throws Throwable
	{
		try
		{
			type(JourneyPageIOS.emailTextBox, email, "Entering a email");
		}
		catch(Exception e)
		{
			e.printStackTrace();	
		}
	}

	/**
	 * Author:@cigniti
	 * This function performs click operation on Next button
	 */
	public void clickNext() {

		try {
			click(JourneyPageIOS.nextButton, "click next Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function performs click operation on Confirm button
	 */
	public void clickConfirm() {

		try {
			click(JourneyPageIOS.confirmButton, "click confirm Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function performs click operation on back button
	 */
	public void clickBack() {

		try {
			click(JourneyPageIOS.backButton, "click back Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function performs click operation on done button
	 */
	public void clickDone() {

		try {
			click(JourneyPageIOS.doneButton, "click done Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Author:@cigniti
	 * This function performs click operation on Congratulations text
	 */
	public void clickCongratulations() {

		try {
			click(JourneyPageIOS.congratsButton, "click congrats Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function Launches the Elevator Pitch
	 */
	public void launchElevatorPitch() {

		try {
			click(JourneyPageIOS.launchElevatorPitch, "click Elevator Pitch");
			Waittime();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function enter the tiltle into Mytiltle textbox
	 */
	public void selectMyTitle(String name){
		try{
			isApplicationReady();
			waitTillElementToBeClickble(JourneyPageIOS.selectTitle,"select title Icon");
			click(JourneyPageIOS.selectTitle, "select title Icon");
			type(JourneyPageIOS.titleTextBox, name, "Entering a Title");
			waitTillElementToBeClickble(JourneyPageIOS.saveBtn,"click ok Button");
			click(JourneyPageIOS.saveBtn, "click save Button");
			click(JourneyPageIOS.titleOkBtn, "click ok Button");

		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}
	/**
	 * Author:@cigniti
	 * This function changes the Language from English(United states) to Chinese(Simplified)
	 */

	public void languagechange_mobile(String languagechange, String defaultLanguage) {
		try{
			Waittime();
			waitTillElementToBeClickble(JourneyPageIOS.inputLanguage(defaultLanguage), "click on Preffered languages");
			click(JourneyPageIOS.inputLanguage(defaultLanguage), "click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageIOS.inputLanguage(languagechange), "click on Preffered languages");
			click(JourneyPageIOS.inputLanguage(languagechange), "click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageIOS.saveBtn, "save Button");
			click(JourneyPageIOS.saveBtn, "save Button");
			click(JourneyPageIOS.signOutBtn, "sign out Button");
			Waittime();
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	/**
	 * Author:@cigniti
	 * This function changes the Language from Chinese(Simplified) to English(United states)
	 */
	public void languagechangeforChina_mobile(String changedLanguage, String defaultLanguage) {
		try{
			waitTillElementToBeClickble(JourneyPageIOS.inputLanguage(changedLanguage), "click on Preffered languages");
			click(JourneyPageIOS.inputLanguage(changedLanguage), "click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageIOS.inputLanguage(defaultLanguage), "click on Preffered languages");
			click(JourneyPageIOS.inputLanguage(defaultLanguage), "click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageIOS.saveBtn, "save Button");
			click(JourneyPageIOS.saveBtn, "save Button");
			click(JourneyPageIOS.signOutBtn, "sign out Button");
			Waittime();
			Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	/**
	 * Author:@cigniti
	 * This function Launches the Priorities
	 */
	public void launchPriorities() throws Throwable

	{
		try
		{
			click(JourneyPageIOS.launchPriorities,"Launch priorities");

		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	} 

	/**
	 * Author:@cigniti
	 * This function enters the task names
	 */
	public void enterTasks(String task1,String task2) 
	{
		try{
			Waittime();
			type(JourneyPageIOS.task1, task1, "Entering a task1");
			hideIOSKeyBoard();
			type(JourneyPageIOS.task2, task2, "Entering a task2");
			hideIOSKeyBoard();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	/**
	 * Author:@cigniti
	 * This function gives rating to all the tasks
	 */
	public void taskRating(){
		try{
			Waittime();
			List<MobileElement> list = appiumDriver.findElements(By.className("XCUIElementTypeSegmentedControl"));
			int count=list.size();

			for(int i=0;i<list.size();i++){
				list.get(i).click();

			}
		}

		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	/**
	 * Author:@cigniti
	 * This function Enters the  email into manager email textbox
	 */
	public void enterManagersEmail(String managersEmail) throws Throwable{
		type(JourneyPageIOS.managersEmail, managersEmail, "Enter managers email");
		hideIOSKeyBoard();
		click(JourneyPageIOS.sendEmailBtn,"send email button ");

	}

	/**
	 * Author:@cigniti
	 * This function performs the click operation on record button
	 */
	public void clickRecordButton() {

		try {

			click(JourneyPageIOS.recordBtn, "click record Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function performs the click operation on play button
	 */
	public void clickPlayButton() {

		try {
			Waittime();
			waitTillElementToBeClickble(JourneyPageIOS.playBtn, "click play Button");
			click(JourneyPageIOS.playBtn, "click play Button");
			Waittime();			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function waits for a particular amount of time
	 */
	public void PlayerWaitTime() {
		try{
			for(int i=1;i<=10;i++){
				Waittime();
			}
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function launches Fly on the wall 
	 */
	public void launchFlyontheWall() throws Throwable

	{
		try
		{
			click(JourneyPageIOS.launchFlyontheWall,"Launch Fly on the wall");

		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	} 

	/**
	 * Author:@cigniti
	 * This function performs click operations Fly on the wall play button
	 */
	public void clickFlyontheWallPlayButton() throws Throwable

	{
		try
		{
			click(JourneyPageIOS.flyOnTheWallPlayBtn,"Launch Fly on the wall play button");

			WebElement nexttrack=appiumDriver.findElement(JourneyPageIOS.nextTrack);
			nexttrack.click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * Author:@cigniti
	 * This function performs click operations on star
	 */
	public void clickStar() {

		try {
			click(JourneyPageIOS.starImg, "click star image");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function swipes down the IOS device
	 */
	public void scrollDown()
	{

		WebElement demon=appiumDriver.findElement(JourneyPageIOS.demonstrationText);
		Point p=demon.getLocation();

		WebElement nexttext=appiumDriver.findElement(JourneyPageIOS.nextMeetingText);
		Point p_nextText=nexttext.getLocation();

		appiumDriver.swipe( p_nextText.x, p_nextText.y,p.x, p.y, 300);

	}

	/**
	 * Author:@cigniti
	 * This function performs tap operation to select a rating in rating screen
	 */
	public void tapRatingFlyOnTheWall() 
	{
		try{
			Waittime();
			List<MobileElement> list = appiumDriver.findElements(By.className("XCUIElementTypeSegmentedControl"));
			int count=list.size();

			for(int i=0;i<list.size();i++){
				if(i==2)
				{
					appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				}
				if(i==5)
				{
					appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				}

				if(i==7)
				{
					appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				}

				list.get(i).click();
			}
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function Enters the email address on Fly on the wall screen
	 */
	public void enterEmailAddressInFlyOnTheWall(String emailAddress1,String emailAddress2,String emailAddress3){
		try{
			Waittime();
			type(JourneyPageIOS.emailAddress1,emailAddress1, "entering email Address1");
			hideIOSKeyBoard();
			type(JourneyPageIOS.emailAddress2, emailAddress2, "entering email Address2");
			hideIOSKeyBoard();	
			type(JourneyPageIOS.emailAddress3, emailAddress3, "entering email Address3");
			hideIOSKeyBoard();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
