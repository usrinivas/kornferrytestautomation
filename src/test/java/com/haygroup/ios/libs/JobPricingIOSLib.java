package com.haygroup.ios.libs;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.ios.page.JobPricingPageIOS;
import com.haygroup.ios.page.JourneyPageIOS;


public class JobPricingIOSLib extends HayGroupIOSCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");


	/**
	 * This function performs click operation on location dropdown
	 * 	 
	 * 
	 */
	
	public void wipeUserdata() throws Throwable
	
	{
		try
		{
			click(JourneyPageIOS.unlockUserData,"performing click on text unlock");
			click(JourneyPageIOS.resetUserData,"performing click on text unlock");
			click(JourneyPageIOS.btnReset,"performing click on Reset button");
			Waittime();
            click(JourneyPageIOS.btnOK,"performing click on OK button");
            Waittime();

		}
		catch(Exception e)
		{
			System.out.println(e);
		}

		} 

public void clickYesButton() {
		
		try {
			click(JobPricingPageIOS.selectYesbtn, "click Yes  button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
public void selectJobpricing(String Data){
	
	try {
		click(JobPricingPageIOS.jobpricingicon, "click jobpricing icon");
		type(JobPricingPageIOS.entertext, Data, "Entered the text");
		click(JobPricingPageIOS.headertext,"Click header");
		
	} 
	catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
	
	public void selectjobfamily()
	{
		try {
			click(JobPricingPageIOS.selectGrade,"select grade");
			click(JobPricingPageIOS.selectJobfamily,"click jobfasmily");
			click(JobPricingPageIOS.selectsubfamily,"Click subfamily");
	
	}
	catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
	public void addLocation(String data)
	{
		try {
			
			click(JobPricingPageIOS.addLocation,"Add location");
			waitTillElementToBeClickble(JobPricingPageIOS.Enteralocation, "Enter location");
			type(JobPricingPageIOS.Enteralocation,data,"Enter location");
			waitTillElementToBeClickble(JobPricingPageIOS.resultItem, "select the item");
			click(JobPricingPageIOS.resultItem,"Click subfamily");
			return ;
	}
	catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return;
	}
}
	
	public void removeddLocation(String location) 
	{
	  try
	  {
		 waitTillElementToBeClickble(JobPricingPageIOS.removeitem, "Removeitem");
		 click(JobPricingPageIOS.removeitem, "remove item "); 
		 click(JobPricingPageIOS.donebtn,"Done button");
		 click(JobPricingPageIOS.savebtn,"Naviagte to pricingscanrios");
		 
	  }
	  catch( Throwable e){
		  e.printStackTrace();
	  }
		 		
	}
	
	@SuppressWarnings("finally")
	public String selectEmployeeandSaveWithSalaryIdentifier(String text)
	{
		try {		
			text = text + generateRandomNumber();
			Waittime();
			type(JobPricingPageIOS.enterPricingtext, text,"enter the text");
			click(JobPricingPageIOS.selectempCheckBox,"click emp name");
			click(JobPricingPageIOS.savebtn,"clicko on save button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
		return text;
		}
	}
	
	public void navigateToSalarySimulator()
	{
		try {		
			waitTillElementToBeClickble(JobPricingPageIOS.salarySimulatorIcon, "Salary Simulator Icon");
			click(JobPricingPageIOS.salarySimulatorIcon, "Salary Simulator Icon");	
		}
		catch (Throwable e){
			e.printStackTrace();}
	}

	public void fillSalarySimulator(String annualSalary, String baseSalary, String totalSalary)
	{
		try {		
			waitTillElementToBeClickble(JobPricingPageIOS.annnualCashTextField, "Annual Salary Text Field");
			type(JobPricingPageIOS.annnualCashTextField, annualSalary, "Annual Salary Text Field");	
			type(JobPricingPageIOS.baseSalaryTextFierld, baseSalary, "Base Salary Text Field");	
			type(JobPricingPageIOS.totalCashTextField, totalSalary, "Total Salary Text Field");	
			click(JobPricingPageIOS.nextbtn,"Navigate to PricingBreakdown");
			click(JobPricingPageIOS.nextbtn,"Navigate to savesimulation");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("finally")
	public String selectSimulateEmployeesave(String text)
	{
		try {	

			text = text + generateRandomNumber();
			type(JobPricingPageIOS.simulationname, text,"Simulationscenarioname");	
			click(JobPricingPageIOS.selectsimempCheckBox, "employeecheckbox selected");	
			click(JobPricingPageIOS.savebtn, "Savesimulationdetails");	
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			return text;
		}
	
	}
	
	public void validateSalaryInfo(String salaryIdentifier)
	{
		try {		
			waitTillElementToBeClickble(JobPricingPageIOS.homeIcon, "Home Icon");
			click(JobPricingPageIOS.homeIcon, "Home Icon");
			waitTillElementToBeClickble(JobPricingPageIOS.myPeople, "My People");
			click(JobPricingPageIOS.myPeople, "My People");
			waitTillElementToBeClickble(JobPricingPageIOS.byname, "By Name");
			click(JobPricingPageIOS.byname, "By Name");
			waitTillElementToBeClickble(JobPricingPageIOS.employeeAtSalaryValidation, "Employee");
			click(JobPricingPageIOS.employeeAtSalaryValidation, "Employee");
			click(JobPricingPageIOS.homeIcon,"Click Home icon");
			
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
