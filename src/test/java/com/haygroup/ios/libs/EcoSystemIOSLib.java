package com.haygroup.ios.libs;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.ios.page.EcoSystemPageIOS;
import com.haygroup.web.page.EcoSystemPage;

import io.appium.java_client.ios.IOSDriver;


public class EcoSystemIOSLib extends HayGroupIOSCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");


	/**
	 * Author:@cigniti
	 * This function changes the language
	 */

	public void languagechange_mobile_IOS(String languagechange ) {
		try{
		waitTillElementToBeClickble(EcoSystemPageIOS.selectPreferedLanguage,"click on Preffered languages");
		click(EcoSystemPageIOS.selectPreferedLanguage,"click on Preffered languages");
		waitTillElementToBeClickble(EcoSystemPageIOS.inputLanguage(languagechange),"click on Preffered languages");
		click(EcoSystemPageIOS.inputLanguage(languagechange),"click on Preffered languages");
		waitTillElementToBeClickble(EcoSystemPageIOS.saveBtn,"Save Button");
		click(EcoSystemPageIOS.saveBtn,"Save Button");
		waitTillElementToBeClickble(EcoSystemPageIOS.okBtn,"OK Button");
		click(EcoSystemPageIOS.okBtn,"OK Button");
		Waittime();
		Waittime();
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function clicks on contact Administrator
	 */
	public void clickContactAdministrator() {
      try{
		waitTillElementToBeClickble(EcoSystemPageIOS.clickContact,"click on contact Administrator");
		click(EcoSystemPageIOS.clickContact,"click on contact");
      }
      catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Author:@cigniti
	 * This function Enters the message
	 */
	public void enterMessage(String message){
		try{
		Waittime();
		type(EcoSystemPageIOS.administratorTextBox,message, "entering message");
		waitTillElementToBeClickble(EcoSystemPageIOS.clickSendButton,"click on send button");
		click(EcoSystemPageIOS.clickSendButton,"click on send button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Author:@cigniti
	 * This function Enters the location
	 */
	public void enterLocation(String location) throws Throwable
	{
		try
		{
			click(EcoSystemPageIOS.editbtn, "Click Edit button");
			type(EcoSystemPageIOS.enterlocation,location,"Enter a location");
			Waittime();
			click(EcoSystemPageIOS.clicklocation,"Click entered location");

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Author:@cigniti
	 * This function clicks the preferred currency
	 */
	public void preferedCurrency() throws Throwable
	{
		try
		{
			click(EcoSystemPageIOS.userpreferedCurrency,"Currency selected");
			click(EcoSystemPageIOS.currency1,"Currency one selected");
			click(EcoSystemPageIOS.saveBtn,"Click on Save button");		
			Waittime();		
			click(EcoSystemPageIOS.userpreferedCurrency,"Currency selected");
			click(EcoSystemPageIOS.currency2,"Currency one selected");
			click(EcoSystemPageIOS.saveBtn,"Click on Save button");
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
						e.printStackTrace();
		}

	}

	/**
	 * Author:@cigniti
	 * This function clicks the current timezone
	 */
	public void userpreferredTimezone()  throws Throwable
	{
		try {
			click(EcoSystemPageIOS.userpreferredTimezone,"Selectpreferedtimezone");
			click(EcoSystemPageIOS.timeZone1,"Select timezone1");
			click(EcoSystemPageIOS.saveBtn,"Click on Save button");
			Waittime();
			click(EcoSystemPageIOS.userpreferredTimezone,"Selectpreferedtimezone");
			click(EcoSystemPageIOS.timeZone2,"Select timezone2");
			click(EcoSystemPageIOS.saveBtn,"Click on Save button");

		} catch (Exception e) {
			// TODO Auto-generated catch block
						e.printStackTrace();
		}	
	}

	
	public void getNotifications(){
		try{
	IOSDriver<WebElement> IosDriver=(IOSDriver)this.appiumDriver;
	List<WebElement> elements = IosDriver.findElements(By.xpath("//XCUIElementTypeCell"));
	System.out.println(elements.size());
	
	for(int i=0;i<=elements.size();i++){
		
		if ( !IosDriver.findElement(By.xpath("(//XCUIElementTypeCell)[i]")).isSelected() )
		{
			elements.get(i).click();
		}
		
	}
		}
		catch (Exception e) {
			// TODO: handle exception
		}	
		

	}
	
	/**
	 * Author:@cigniti
	 * This function clicks the notifications
	 */
	public void notifications() throws Throwable
	{

		try
		{
			isApplicationReady();
			click(EcoSystemPageIOS.userpreference,"Click notifications");
		     //getNotifications();
			click(EcoSystemPageIOS.sendMeNotifications,"Click notifications");
			click(EcoSystemPageIOS.saveBtn, "click on Save button");
			Waittime();
			click(EcoSystemPageIOS.userpreference,"Click notifications");
			click(EcoSystemPageIOS.doNotSendMeNotifications,"Click notifications");
			click(EcoSystemPageIOS.saveBtn, "click on Save button");

		} catch (Exception e) {
			// TODO: handle exception
		}	
	}

	/**
	 * Author:@cigniti
	 * This function clicks the notifications
	 */
	public void MyPlanNotifications()
	{
		try
		{
			click(EcoSystemPageIOS.userPreferenceforActionplan,"Click send daily notofications");
			click(EcoSystemPageIOS.doNotSendMeNotifications,"Click on Save button");
			click(EcoSystemPageIOS.saveBtn,"Click on Save button");
			Waittime();
			click(EcoSystemPageIOS.userPreferenceforActionplan,"Click send daily notofications");
			click(EcoSystemPageIOS.sendmedailynotifications,"Click on Save button");
			click(EcoSystemPageIOS.saveBtn,"Click on Save button");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Author:@cigniti
	 * This function clicks the week starting day
	 */
	public void weekStartingDay(String WeekStartingDay)
	{
		try
		{
			click(EcoSystemPageIOS.weekStartingDay,"select week startingday");
			
			click(EcoSystemPageIOS.saveBtn,"click on save button");
			click(EcoSystemPageIOS.closetinybtn,"click on close tiny button");
			Waittime();
			click(EcoSystemPageIOS.backBtn, "Back to Myplanpreferenceforweekstartingday");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void weekEndingDay(String WeekEndingDay)
	{
		try
		{
			click(EcoSystemPageIOS.weekEndingDay,"Click weekEndingDay");
			//click(EcoSystemPageIOS.inputWeekEndingDay(WeekEndingDay),"select week ending day");
			click(EcoSystemPageIOS.saveBtn, "Click on  save button");
			click(EcoSystemPageIOS.closetinybtn,"click on close tiny button");
			click(EcoSystemPageIOS.backBtn,"Back to MyplanpreferenceforweekEndingday");
		}

		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Author:@cigniti
	 * This function clicks the team Meeting Day
	 */
	public void teamMeetingDay(String Teammeeting)
	{
		try
		{
			click(EcoSystemPageIOS.teamMeetingDay,"Click weekStartingDay");
			click(EcoSystemPageIOS.saveBtn, "Click on  save button");
			click(EcoSystemPageIOS.closetinybtn,"click on close tiny button");
			click(EcoSystemPageIOS.backBtn,"Back to teammeeting day");
		}

		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Author:@cigniti
	 * This function clicks the team Meeting time
	 */
	public void teammeetingtime(String teammeetingtime)
	{
		try
		{
			click(EcoSystemPageIOS.Teammeetingtime,"Click teammeeting time");
			click(EcoSystemPageIOS.saveBtn, "Click on  save button");
			click(EcoSystemPageIOS.closetinybtn,"click on close tiny button");
		}

		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Author:@cigniti
	 * This function clicks the time
	 */
	public void time(String time) 
	{
		try
		{
			click(EcoSystemPageIOS.time,"Click  time");
			click(EcoSystemPageIOS.saveBtn, "Click on  save button");
		}

		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}	

	/**
	 * Author:@cigniti
	 * This function clicks the done button
	 */
	public  void clickDoneBtn(){
		try{
			waitTillElementToBeClickble(EcoSystemPageIOS.doneBtn, "click on Done button");
			click(EcoSystemPageIOS.doneBtn,"click on done button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Author:@cigniti
	 * This function clicks the done button
	 */
	public  void click() {
		try{
			waitTillElementToBeClickble(EcoSystemPageIOS.doneBtn, "click on Done button");
			click(EcoSystemPageIOS.doneBtn,"click on done button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}





