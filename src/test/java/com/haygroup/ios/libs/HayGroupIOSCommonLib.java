package com.haygroup.ios.libs;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.gallop.accelerators.ActionEngine;
import com.haygroup.ios.page.EcoSystemPageIOS;
import com.haygroup.ios.page.HomePageIOS;
import com.haygroup.ios.page.JourneyPageIOS;
import com.haygroup.ios.page.LoginPageIOS;
import com.haygroup.web.page.HomePage;
import io.appium.java_client.MobileElement;

public class HayGroupIOSCommonLib extends ActionEngine{

	/*	Login to application with User Name and Password*/
	public void loginToIOSApplication(String UserName, String Password) throws Throwable {	
		appiumDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		//appiumDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		//verifyLogOutBtn();
		waitTillElementToBeClickble(LoginPageIOS.userName_mobile, "entering login username");
		type(LoginPageIOS.userName_mobile,UserName, "entering login username");
		type(LoginPageIOS.password_mobile, Password, "login password");
		click(LoginPageIOS.loginBtn_mobile, "Login button");
	}

	public void loginToJourneyIOSApplication(String UserName, String Password) throws Throwable {	
		waitTillElementToBeClickble(LoginPageIOS.userName_mobile, "entering login username");
		type(LoginPageIOS.userName_mobile,UserName, "entering login username");
		type(LoginPageIOS.password_mobile, Password, "login password");
		click(LoginPageIOS.loginBtn_Journey, "Login button");

	}

	public void optimizeforChina(String UserName, String Password){
		try {
			waitTillElementToBeClickble(LoginPageIOS.userName_mobile, "entering login username");
			type(LoginPageIOS.userName_mobile,UserName, "entering login username");
			type(LoginPageIOS.password_mobile, Password, "login password");
			waitTillElementToBeClickble(EcoSystemPageIOS.optimizeforChina, "optimizeforChina button");
			click(EcoSystemPageIOS.optimizeforChina, "optimizeforChina button");
			click(LoginPageIOS.loginBtn_mobile, "Login button");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void journey_optimizeforChina(String UserName, String Password){
		try {
			int k=6;
			waitTillElementToBeClickble(LoginPageIOS.userName_mobile, "entering login username");
			type(LoginPageIOS.userName_mobile,UserName, "entering login username");
			type(LoginPageIOS.password_mobile, Password, "login password");
			waitTillElementToBeClickble(JourneyPageIOS.optimizeforChina, "optimizeforChina button");
			click(JourneyPageIOS.optimizeforChina, "optimizeforChina button");
			click(LoginPageIOS.loginBtn_Journey, "Login button");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void verifyLogOutBtn(){
		if(verifyElementPresent(HomePageIOS.activateLogOutMobile, "Logout btn")){
			try {
				waitTillElementToBeClickble(HomePageIOS.activateLogOutMobile, "click on logout button");
				click(HomePageIOS.activateLogOutMobile, "Logout button");
				click(EcoSystemPageIOS.okBtn, "OK button");
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		}
		else{
			System.out.println("Element not present");
		}
	}

	public void mobileSettingsPageButtonIOS() {
		try {
			waitTillElementToBeClickble(EcoSystemPageIOS.mobileSettingsButton, "Settings button");
			click(EcoSystemPageIOS.mobileSettingsButton, "Settings button");
		} catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickSettings() {

		try {
			Waittime();
			Waittime();
			if (verifyElementPresent(JourneyPageIOS.btnGetStarted, "Get Started")){
				click(JourneyPageIOS.btnGetStarted, "performing click on get started button");
				waitForElementToBeInvisible(appiumDriver,JourneyPageIOS.btnGetStarted, 3000);
			}
			else
				click(JourneyPageIOS.journeySettingsButton, "click on settings Button");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void notifyIconsJourneyIOS() {
		try {
			click(EcoSystemPageIOS.notifyIcon_rightScreen, "notifyIcon_rightScreen button");
		} catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void verifyGetContextHandles(){
		Set<String> contextNames = this.appiumDriver.getContextHandles();
		for (String contextName : contextNames) {
			System.out.println(contextName);
		}
	}

	public void clickDoneButton(){
		try {
			waitTillElementToBeClickble(EcoSystemPageIOS.doneBtn,"Wait for Done btn to be clickable");
			click(EcoSystemPageIOS.doneBtn,"click on menu");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*Log out from application*/
	public void logOutFromApplication() throws Throwable{
		JSClick(HomePage.logOutButton, "Log out");
	}

	public void logOutMobileApplication(){
		try{
		waitTillElementToBeClickble(HomePageIOS.activateLogOutMobile, "Log out Mobile");
		click(HomePageIOS.activateLogOutMobile, "Log out Mobile");
		click(HomePageIOS.btnConfirmLogout, "Log out Mobile");
		Waittime(); 
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void journeyLogOutMobileApplication(){
		try{
		waitTillElementToBeClickble(HomePageIOS.Logout_mobile, "Log out Mobile");
		click(HomePageIOS.Logout_mobile, "Log out Mobile");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Launchstart()

	{
		try
		{

			click(JourneyPageIOS.launchStart,"LaunchStart");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} 

	public void clickstart() throws Throwable
	{

		Waittime();
		List<MobileElement> list = appiumDriver.findElements(By.className("XCUIElementTypeSegmentedControl"));
		int count=list.size();
		for(int i=0;i<list.size();i++){
			if(i==2)
			{
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			}
			if(i==5)
			{
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			}

			if(i==7)
			{
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			}
			if(i==9)
			{
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			}

			list.get(i).click();
		}

	}
}
