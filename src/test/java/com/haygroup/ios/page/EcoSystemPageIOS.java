package com.haygroup.ios.page;

import org.openqa.selenium.By;

public class EcoSystemPageIOS {

	public static By mobileSettingsButton;
	public static By selectPreferedLanguage;
	public static By okBtn;
	public static By languageIcon;
	public static By saveBtn;
	public static By dynamicLocator;
	public static By doneBtn;
	public static By optimizeforChina;
	public static By notifyIcon_rightScreen;
	public static By zoomBtn;
	public static By clickContact;
	public static By administratorTextBox;
	public static By clickSendButton;
	public static By selectCheckbox;
	public static By editbtn;
	public static By enterlocation;
	public static By clicklocation;
	public static By userPrefferedlanguage;
	public static By selectLanguage;
	public static By lsavebtn;
	public static By userpreferedCurrency;
	public static By csavebtn;
	public static By popupOKBtn;
	public static By userpreferredTimezone;
	public static By selectzone;
	public static By userpreference;
	public static By sendmenotifications;
	public static By userPreferenceforActionplan;
	public static By sendmedailynotifications;
	public static By weekStartingDay;
	public static By day;
	public static By weekEndingDay;
	public static By weekEndDay;
	public static By teamMeetingDay;
	public static By tmDay;
	public static By Teammeetingtime;
	public static By time;
	public static By ResetuserData;
	public static By myplanpreference;
	public static By userpreferencetoreceivemailnotification;
	public static By selectCurrency;
	public static By selectTimezone;
	public static By selectemailnotifications;
	public static By selectActionPlanreminders;
	public static By selectWeekStaringDay;
	public static By selectWeekEndingDay;
	public static By selectTeammeeting;
	public static By Myplanpreferenceforweekstartingday;
	public static By MyplanpreferenceforweekEndingday;
	public static By Teammeetingday;
	public static By backBtn;
	public static By closetinybtn;
	public static By currency1;
	public static By currency2;
	public static By timeZone1;
	public static By timeZone2;
	public static By sendMeNotifications;
	public static By doNotSendMeNotifications;
//	public static By startingDay1;
	//public static By startingDay1;
	public static By Day1;
	public static By startingDay1;
	

	static {

		mobileSettingsButton=By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton");
		notifyIcon_rightScreen = By.xpath("//AppiumAUT/XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[20]/XCUIElementTypeOther");
		zoomBtn = By.id("minus");
		selectPreferedLanguage = By.xpath("//XCUIElementTypeCell[1]");
		saveBtn = By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton");
		okBtn = By.xpath("//XCUIElementTypeAlert/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton");
		doneBtn = By.id("Done");
		optimizeforChina = By.id("Optimize for China");
		clickContact = By.id("Contact Administrator");
		administratorTextBox = By.xpath("//XCUIElementTypeOther/XCUIElementTypeTextView");
		clickSendButton = By.id("Send");
		selectCheckbox = By.className("XCUIElementTypeCell");
		editbtn=By.xpath("(//XCUIElementTypeButton[@name='Edit'])[1]");
		enterlocation=By.id("Enter a location");
		clicklocation = By.id("Bangalore,Karnataka,IN");
		userPrefferedlanguage=By.name("User Preferred Language");
		selectLanguage=By.xpath("//XCUIElementTypeStaticText[@name='English']");
		lsavebtn=By.id("Save");
		userpreferedCurrency=By.name("User Preferred Currency");
		csavebtn=By.id("Save");
		popupOKBtn = By.id("OK");
		userpreferredTimezone=By.id("User Preferred Time Zone");
		userpreference=By.id("User preference to receive email notifications");
		sendmenotifications=By.id("Send me notifications as and when required");
		userPreferenceforActionplan=By.id("User Preference for Action Plan Reminders by Email Notifications");
		sendmedailynotifications=By.id("Send me notifications on a daily basis");

		
		weekStartingDay=By.id("My Plan Preference for Week Starting Day");
		day=By.id("Monday");
		weekEndingDay=By.id("My Plan Preference for Week Ending Day");
		weekEndDay=By.id("Friday");
		teamMeetingDay=By.id("My Plan Preference for Team Meeting Day");
		tmDay=By.id("Wednesday");
		Teammeetingtime=By.id("Team Meeting Time");
		time=By.id("1:00 AM");
		ResetuserData=By.id("RESET USER DATA");	
		myplanpreference=By.id("My Plan Preference for Week Ending Day");
		userpreferencetoreceivemailnotification=By.id("User preference to receive email notifications");
		Myplanpreferenceforweekstartingday=By.id("My Plan Preference for Week Starting Day");
		MyplanpreferenceforweekEndingday= By.id("My Plan Preference for Week Ending Day");
		Teammeetingday=By.id("My Plan Preference for Team Meeting Day");
		backBtn = By.className("XCUIElementTypeImage");
		closetinybtn = By.id("btn close tiny");
		currency1 = By.xpath("//XCUIElementTypeStaticText[contains(@name,'Belize')]");
		currency2 = By.xpath("//XCUIElementTypeStaticText[contains(@name,'Baht')]");
		timeZone1 = By.xpath("//XCUIElementTypeStaticText[contains(@name,'Asmara')]");
		timeZone2 = By.xpath("//XCUIElementTypeStaticText[contains(@name,'Accra')]");
		sendMeNotifications = By.xpath("//XCUIElementTypeStaticText[contains(@name,'Send me notifications')]");
		doNotSendMeNotifications = By.xpath("//XCUIElementTypeStaticText[contains(@name,'Do not send')]");
	}

	public static By inputLanguage(String language){

		return languageIcon = By.name(language);
	}
	public static By inputTypeLanguage(String language){

		return languageIcon = By.id(language);
	}
	public static By getDynamicLocatorByID(String locator){

		return dynamicLocator = By.id(locator);
	}

	public static By inputemailnotifications (String emailnotifications)
	{
		return selectemailnotifications=By.xpath("//android.widget.TextView[@text='"+emailnotifications+"']");

	}

}
