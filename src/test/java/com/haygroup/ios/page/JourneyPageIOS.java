package com.haygroup.ios.page;

import org.openqa.selenium.By;

public class JourneyPageIOS {

	public static By journeySettingsButton;
	public static By optimizeforChina;
	public static By dynamicLocator;
	public static By resetUserData;
	public static By unlockUserData;
	public static By btnReset;
	public static By btnOK;
	public static By btnYes;
	public static By btnGetStarted;
	public static By friendlyObservationsLink;
	public static By meetingNameTextBox;
	public static By emailTextBox;
	public static By doneButton;
	public static By nextButton;
	public static By backButton;
	public static By launchStart;
	public static By strengthsandweakness;
	public static By launchElevatorPitch;
	public static By selectTitle;
	public static By titleTextBox;
	public static By saveBtn;
	public static By titleOkBtn;
	public static By currentTitle;
	public static By languageIcon;
	public static By signOutBtn;
	public static By preferencesText;
	public static By confirmButton;
	public static By congratsButton;
	public static By clickText;
	public static By launchPriorities;
	public static By task1;
	public static By task2;
	public static By managersEmail;
	public static By sendEmailBtn;
	public static By recordBtn;
	public static By playBtn;
	public static By launchFlyontheWall;
	public static By flyOnTheWallPlayBtn;
	public static By starImg;
	public static By demonstrationText;
	public static By nextMeetingText;
	public static By emailAddress1;
	public static By emailAddress2;
	public static By emailAddress3;
	public static By trackPosition;
    public static By nextTrack;
		
	
	static {

		journeySettingsButton=By.id("gear");
		optimizeforChina = By.id("Optimize for China");
		resetUserData = By.id("Reset User Data");
		unlockUserData = By.xpath("//XCUIElementTypeSwitch[25]");
		btnReset = By.xpath("//XCUIElementTypeButton[@name='Reset']");
		btnOK = By.id("OK");
		btnYes = By.id("YES");
		btnGetStarted = By.xpath("//XCUIElementTypeButton[@name='GET STARTED']");
		friendlyObservationsLink=By.id("Launch Friendly Observation");
		meetingNameTextBox=By.xpath("//XCUIElementTypeStaticText[contains(@name,'Meeting Name')]/following-sibling::XCUIElementTypeTextField");
		emailTextBox=By.xpath("//XCUIElementTypeTextField[@value='Enter email address here']");
		doneButton=By.id("Done");
		congratsButton=By.id("CONGRATULATIONS!");
		nextButton = By.id("Next");
		confirmButton = By.id("Confirm");
		backButton = By.id("Back");
		launchStart = By.id("Launch Start");
		strengthsandweakness = By.id("");
		launchElevatorPitch = By.id("Launch Elevator Pitch");
		selectTitle = By.id("Title");
		titleTextBox = By.className("XCUIElementTypeTextField");
		saveBtn = By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton[3]");
		clickText = By.xpath("//XCUIElementTypeStaticText[contains(@name,'When is the')]");
		titleOkBtn = By.id("OK");
		signOutBtn = By.xpath("(//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton)[2]");
		preferencesText = By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeStaticText");
		launchPriorities = By.id("Launch Priorities");
		task1 = By.xpath("//XCUIElementTypeStaticText[@name='Task 1']/following-sibling::XCUIElementTypeTextField");
		task2 = By.xpath("//XCUIElementTypeStaticText[@name='Task 2']/following-sibling::XCUIElementTypeTextField");
		//managersEmail = By.xpath("//XCUIElementTypeTextField[contains(@value,'Manager')]");
		managersEmail = By.xpath("//XCUIElementTypeButton[@name='Send Email']/parent::XCUIElementTypeOther/preceding-sibling::XCUIElementTypeOther[1]/XCUIElementTypeTextField");
		sendEmailBtn = By.id("Send Email");
		currentTitle = By.xpath("//XCUIElementTypeCell/XCUIElementTypeStaticText[@name='Title']/following-sibling::XCUIElementTypeStaticText");
		recordBtn = By.id("recordButton");
		playBtn = By.xpath("(//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeButton)[2]");
		launchFlyontheWall = By.id("Launch Fly On the Wall");
		flyOnTheWallPlayBtn = By.xpath("//XCUIElementTypeOther/XCUIElementTypeScrollView");
		starImg = By.id("star");
		demonstrationText = By.xpath("//XCUIElementTypeStaticText[contains(@name,'Are you demonstrating active')]");
		nextMeetingText = By.xpath("//XCUIElementTypeStaticText[contains(@name,'When is the next meeting')]");
		emailAddress1 = By.xpath("//XCUIElementTypeStaticText[@name='Email Address 1']/following-sibling::XCUIElementTypeTextField");
		emailAddress2 = By.xpath("//XCUIElementTypeStaticText[@name='Email Address 2']/following-sibling::XCUIElementTypeTextField");
		emailAddress3 = By.xpath("//XCUIElementTypeStaticText[@name='Email Address 3']/following-sibling::XCUIElementTypeTextField");
		trackPosition = By.id("TrackPosition");
		nextTrack = By.id("Next track");
	}
	
	public static By getDynamicLocatorByID(String locator){

		return dynamicLocator = By.id(locator);
	}
	
	public static By inputLanguage(String language){
		return languageIcon = By.xpath("//XCUIElementTypeStaticText[@name='"+language+"']");
			
	}


}
