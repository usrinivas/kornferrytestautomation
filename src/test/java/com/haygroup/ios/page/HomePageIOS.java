package com.haygroup.ios.page;

import org.openqa.selenium.By;

public class HomePageIOS {

	public static By activateLogOutMobile;
	public static By Logout_mobile;
	public static By btnConfirmLogout;

	/* Page Objects Of Home Page */
	static {
		activateLogOutMobile=By.xpath("//XCUIElementTypeScrollView//XCUIElementTypeImage/following-sibling::XCUIElementTypeButton");
		Logout_mobile = By.id("Log Out");
		btnConfirmLogout = By.xpath("(//XCUIElementTypeButton[@name='Log Out'])[2]");		
	}

}