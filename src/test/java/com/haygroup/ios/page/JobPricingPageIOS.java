package com.haygroup.ios.page;

import org.openqa.selenium.By;

public class JobPricingPageIOS {
	
	public static By jobpricingicon;
	public static By entertext;
	public static By headertext;
	public static By selectYesbtn;
	public static By selectGrade;
	public static By selectJobfamily;
	public static By selectsubfamily;
	public static By addLocation;
	public static By Enteralocation;
	public static By resultItem;
	public static By removeitem;	
	public static By donebtn;
	public static By savebtn;
	public static By enterPricingtext;
	public static By selectempCheckBox;
	public static By salarySimulatorIcon;
	public static By annnualCashTextField;
	public static By baseSalaryTextFierld;
	public static By totalCashTextField;
	public static By nextbtn;
	public static By simulationname;
	public static By selectsimempCheckBox;
	public static By homeIcon;
	public static By myPeople;
	public static By byname;
	public static By employeeAtSalaryValidation;

	
	static {
		
		jobpricingicon = By.id("Job Pricing");
		entertext =By.id("Enter Job Title or Employee Name");
		headertext=By.xpath("//XCUIElementTypeStaticText");
		selectYesbtn=By.id("Yes");
		selectGrade = By.xpath("//XCUIElementTypeStaticText[@value='19']");
		selectJobfamily=By.id("Administration / support / service v");
		selectsubfamily=By.id("Clerical Services v");
		addLocation=By.id("Add Location");
		Enteralocation=By.id("Enter a location");
		resultItem=By.xpath("//XCUIElementTypeStaticText[1]");
		removeitem=By.xpath("(//XCUIElementTypeButton)[3]");
		donebtn=By.id("Done");
		savebtn=By.id("Save");
		enterPricingtext=By.xpath("//XCUIElementTypeTextField[@value='Please enter a scenario name']");
		selectempCheckBox=By.xpath("//XCUIElementTypeStaticText[@name='David Sergovic']/following-sibling::XCUIElementTypeButton");
		salarySimulatorIcon = By.id("Salary Simulator");
		annnualCashTextField=By.xpath("//XCUIElementTypeTextField[1]");
		baseSalaryTextFierld=By.xpath("//XCUIElementTypeTextField[2]");
		totalCashTextField=By.xpath("//XCUIElementTypeTextField[3]");
		nextbtn=By.id("Next");
		simulationname=By.xpath("//XCUIElementTypeTextField[@value='Please enter a simulation name']");
		selectsimempCheckBox=By.xpath("//XCUIElementTypeStaticText[@name='David Sergovic']/following-sibling::XCUIElementTypeButton");
		homeIcon = By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]");		
		myPeople = By.id("My People");
		byname = By.id("By Name");
		employeeAtSalaryValidation = By.id("David Sergovic");
	}
	
}

