package com.haygroup.ios.page;

import org.openqa.selenium.By;

public class JobGradingPageIOS

{
	
	public static By jobGradingIcon;
	public static By enterJobtxtbox;
	public static By enterJob;
	public static By selectJobFamily;
	public static By selectSubFamily;
	public static By selectJobType;
	public static By gradeJobBtn;
	public static By applyGradeBtn;
	public static By selectEmpCheckBox;
	public static By applyGradetoEmpBtn;
	public static By enterScenarioName;
	public static By savethisGradeScenarioBtn;
	public static By homeIcon;
	public static By myPeople;
	public static By byname;
	public static By employeeAtGradeValidation;
	public static By okButton;
	public static By policyDevelopment;
	
static {
		
		jobGradingIcon = By.id("Job Grading");
		enterJobtxtbox=By.xpath("//XCUIElementTypeSearchField");
		enterJob=By.xpath("//XCUIElementTypeStaticText");
		selectJobFamily=By.id("Administration / support / service v");
		selectSubFamily=By.id("Clerical Services v");
		selectJobType=By.id("Office Manager v");
		gradeJobBtn=By.id("Grade Job");
		applyGradeBtn=By.id("Apply Grade");
		selectEmpCheckBox=By.xpath("//XCUIElementTypeStaticText[@name='David Sergovic']/following-sibling::XCUIElementTypeButton");
		applyGradetoEmpBtn=By.xpath("//XCUIElementTypeButton[@name='Apply Grade']");
		enterScenarioName=By.xpath("//XCUIElementTypeTextField[@value='Please enter a scenario name']");
		savethisGradeScenarioBtn=By.xpath("//XCUIElementTypeButton[@name='Save This Grading Scenario']");
		homeIcon = By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]");		
		myPeople = By.id("My People");
		byname = By.id("By Name");
		employeeAtGradeValidation = By.id("David Sergovic");
		okButton=By.id("OK");
		policyDevelopment=By.xpath("//XCUIElementTypeStaticText[@name= 'Policy development']//following-sibling::XCUIElementTypeOther//XCUIElementTypeSlider");
		
}

}
