package com.haygroup.ios.page;

import org.openqa.selenium.By;

public class StylesClimatePageIOS {

	public static By stylesAndClimateTab;
	public static By clickToScanQRCode;
	public static By scanCodeOKButton;
	public static By yesButton;
	public static By uniqueCodeTextBox;
	public static By enterQrcodeContinueBtn;
	public static By designMyPlanBtn;
	public static By designMyOwnPlanBtn;
	public static By enterManualFeedback;
	public static By continueBtn;
	public static By vStyle1;
	public static By vStyle2;
	public static By createAPlanForMeBtn;
	public static By doneBtn;
	public static By clickHomeBtn;
	public static By oneOnOneText;
	public static By teamText;
	public static By oneTimeText;	
	public static By vStyle3;
	public static By vStyle4;
	public static By vStyle5;
	public static By selectTip2;
	public static By vsubStyle3;
	public static By OKButton;
	public static By week1Text;
	public static By visionaryStyle;
	public static By affiliateStyle;
	public static By pacesettingStyle;
	public static By participativeStyle;	
	public static By editbtn;
	public static By datedrpdwn;
	public static By selectCheckbox;
	public static By selectLink1;
	public static By selectLink2;
	public static By entertext;
	public static By ratebtn;
	public static By congratsTipPopup;
	public static By calenderDone;
	public static By savebtn;
	public static By namedrpdwn;
	public static By selectnamefrmdrpdwn;
	public static By selectokbtn;
	public static By navigatebackbtn;

	/* Page Objects Of Styles and Climate */
	static {
		
		stylesAndClimateTab      = By.id("Styles & Climate");		
		clickToScanQRCode      = By.xpath("//XCUIElementTypeCell/XCUIElementTypeStaticText[contains(@name,'CREATE AN ACTION PLAN')]/following-sibling::XCUIElementTypeButton[3]");
		enterManualFeedback			= By.id("Enter Feedback Manually");
		scanCodeOKButton      = By.id("OK");
		yesButton      = By.id("Yes");
		uniqueCodeTextBox      = By.className("XCUIElementTypeTextField");
		enterQrcodeContinueBtn      = By.id("Continue");
		designMyPlanBtn      = By.id("Design My Plan");
		designMyOwnPlanBtn      = By.id("Design My Own Plan");
		continueBtn = By.id("Continue");
		vStyle1 = By.xpath("(//XCUIElementTypeCell)[1]/XCUIElementTypeButton[6]");
		vStyle2 = By.xpath("(//XCUIElementTypeCell)[2]/XCUIElementTypeButton[6]");
		vsubStyle3 = By.xpath("(//XCUIElementTypeCell)[3]/XCUIElementTypeButton[6]");
		createAPlanForMeBtn = By.id("Create A Plan For Me");
		doneBtn = By.id("Done");
		clickHomeBtn = By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]");
		oneOnOneText = By.xpath("(//XCUIElementTypeStaticText[@name='One-on-One (5/5)'])[2]");
		teamText = By.xpath("(//XCUIElementTypeStaticText[@name='Team (4/4)'])[2]");
		oneTimeText = By.xpath("(//XCUIElementTypeStaticText[@name='One-Time (0)'])[2]");
		selectTip2 = By.xpath("(//XCUIElementTypeCell)[2]/XCUIElementTypeButton[6]/parent::XCUIElementTypeCell/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeButton");
		vStyle3 = By.xpath("//XCUIElementTypeStaticText[@name='Using the Affiliative Style']/parent::XCUIElementTypeOther/following-sibling::XCUIElementTypeCell[1]/XCUIElementTypeButton[6]");
		vStyle4 = By.xpath("//XCUIElementTypeStaticText[@name='Using the Affiliative Style']/parent::XCUIElementTypeOther/following-sibling::XCUIElementTypeCell[2]/XCUIElementTypeButton[6]");
		vStyle5 = By.xpath("//XCUIElementTypeStaticText[@name='Using the Affiliative Style']/parent::XCUIElementTypeOther/following-sibling::XCUIElementTypeCell[3]/XCUIElementTypeButton[6]");
		OKButton      = By.id("OK");
		week1Text = By.id("WEEK 1");
		visionaryStyle = By.xpath("//XCUIElementTypeStaticText[@name='Most Impactful']/../following-sibling::XCUIElementTypeOther[1]");
		affiliateStyle=By.xpath("//XCUIElementTypeStaticText[@name='Most Impactful']/../following-sibling::XCUIElementTypeOther[2]");
		pacesettingStyle = By.xpath("//XCUIElementTypeStaticText[@name='Most Impactful']/../following-sibling::XCUIElementTypeOther[7]");
		participativeStyle= By.xpath("//XCUIElementTypeStaticText[@name='Most Impactful']/../following-sibling::XCUIElementTypeOther[3]");
		editbtn=By.id("Edit");
		datedrpdwn=By.xpath("//XCUIElementTypeCell/XCUIElementTypeButton[3]");
		selectCheckbox=By.xpath("//XCUIElementTypeStaticText[@name='WEEK 1']/parent::XCUIElementTypeCell/following-sibling::XCUIElementTypeCell[2]/XCUIElementTypeButton");
		selectLink1=By.xpath("(//XCUIElementTypeCell//XCUIElementTypeStaticText)[6]");
		selectLink2=By.xpath("(//XCUIElementTypeCell//XCUIElementTypeStaticText)[9]");
		entertext=By.className("XCUIElementTypeTextView");
		ratebtn=By.id("Rate");
		congratsTipPopup=By.id("btn close selected");
		calenderDone=By.id("Done");
		savebtn=By.id("Save");
		namedrpdwn=By.xpath("(//XCUIElementTypeCell/XCUIElementTypeButton)[2]");
		selectnamefrmdrpdwn=By.xpath("(//XCUIElementTypeCell/XCUIElementTypeButton)[2]");
		selectokbtn=By.id("OK");
		navigatebackbtn=By.id("DETAIL");
	
	}
}
