package com.haygroup.ios.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.EcoSystemIOSLib;
import com.haygroup.ios.page.EcoSystemPageIOS;

public class TC_IOS_EcoSystem_Eco_003 extends EcoSystemIOSLib{
	
	@DataProvider
	public Object[][] getTestDataFor_EcoSystemspage() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_EcoSystemspage")
	public void EcoSystems003IOSTest(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
			this.reporter.initTestCaseDescription("TC.Contact Administrator");	
			/* Login to the application */
			loginToIOSApplication(data.get("UserNameIOS"), data.get("PasswordMobile"));
			mobileSettingsPageButtonIOS();
			clickContactAdministrator();
			enterMessage(data.get("message"));
			clickDoneBtn();		
			
		}} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		logOutMobileApplication();
	}

}
