package com.haygroup.ios.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.StylesClimateIOSLib;


public class TC_IOS_StylesClimate_003 extends StylesClimateIOSLib{
	
	@DataProvider
	public Object[][] getTestDataFor_StylesAndClimate() {
		return TestUtil.getData("Create_Styles_And_Climate", TestData, "StylesAndClimate");
	}

	@Test(dataProvider = "getTestDataFor_StylesAndClimate")
	public void stylesClimate003(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Complete and edit tips");	
					
				loginToIOSApplication(data.get("UserNameIOS"), data.get("PasswordMobile"));	
				//Click on Styles&Climate Tab
				openStylesAndClimate();
				Waittime();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			    Waittime();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);											//Click Scan QR Code
				clickScanQRCode();			
				clickEnterFeedBackManuallyButton();
			    clickYesButton();
				enterUinqueCode(data.get("UniqueCode"));
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);		
				clickContinueButton();				
				clickDesignMyPlanButton();
				clickDesignMyOwnPlanButton();
				selectFiveTips();				
				clickContinueButton();
				selectFourTips();
				clickContinueButton();			
				clickDoneBtn();
				selectTip();
				selectMyPlanLink1();
				selectMyPlanLink2();							
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@AfterMethod
	public void logoutFromApplication() throws Throwable {
		//Logout from the application 
				clickHomeButton();
				logOutMobileApplication();
	}

}
