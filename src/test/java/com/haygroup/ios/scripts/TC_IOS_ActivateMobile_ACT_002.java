package com.haygroup.ios.scripts;

import java.util.Hashtable;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.EcoSystemIOSLib;
import com.haygroup.ios.page.EcoSystemPageIOS;

public class TC_IOS_ActivateMobile_ACT_002 extends EcoSystemIOSLib {

	@DataProvider
	public Object[][] getTestDataFor_ActivateMobile() {
		return TestUtil.getData("ActivateMobile", TestData, "ActivateMobile");
	}

	@Test(dataProvider = "getTestDataFor_ActivateMobile")
	public void ActivateMobile002Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.activatemobile");	
				optimizeforChina(data.get("UserNameIOS"), data.get("PasswordMobile"));
				mobileSettingsPageButtonIOS();
				assertTextMatching(EcoSystemPageIOS.getDynamicLocatorByID(data.get("UserNameIOS")), data.get("UserNameIOS"), "UserName Verification");
				clickDoneButton();
			}									
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		logOutMobileApplication();

	}
}


