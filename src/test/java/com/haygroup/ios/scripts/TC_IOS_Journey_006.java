package com.haygroup.ios.scripts;

import java.util.Hashtable;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.page.JourneyPageAndroid;
import com.haygroup.ios.libs.JourneyIOSLib;
import com.haygroup.ios.page.JourneyPageIOS;


public class TC_IOS_Journey_006 extends JourneyIOSLib {

	@DataProvider
	public Object[][] getTestDataFor_journeyFlyontheWall() {
		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}

	@Test(dataProvider = "getTestDataFor_journeyFlyontheWall")
	public void JourneyFlyontheWall006Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.journeyFlyontheWall");	
				loginToJourneyIOSApplication(data.get("UserNameIOS").trim(), data.get("Password").trim());
				clickSettings();			
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				launchFlyontheWall();
				clickFlyontheWallPlayButton();
				appiumDriver.swipe(5, 300, 5, 100, 300);
				clickNext();
				tapRatingFlyOnTheWall();
				clickNext();
				clickConfirm();
				enterEmailAddressInFlyOnTheWall(data.get("Email_id_1"),data.get("Email_id_2"),data.get("Email_id_3"));
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				clickDone();
				clickSettings();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);										
			}

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
	journeyLogOutMobileApplication();
	clickYesButton();
	}
	
}


