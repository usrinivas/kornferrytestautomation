package com.haygroup.ios.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.JourneyAndroidLib;
import com.haygroup.ios.libs.JourneyIOSLib;

public class TC_IOS_Journey_004 extends JourneyIOSLib {

	@DataProvider
	public Object[][] getTestDataFor_ElevatorPitch() {
		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}

	@Test(dataProvider = "getTestDataFor_ElevatorPitch")
	public void JourneyElevatorPitch004Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.ElevatorPitch");	
				loginToJourneyIOSApplication(data.get("UserNameIOS").trim(), data.get("Password").trim());
				clickSettings();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				launchElevatorPitch();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				clickRecordButton();
		        PlayerWaitTime();
				clickPlayButton();
				PlayerWaitTime();
				clickNext(); 
				clickDone();
				clickCongratulations();
				clickSettings();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				
			}
		}					
			catch (Exception e) {
					e.printStackTrace();
					//throw new RuntimeException(e);
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@AfterMethod
			public void logoutApplication() throws Throwable {
				journeyLogOutMobileApplication();
				clickYesButton();
			}
		}
