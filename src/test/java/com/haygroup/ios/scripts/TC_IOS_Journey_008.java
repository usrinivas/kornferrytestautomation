package com.haygroup.ios.scripts;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.EcoSystemIOSLib;
import com.haygroup.ios.libs.JourneyIOSLib;
import com.haygroup.ios.page.EcoSystemPageIOS;
import com.haygroup.ios.page.JourneyPageIOS;
import com.haygroup.web.page.EcoSystemPage;
import io.appium.java_client.SwipeElementDirection;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSElement;



public class TC_IOS_Journey_008 extends JourneyIOSLib {

	@DataProvider
	public Object[][] getTestDataFor_chinaServer() {
		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}

	@Test(dataProvider = "getTestDataFor_chinaServer")
	public void JourneychinaServer008Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.chinaServer");	
				journey_optimizeforChina(data.get("UserNameIOS").trim(), data.get("Password").trim());
				clickSettings();
				assertTextMatching(JourneyPageIOS.getDynamicLocatorByID(data.get("UserNameIOS").trim()), data.get("UserNameIOS").trim(), "UserName Verification");
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		journeyLogOutMobileApplication();
		clickYesButton();
	}
}


