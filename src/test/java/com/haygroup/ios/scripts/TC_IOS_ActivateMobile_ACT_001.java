package com.haygroup.ios.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.StylesClimateAndroidLib;
import com.haygroup.android.page.StylesClimatePageAndroid;
import com.haygroup.ios.libs.HayGroupIOSCommonLib;
import com.haygroup.ios.libs.StylesClimateIOSLib;
import com.haygroup.ios.page.StylesClimatePageIOS;
import com.haygroup.web.page.EcoSystemPage;

public class TC_IOS_ActivateMobile_ACT_001 extends StylesClimateIOSLib{

	@DataProvider
	public Object[][] getTestDataFor_ActivateMobile() {
		return TestUtil.getData("Create_Styles_And_Climate", TestData, "StylesAndClimate");
	}

	@Test(dataProvider = "getTestDataFor_ActivateMobile")
	public void ActivateMobile001Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.ScanQRcode");	
	
				//Login to the application
			    loginToIOSApplication(data.get("UserNameIOS"), data.get("PasswordMobile"));	
				openStylesAndClimate();		
				Waittime();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			    Waittime();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);				
				clickScanQRCode();	
				openQRcode();
				Thread.sleep(5000);
				quitQRcode();	
				Waittime();
				Waittime();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);		
				clickContinueButton();					
				clickDesignMyPlanButton();
				clickCreateAPlanForMeButton();
				clickContinueButton();
				clickContinueButton();
				Waittime();
				clickDoneButtonInOneTimeText();
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutFromApplication() throws Throwable {
		//Logout from the application 
		clickHomeButton();
		logOutMobileApplication();
	}
}
