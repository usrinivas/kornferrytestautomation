package com.haygroup.ios.scripts;

import java.util.Hashtable;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.JobPricingIOSLib;


/*This Test is To create a failure Job description*/
public class TC_IOS_JobPricing_001 extends JobPricingIOSLib {
	@DataProvider
	public Object[][] getTestDataFor_JobPricing() {
		return TestUtil.getData("Create_JobPricing", TestData, "JobPricing");
	}

	@Test(dataProvider = "getTestDataFor_JobPricing")
	public void JobPricing001(Hashtable<String, String> data) {
		//public void mobileSettingsPage002Test() {
		try {
			if (data.get("RunMode").equals("Y")) {
			this.reporter.initTestCaseDescription("TC.JobPricing");	
			/* Login to the application */
			loginToIOSApplication(data.get("UserName").trim(),data.get("Password").trim());
			selectJobpricing(data.get("Jobtitle").trim());
			clickYesButton();
			selectjobfamily();
			appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			addLocation(data.get("Location").trim());
		    removeddLocation(data.get("Location").trim());
			String salaryIdentifier = selectEmployeeandSaveWithSalaryIdentifier(data.get("ScenarioName").trim());
			navigateToSalarySimulator();			
			fillSalarySimulator(data.get("Fixed").trim(),data.get("Base").trim(),data.get("Final").trim());
			@SuppressWarnings("unused")
			String salarysimulate=selectSimulateEmployeesave(data.get("ScenarioName1"));		
			validateSalaryInfo(salaryIdentifier);		
			}
			} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		logOutMobileApplication();		
	}
}
