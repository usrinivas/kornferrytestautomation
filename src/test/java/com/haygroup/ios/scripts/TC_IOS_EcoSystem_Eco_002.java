package com.haygroup.ios.scripts;

import java.util.Hashtable;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.EcoSystemIOSLib;

public class TC_IOS_EcoSystem_Eco_002 extends EcoSystemIOSLib{

	@DataProvider
	public Object[][] getTestDataFor_EcoSystemspage() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_EcoSystemspage")
	public void EcoSystems002IOSTest(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.settingspage");	
				/* Login to the application */
				loginToIOSApplication(data.get("UserNameMobile"), data.get("PasswordMobile"));
				Waittime();
				mobileSettingsPageButtonIOS();			
				enterLocation(data.get("Location"));
				Waittime();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				preferedCurrency();
				userpreferredTimezone();
				notifications();		
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				MyPlanNotifications();
	     		clickDoneBtn();
	

			} }catch (Exception e) {
				e.printStackTrace();
				
				//throw new RuntimeException(e);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		logOutMobileApplication();

		//captureBrowserConsoleLogs(Driver);
	}

}
