package com.haygroup.ios.scripts;

import java.util.Hashtable;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.JourneyIOSLib;
import com.haygroup.ios.page.JourneyPageIOS;

public class TC_IOS_Journey_007 extends JourneyIOSLib {

	@DataProvider
	public Object[][] getTestDataFor_journeypreferences() {
		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}

	@Test(dataProvider = "getTestDataFor_journeypreferences")
	public void JourneyPreferences007Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.JourneyPreferences");	
				loginToJourneyIOSApplication(data.get("UserNameIOS").trim(), data.get("Password").trim());
				clickSettings();
				selectMyTitle(data.get("MyTitle").trim());		
				assertTextMatching(JourneyPageIOS.currentTitle,data.get("MyTitle").trim(), "title");   
				languagechange_mobile(data.get("LanguageMobile").trim(),data.get("DefaultLanguage").trim());
				loginToJourneyIOSApplication(data.get("UserNameIOS").trim(), data.get("Password").trim());			
				clickSettings();     			
			    //assertTextMatching(JourneyPageIOS.preferencesText,data.get("PreferencesText").trim(), "Language Verification");
				languagechangeforChina_mobile(data.get("LanguageMobile").trim(),data.get("DefaultLanguage").trim());
//PreferencesText
//设置
			}

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
	}
}


