package com.haygroup.ios.scripts;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.JourneyIOSLib;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSElement;


public class TC_IOS_Journey_003 extends JourneyIOSLib {

	@DataProvider
	public Object[][] getTestDataFor_LaunchStart() {
		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}

	@Test(dataProvider = "getTestDataFor_LaunchStart")
	public void JourneyLauncStart003Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.LaunchStart");	
				loginToJourneyIOSApplication(data.get("UserNameIOS").trim(), data.get("Password").trim());
				clickSettings();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				Launchstart();
				clickstart();
				clickNext();
				clickConfirm();
				Waittime();
				clickstart();
				clickDone();
				clickCongratulations();
				clickSettings();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);				
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@AfterMethod
	public void logoutApplication() throws Throwable {
		journeyLogOutMobileApplication();
		clickYesButton();
	}
}


