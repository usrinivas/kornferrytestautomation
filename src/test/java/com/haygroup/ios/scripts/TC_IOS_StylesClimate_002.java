package com.haygroup.ios.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.StylesClimateAndroidLib;
import com.haygroup.android.page.StylesClimatePageAndroid;
import com.haygroup.ios.libs.HayGroupIOSCommonLib;
import com.haygroup.ios.libs.StylesClimateIOSLib;
import com.haygroup.ios.page.StylesClimatePageIOS;
import com.haygroup.web.page.EcoSystemPage;

public class TC_IOS_StylesClimate_002 extends StylesClimateIOSLib{

	@DataProvider
	public Object[][] getTestDataFor_StylesAndClimate() {
		return TestUtil.getData("Create_Styles_And_Climate", TestData, "StylesAndClimate");
	}

	@Test(dataProvider = "getTestDataFor_StylesAndClimate")
	public void stylesClimate002(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Design My Own Plan - Manually enter percentiles");	
	
				//Login to the application
				loginToIOSApplication(data.get("UserNameIOS"), data.get("PasswordMobile"));		
				//Click on Styles&Climate Tab
				openStylesAndClimate();
				Waittime();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
			    Waittime();
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);											
				clickScanQRCode();			
				clickEnterFeedBackManuallyButton();
			    clickYesButton();
				enterUinqueCode(data.get("UniqueCode"));			
				appiumDriver.swipe(5, appiumDriver.manage().window().getSize().getHeight()-100, 5, 100, 300);
				clickContinueButton();				
				clickDesignMyPlanButton();
				clickDesignMyOwnPlanButton();
				selectFiveTips();			
				clickContinueButton();
				selectFourTips();
				clickContinueButton();				
			    selectOneTip();			
			    clickOKButton();			
			    clickDoneBtn();			
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutFromApplication() throws Throwable {
		//Logout from the application 
		clickHomeButton();
		logOutMobileApplication();
	}
}
