package com.haygroup.ios.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.JobGradingIOSLib;

public class TC_IOS_JobGrading_001 extends JobGradingIOSLib{

	@DataProvider
	public Object[][] getTestDataFor_JobGrading() {
		return TestUtil.getData("createJobGrading",TestData,"JobGrading");
	}

	@Test(dataProvider ="getTestDataFor_JobGrading")
	public void JobGrading001(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
			this.reporter.initTestCaseDescription("TC.JobGrading");	
			/* Login to the application */
			loginToIOSApplication(data.get("UserName").trim(),data.get("Password").trim());		
			selectJobGrading(data.get("selectNameTitle").trim());		
			selectjobfamily();
			clickApplyGradeBtn();
			selectEmployeeApplyGrade();
			saveThisGradescenarioDetails(data.get("ScenarioName"));
			validateEmployeeGrade();
			}
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		logOutMobileApplication();

	}
}




