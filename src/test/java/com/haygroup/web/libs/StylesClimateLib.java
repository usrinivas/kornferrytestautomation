package com.haygroup.web.libs;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;

import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.Xls_Reader;
import com.google.common.base.Function;
import com.haygroup.web.page.StylesClimatePage;

public class StylesClimateLib extends HayGroupCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	/**
	 * @author: cigniti
	 * @description This function performs click operation on EnterFeedBackManuallyButton
	 * 
	 */
	public void clickEnterFeedBackManuallyButton()  {

		try {

			Waittime();
			scrollToView(StylesClimatePage.enterFeedbackmanually);
			waitForVisibilityOfElement(StylesClimatePage.enterFeedbackmanually, "", 60);
			click(StylesClimatePage.enterFeedbackmanually, "Enter Feedback Manually button");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on overwrite pop up
	 * 
	 */
	public void clickOverWritePopup() {
		try {
			waitForVisibilityOfElement(StylesClimatePage.okButton, "", 60);
			click(StylesClimatePage.okButton, "Confirmation for Over writing the Previous road Map");
			isApplicationReady();		
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function enters the unique code
	 * 
	 */
	public void enterUniqueCode(String uniqueCode) {
		try {
			Waittime();
			type(StylesClimatePage.uniqueCodeField, uniqueCode, "Entering Unique Code");
			Waittime();
			click(StylesClimatePage.continueButton, "Continue Button to Enter Feedback Manually");
			isApplicationReady();
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	/**
	 * @author: cigniti
	 * @description This function performs click operation on continue Activate RoadMap button
	 * 
	 */

	public void continueActivateRoadMap()  {
		try {

			scrollToView(StylesClimatePage.continueButton);
			Waittime();
			JSClick(StylesClimatePage.continueButton, "Continue Button to view activate Road Map");
			Waittime();
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on design my plan
	 * 
	 */

	public void designMyPlan()  {
		try {
			isApplicationReady();
			waitTillElementToBeClickble(StylesClimatePage.designMyPlan, "wait for design my plan button");
			click(StylesClimatePage.designMyPlan, "Design My Plan button");
			Waittime();
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on designMyOwnPlan button
	 * 
	 */
	public void designMyOwnPlan()  {
		try {
			isApplicationReady();
			waitForVisibilityOfElement(StylesClimatePage.designMyOwnPlan, "", 60);
			click(StylesClimatePage.designMyOwnPlan, "Design MyOwn Plan button");
			isApplicationReady();
			Waittime();
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on createPlanForMe button
	 * 
	 */
	public void createPlanForMe()  {
		try {
			isApplicationReady();
			waitForVisibilityOfElement(StylesClimatePage.createPlanForMe, "", 60);
			click(StylesClimatePage.createPlanForMe, "Create A Plan button");
			isApplicationReady();
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	/**
	 * @author: cigniti
	 * @description This function selects the style
	 * 
	 */

	public void selectStyle(String styleName)  {
		try {
			selectStyleByText(styleName);
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects the tips
	 * 
	 */
	public void selectTips(int tipsCount)  {
		try {
			selectTipsByIndex(tipsCount);
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function clicks continue button
	 * 
	 */
	public void clickContinueButton()  {
		try {

			Waittime();
			waitForVisibilityOfElement(StylesClimatePage.continueOneOnOne, "", 30);
			JSClick(StylesClimatePage.continueOneOnOne, "Continue Button");
			isApplicationReady();
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function reviews and complete the tasks
	 * 
	 */
	public void reviewAndCompleteTasks() {
		try {
			waitForVisibilityOfElement(StylesClimatePage.checkBoxList, "Checkbox list", IFrameworkConstant.WAIT_TIME_MEDIUM);
			boolean value = false;
			while (!(value)) {
				value = selectcheckbox();
				if (!(value)) {
					selectNextWeek();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects the style By text
	 * 
	 */
	public void selectStyleByText(String value) {
		try {          
			String styleXPath = "//div[starts-with(text(),'" + value + "')]/ancestor::div[@class='tile-item-wrapper']/a/div[2]/div[2]";
			JSMouseHoverImitate(By.xpath(styleXPath),"Hover on"+value);
			WebDriverWait wait = new WebDriverWait(Driver, 90);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[starts-with(text(),'" + value + "')]/ancestor::div[@class='tile-item-wrapper']//div//span[@class='add-to-plan-text']")));
			JSClick(By.xpath("//div[starts-with(text(),'" + value + "')]/ancestor::div[@class='tile-item-wrapper']//div//span[@class='add-to-plan-text']"), "Add to plan");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function selects the tips by index
	 * 
	 */
	public void selectTipsByIndex(int tipsCount) {
		try {          
			for(int i=2;i<=tipsCount;i++){
				JSMouseHoverImitate(By.xpath("//*[@id='tip-style-group-container']/li["+i+"]/div/a/div[2]/div[2]"),"Hover on Plan "+tipsCount);
				WebDriverWait wait = new WebDriverWait(Driver, 90);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//ul[@id='tip-style-group-container'])[1]/li["+i+"]/div/a/div[2]/div[2]/span[@class='add-to-plan-text']")));
				JSClick(By.xpath("(//ul[@id='tip-style-group-container'])[1]/li["+i+"]/div/a/div[2]/div[2]/span[@class='add-to-plan-text']"), "Add to plan");
				/* Verify Tips Count*/
				Driver.findElement(By.id("numerator")).getText().equals(i-1);
			}
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * @author: cigniti
	 * @description This function verifies Number Of Tips
	 * 
	 */

	public void verifyNumberOfTips(int tipsCount){
		/* Verify Tips Count*/
		Driver.findElement(By.id("numerator")).getText().equals(tipsCount);

	}

	/**
	 * @author: cigniti
	 * @description This function selects Next Week
	 * 
	 */
	public void selectNextWeek()  {
		try {
			click(StylesClimatePage.nextWeekBtn, "Next week");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function reviews the pop up
	 * 
	 */
	public boolean reviewPopup() throws Throwable {
		boolean progressStatus = false;
		int ratingRandomNumber = (int)(Math.random() * 5) + 1;
		waitForVisibilityOfElement(By.id("app-modal"), "Modal Popup", IFrameworkConstant.WAIT_TIME_MEDIUM);		
		isApplicationReady();
		Driver.findElement(By.cssSelector("input[type='radio'][value='"+ratingRandomNumber+"']")).click();
		type(StylesClimatePage.editReview, "Reviewed and rated "+ratingRandomNumber + " star", "Edit Review");
		JSClick(StylesClimatePage.markCompletebBtn, "Continue to move to One Time");
		isApplicationReady();
		Thread.sleep(3000);
		String percentage = Driver.findElement(By.className("color-quaternarytext")).getAttribute("innerText")
				.split(" ")[0];
		if (percentage.indexOf("100%") > -1) {
			Waittime();
			progressStatus = true;			
		}
		waitForVisibilityOfElement(StylesClimatePage.continueOneOnOne, "Wait for Continue Button", IFrameworkConstant.WAIT_TIME_MEDIUM);
		click(StylesClimatePage.continueOneOnOne, "Continue to move to One Time");
		return progressStatus;
	}

	/**
	 * @author: cigniti
	 * @description This function selects checkbox
	 * 
	 */
	public boolean selectcheckbox() throws Throwable {
		boolean checkedStatus = false;
		List<WebElement> CheckBox = Driver.findElements(StylesClimatePage.checkBoxListInCalander);
		for (int i = 1; i <= CheckBox.size(); i++) {
			if (checkedStatus != true)
			{
				JSClick(StylesClimatePage.clickCheckBox, "Check Box");
				checkedStatus = reviewPopup();
			}
		}
		return checkedStatus;
	}

	/**
	 * @author: cigniti
	 * @description This function enters the percentiles for Style Feedback
	 * 
	 */
	public void enterStyleFeedback(){
		List<WebElement> percentileTextBox = Driver.findElements(StylesClimatePage.enterStyleFeedback);
		for (int i = 1; i <= percentileTextBox.size(); i++) {
			Driver.findElement(By.xpath("//*[@id='stylesTable']/table/tbody/tr["+i+"]/td[3]/input")).clear();
			Driver.findElement(By.xpath("//*[@id='stylesTable']/table/tbody/tr["+i+"]/td[3]/input")).sendKeys("20");
		}
	}

	/**
	 * @author: cigniti
	 * @description This function enters the percentiles for Climate Rater Feedback
	 * 
	 */
	public void enterClimateRaterFeedback(){
		List<WebElement> percentileTextBox = Driver.findElements(StylesClimatePage.enterClimateRaterFeedback);
		for (int i = 1; i <= percentileTextBox.size(); i++) {
			Driver.findElement(By.xpath("//*[@id='climateTable']/table/tbody/tr["+i+"]/td[4]/input")).clear();
			Driver.findElement(By.xpath("//*[@id='climateTable']/table/tbody/tr["+i+"]/td[4]/input")).sendKeys("20");
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs click operation on done button
	 * 
	 */
	public void clickDoneButton()  {
		try {
			waitForVisibilityOfElement(By.xpath("//*[text() = 'Done']"), "Done Button", IFrameworkConstant.WAIT_TIME_MEDIUM);
			JSClick(StylesClimatePage.doneButton, "Done Button");
			isApplicationReady();
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function verifies the congratulation tip pop up
	 * 
	 */
	public void verifyTipPopup() {
		try {
			JSClick(StylesClimatePage.clickCheckBox, "Check Box");
			int ratingRandomNumber = (int)(Math.random() * 5) + 1;
			waitForVisibilityOfElement(By.id("app-modal"), "Modal Popup", IFrameworkConstant.WAIT_TIME_MEDIUM);		
			isApplicationReady();

			/*Assert ' How Useful Was This Item' pop-ups of a tip.*/
			assertTextMatching(StylesClimatePage.tipPopupHeaderText, "How Useful Was This Item?", "Verifying Pop Up Header Text");
			Driver.findElement(By.cssSelector("input[type='radio'][value='"+ratingRandomNumber+"']")).click();
			type(StylesClimatePage.editReview, "Reviewed and rated "+ratingRandomNumber + " star", "Edit Review");
			JSClick(StylesClimatePage.markCompletebBtn, "Continue to move to One Time");

			/*Assert 'CONGRATULATIONS!' pop-ups of a tip.*/
			waitForVisibilityOfElement(StylesClimatePage.continueOneOnOne, "Wait for Continue Button", IFrameworkConstant.WAIT_TIME_MEDIUM);
			assertTextMatching(StylesClimatePage.congratsPopUp, "CONGRATULATIONS!", "Verifying Pop Up Header Text");
			click(StylesClimatePage.continueOneOnOne, "Continue to move to One Time");

			/*Assert Check Box is Selected*/

			waitForInVisibilityOfElement(By.id("app-modal"), "Modal Popup");
			assertTrue(Driver.findElement(By.xpath("(//input[@type='checkbox'])[1]")).isSelected());
			Driver.findElement(By.xpath("(//input[@type='checkbox'])[1]")).click();
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function edits the Name On Details Tip Popup
	 * 
	 */
	public void editNameOnDetailsTipPopup() {
		try {
			/*Get the author name of any/first tip*/
			WebElement tipAuthorName = Driver.findElement(By.xpath("(//ul[contains(@class,'day-grid')]/li/div/ul/li/a/footer/strong)[1]"));
			String tipOldAuthorName = tipAuthorName.getText();

			//Open Details popup by clicking on any/first tip
			String tipText = Driver.findElement(By.xpath("(//ul[contains(@class,'day-grid')]/li/div/ul/li/a/h4)[1]")).getText();
			String tipCurrentDay = Driver.findElement(By.xpath("(//ul[contains(@class,'day-grid')]//h4)[1]/ancestor::li[contains(@id,'day-')]")).getAttribute("id");

			//Increment day count.
			String[] textOne = tipCurrentDay.split("-");
			String textTwo=textOne[1];
			int oldDayCount = Integer.parseInt(textTwo);
			int newDayCount = oldDayCount+1;
			Driver.findElement(By.xpath("(//ul[contains(@class,'day-grid')]/li/div/ul/li/a)[1]")).click();

			//Assert 'DETAILS' pop-up of a tip.

			waitForVisibilityOfElement(StylesClimatePage.detailsPopUp, "Wait for Details PopUp", IFrameworkConstant.WAIT_TIME_MEDIUM);
			assertTextMatching(StylesClimatePage.detailsPopUpText, "DETAIL", "Verifying Pop Up Header Text");
			click(StylesClimatePage.detailPopUpEditLink, "Click Edit to update Name");

			//Select different Name from The Appointment drop down
			waitForVisibilityOfElement(StylesClimatePage.appointmentDropDown, "Wait for Appointment Dropdown", IFrameworkConstant.WAIT_TIME_MEDIUM);
			click(StylesClimatePage.appointmentDropDown, "Open Appointment Dropdown");
			List<WebElement> listOptions = Driver.findElements(StylesClimatePage.appointmentDropDownOptions);
			String tipNewAuthorName = "";
			for(int i=0; i<=listOptions.size(); i++){
				if(!(listOptions.get(i).getText()).equals(tipOldAuthorName)){
					tipNewAuthorName = listOptions.get(i).getText();
					System.out.println("NEW AUTHER NAME IS========"+tipNewAuthorName);
					int j=i+1;
					Driver.findElement(By.xpath("//*[@id='select2-drop']/ul/li["+j+"]")).click();
					break;
				}
			}

			//Select different Date from Date field
			waitForVisibilityOfElement(StylesClimatePage.dateField, "Wait for Date Field", IFrameworkConstant.WAIT_TIME_MEDIUM);
			click(StylesClimatePage.dateField, "Open Date Field");
			String nextDateValue = Driver.findElement(By.xpath("//td[@class='active day']/following-sibling::td[1]")).getText();
			click(StylesClimatePage.selectNextDate, "Select Next Day from Calender");
			click(StylesClimatePage.detailPopUpDoneLink, "Click on Done to close pop up");
			isApplicationReady();
			Waittime();

			//Assert tip has been moved to given date or not
			assertTrue(Driver.findElement(By.xpath("(//li[@id='day-"+newDayCount+"']//ul//h4[contains(text(),'"+tipText+"')])[1]")).isDisplayed());

			//Assert the updated Name on ToDo page
			String tipUpdatedAuthorName = Driver.findElement(By.xpath("(//li[@id='day-"+newDayCount+"']//ul//h4[contains(text(),'"+tipText+"')]/following-sibling::footer/strong)[1]")).getText();
			assertTrue(tipUpdatedAuthorName.equalsIgnoreCase(tipNewAuthorName));
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	/**
	 * @author: cigniti
	 * @description This function removes the tip from the plan
	 * 
	 */

	public void removeTipFromPlan() {
		try {
			Waittime();
			String tipText = Driver.findElement(By.xpath("(//ul[contains(@class,'day-grid')]/li/div/ul/li/a/h4)[1]")).getText();
			Driver.findElement(By.xpath("(//ul[contains(@class,'day-grid')]/li/div/ul/li/a)[1]")).click();
			waitForVisibilityOfElement(StylesClimatePage.detailPopUpEditLink, "Wait for Details PopUp", IFrameworkConstant.WAIT_TIME_MEDIUM);
			click(StylesClimatePage.removeTip, "Click on Remove From Plan");
			waitForVisibilityOfElement(StylesClimatePage.justThisButton, "Wait for Just This button", IFrameworkConstant.WAIT_TIME_MEDIUM);
			click(StylesClimatePage.justThisButton, "Click on Just This button");
			isApplicationReady();

			String AllTipsText = Driver.findElement(By.id("plan-container")).getText();
			assertFalse(AllTipsText.contains(tipText));
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function verifies LevelUp Page
	 * 
	 */
	public void verifyLevelUpPage(){
		try {
			isApplicationReady();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String actURL = getCurrentURL();
		if(actURL.contains("roadmap"))
		{
			Driver.findElement(By.xpath("//button[@class='btn btn-icon btn-primary']")).click();;
			try {
				isApplicationReady();
				selectRegion("Styles & Climate");
				isApplicationReady();
				verifyLevelUpPage();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}