package com.haygroup.web.libs;

import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gallop.accelerators.ActionEngine;
import com.gallop.support.CustomException;
import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.EcoSystemPage;
import com.haygroup.web.page.HomePage;
import com.haygroup.web.page.LoginPage;

import freemarker.core.Environment;


public class HayGroupCommonLib extends ActionEngine{
	//public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");
	
	public String Environment=null;
	
	public String launchURL(Hashtable<String, String> data){
		String Env=null;
		Environment = System.getenv("Environment");
		System.out.println("the environmental variable from jernkins is "+Environment);
		
		if(Environment.equalsIgnoreCase("Dev"))
		{
			Env=data.get("DevURL").trim();
		}
		else if(Environment.equalsIgnoreCase("Staging"))
		{
			Env=data.get("StagingURL").trim();
		}
		else if(Environment.equalsIgnoreCase("Testing"))
		{
			Env=data.get("TestURL").trim();
		}
		return Env;
		}

	/**
	 * @author: cigniti
	 * @description This function performs launch of application
	 * 
	 */

	public void launchApplication(String SheetName, String AppURL){

		
		try {
			
			Driver.get(AppURL);
			isApplicationReady();
			Driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs login to application
	 * 
	 */
	public void loginToApplication(String UserName, String Password) throws Throwable {	

		try {

			isApplicationReady();
			checkPageIsReady();
			try {
				waitTillElementToBeClickble(LoginPage.userName, "entering login username");
				type(LoginPage.userName, UserName, "entering login username");
				type(LoginPage.password, Password, "entering login password");
				JSClick(LoginPage.loginBtn, "Login button");
			} catch (Exception e) {
				// TODO: handle exception
				throw new CustomException("Error in finding the user name locator");
			}


		} catch (StaleElementReferenceException e) {
			// TODO: handle exception
		}

	}

	/**
	 * @author: cigniti
	 * @description This function performs login to application
	 * 
	 */

	public void loginToHaygroupApplication(String UserName, String Password)  { 

		try {

			String browserName = getBrowser();

			if(browserName.equalsIgnoreCase("chrome")){
				Waittime();
				Driver.manage().timeouts().pageLoadTimeout(150, TimeUnit.SECONDS);
				Waittime();
				waitForVisibilityOfElement(LoginPage.newusername, UserName, 180);
				type(LoginPage.newusername, UserName, "entering login username");
				type(LoginPage.newpassword, Password, "entering login password");
				click(LoginPage.newloginBtn, "Login button");
			}

			else if(browserName.equalsIgnoreCase("firefox")){
				Waittime();
				Driver.manage().timeouts().pageLoadTimeout(150, TimeUnit.SECONDS);
				Waittime();
				Thread.sleep(10000);
				Waittime();
				waitForVisibilityOfElement(LoginPage.newusername, UserName, 5000);
				type(LoginPage.newusername, UserName, "entering login username");
				type(LoginPage.newpassword, Password, "entering login password");
				click(LoginPage.newloginBtn, "Login button");
			}

			else if(browserName.equalsIgnoreCase("ie")){
				Waittime();
				Driver.manage().timeouts().pageLoadTimeout(150, TimeUnit.SECONDS);
				Waittime();
				waitForVisibilityOfElement(LoginPage.newusername,  "entering login username",150);
				type(LoginPage.newusername, UserName, "entering login username");
				type(LoginPage.newpassword, Password, "entering login password");
				click(LoginPage.newloginBtn, "Login button");
			}
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * @author: cigniti
	 * @description This function performs login to application
	 * 
	 */
	public void loginToJMApplication(String UserName, String Password)  {	
		try {
			waitForVisibilityOfElement(LoginPage.JM_username, "entering login username",60);
			type(LoginPage.JM_username, UserName, "entering login username");
			type(LoginPage.JM_password, Password, "entering login password");
			JSClick(LoginPage.JM_LoginBut, "Login button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs login to application
	 * 
	 */
	public void loginToMobileApplication(String UserName, String Password) {	
		try {
			waitTillElementToBeClickble(LoginPage.userName_mobile, "entering login username");
			type(LoginPage.userName_mobile, UserName, "entering login username");
			type(LoginPage.password_mobile, Password, "login password");
			click(LoginPage.loginBtn_mobile, "Login button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * @author: cigniti
	 * @description This function performs selecting the region
	 * 
	 */
	public void selectRegion(String region) {	
		try {
			JSClick(HomePage.selectregion(region), "Select region");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs logout application
	 * 
	 */
	public void logOutFromApplication() {
		try {
			JSClick(HomePage.logOutButton, "Log out");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function performs logout for mobile application
	 * 
	 */

	public void logOutMobileApplication() {
		try {
			waitTillElementToBeClickble(HomePage.logOut_mobile, "Log out Mobile");
			click(HomePage.logOut_mobile, "Log out Mobile");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
