package com.haygroup.web.libs;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.EcoSystemPage;
import com.haygroup.web.page.LoginPage;


public class EcoSystemLib extends HayGroupCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	/**
	 * @author: cigniti
	 * @description This function performs click operation on settings button
	 * 
	 */

	public void settingspagebutton() {

		try {

			Waittime();
			waitTillElementToBeClickble(EcoSystemPage.btnSettings, "click on settings");
			click(EcoSystemPage.btnSettings, "click on settings");
			isApplicationReady();

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author :cigniti
	 * @description This function performs language change operation
	 * 	 
	 * 
	 */

	public void verifylanguageChange(String changedLanguage){
		Select select = new  Select(Driver.findElement(EcoSystemPage.ddnLanguageChange));
		WebElement option = select.getFirstSelectedOption(); 
		System.out.println("option is "+option);
		System.out.println("option text  is "+option.getAttribute("value"));

		if (option.getAttribute("value").equalsIgnoreCase(changedLanguage)) {
			assertTrue(true, "Passed");
			try {
				this.reporter.SuccessReport("Verify language change", "language has successfully changed to "+changedLanguage);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
			try {
				this.reporter.failureReport("Verify language change", "language NOT changed  ",this.Driver);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/**
	 * @author :cigniti
	 * @description This function performs click operation on location dropdown
	 * 	 
	 * 
	 */

	public void locationchange(String location1) {

		try {

			click(EcoSystemPage.ddnLocation, "click on location dropdown");
			type(EcoSystemPage.ddnLocationText,location1,"enter text");
			click(EcoSystemPage.ddnLocationselect,"click on location");			


		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author :cigniti
	 * @description This function performs click operation on location dropdown
	 * 	 
	 * 
	 */

	public void locationchangeagain(String location2) {

		try {

			scrollToView(EcoSystemPage.txtMyprofile);
			Thread.sleep(5000);
			click(EcoSystemPage.ddnLocation, "click on location dropdown");
			type(EcoSystemPage.ddnLocationText,location2,"enter text");
			click(EcoSystemPage.ddnLocationselect2,"click on location");


		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	/**
	 * @author :cigniti
	 * @description This function performs click operation on currency dropdown
	 * 	 
	 * 
	 */

	public void currencychange(String currencyName) {
		try {

			selectByValue(EcoSystemPage.ddnCurrency, currencyName, "Currency");
			waitForVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed", 30);
			waitForInVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author :cigniti
	 * @description This function performs click operation on timezone dropdown
	 * 	 
	 * 
	 */

	public void timezonechange(String timezonename) {

		try {

			selectByVisibleText(EcoSystemPage.ddnTimezone, timezonename, "timezone");
			waitForVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed", 30);
			waitForInVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * @author :cigniti
	 * @description This function performs click operation on EMAIL NOTIFICATION PREFERENCES radio buttons
	 * 	 
	 * 
	 */

	public void emailnotification() {

		try {

			Thread.sleep(2000);
			scrollToView(EcoSystemPage.txtMailnotification);
			waitTillElementToBeClickble(EcoSystemPage.radDoNotSendEmailnotification, "click on do not send");
			click(EcoSystemPage.radDoNotSendEmailnotification, "click on do not send");
			waitTillElementToBeClickble(EcoSystemPage.radSendEmailnotification, "click on  send");
			click(EcoSystemPage.radSendEmailnotification, "click on send");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * @author :cigniti
	 * @description This function performs click operation on myplan radio buttons
	 * 	 
	 * 
	 */

	public void myplan() {

		try {


			waitTillElementToBeClickble(EcoSystemPage.radWeekbasisMyplannotification,"click on weekbasisnotification");
			click(EcoSystemPage.radWeekbasisMyplannotification, "click on weekbasisnotification");
			waitTillElementToBeClickble(EcoSystemPage.radDailybasisMyplannotification,"click on dailybasisnotification");
			click(EcoSystemPage.radDailybasisMyplannotification, "click on dailybasisnotification");

		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * @author :cigniti
	 * @description This function performs click operation on startday dropdown
	 * 	 
	 * 
	 */

	public void startdaychange(String weekstartingdaydropdown) {

		try {
			Driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			selectByVisibleText(EcoSystemPage.ddnWeekstartingday, weekstartingdaydropdown, "startday");
			Waittime();

		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * @author :cigniti
	 * @description This function performs click operation on weekend dropdown
	 * 	 
	 * 
	 */

	public void enddaychange(String weekenddaydropdown) throws Throwable {

		String browserName = getBrowser();

		if(browserName.equalsIgnoreCase("chrome")){

			WebDriverWait wait = new WebDriverWait(Driver,150);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".js-week-end-region>div>div>select")));
			Waittime();
			selectByValue(EcoSystemPage.ddnWeekendday, weekenddaydropdown, "endday");
			isApplicationReady();
		}

		else if(browserName.equalsIgnoreCase("firefox")){

			WebDriverWait wait = new WebDriverWait(Driver,150);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".js-week-end-region>div>div>select")));
			Waittime();
			selectByValue(EcoSystemPage.ddnWeekendday, weekenddaydropdown, "endday");
			isApplicationReady();
		}

		else if(browserName.equalsIgnoreCase("ie")){
			Waittime();
			WebDriverWait wait = new WebDriverWait(Driver,150);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".js-week-end-region>div>div>select")));
			Waittime();
			selectByValue(EcoSystemPage.ddnWeekendday, weekenddaydropdown, "endday");
			isApplicationReady();

		}
	}


	/**
	 * @author :cigniti
	 * @description This function performs click operation on teammeetingdaychange dropdown
	 * 	 
	 * 
	 */

	public void teammeetingdaychange(String teammeetingdaydropdown) {

		try {
			selectByVisibleText(EcoSystemPage.ddnTeammeetingday, teammeetingdaydropdown, "teammeetingday");
			isApplicationReady();

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}


	/**
	 * @author :cigniti
	 * @description This function performs click operation on teammeetingtimechange dropdown
	 * 	 
	 * 
	 */

	public void teammeetingtimechange(String teammeetingtimedropdown) {

		try {


			selectByValue(EcoSystemPage.ddnTeammeetingtime, teammeetingtimedropdown, "teammeetingtime");
			Waittime();

		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * @author :cigniti
	 * @description This function performs click operation on languagechange dropdown
	 * 	 
	 * 
	 */


	public void languagechange(String languagechange) {

		try {

			selectByValue(EcoSystemPage.ddnLanguageChange, languagechange, "languagechange");
			Thread.sleep(2000);
			checkPageIsReady();

		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author :cigniti
	 * @description This function checks whether the language has changed or not
	 * 	 
	 * 
	 */

	public void languagechangecheck(String verifytext) {


		try {

			isElementPresent(EcoSystemPage.verifylanguageChange, verifytext, true);
			Thread.sleep(2000);

		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}


	/**
	 * @author :cigniti
	 * @description This function performs click operation on contact button
	 * 	 
	 * 
	 */

	public void contact(String message) throws Throwable {

		try {

			click(EcoSystemPage.btnContact, "click on contact button");
			isApplicationReady();
			Waittime();
			waitForVisibilityOfElement(EcoSystemPage.chkUser, "verify check box", 5000);
			click(EcoSystemPage.chkUser, "click on checkbox");
			isApplicationReady();
			type(EcoSystemPage.txaMsg, message, "enter message");
			click(EcoSystemPage.btnSend, "click on send");
			Thread.sleep(2000);

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	/**
	 * @author :cigniti
	 * @description This function performs scroll operation
	 * 	 
	 * 
	 */

	public void scroll()  throws Throwable {

		try {

			scrollToView(EcoSystemPage.btnClose);

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author :cigniti
	 * @description This function performs verification of text
	 * 	 
	 * 
	 */

	public String getSuccessMsg() throws Throwable {
		String successmsg = getText(EcoSystemPage.verifyText, "successMsg").substring(1).trim();
		return successmsg;
	}
}
