package com.haygroup.web.libs;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.JobMapping;

public class JobMappingLib extends HayGroupCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");


	/**
	 * @author: cigniti
	 * @description This function uploads File
	 * 
	 */

	public void uploadFile() 
	{
		try{

			JSClick(JobMapping.Upload, "Performing click on upload button");
			scrollToView(JobMapping.UploadFile);
			mouseover(JobMapping.UploadFile, "");
			mouseClick(JobMapping.UploadFile, "");
			Waittime();
			sendPath();
			Waittime();
			Waittime();
			scrollToView(JobMapping.NextButton);
			JSClick(JobMapping.NextButton, "performing click on next button ");
			System.out.println("in the end");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}	
	}

	/**
	 * @author: cigniti
	 * @description This function sends the required path  
	 * 
	 */
	public void sendPath()  {

		try{
			Thread.sleep(10000);
			StringSelection sel=new StringSelection(System.getProperty("user.dir")+"\\TestData\\Job_Mapping_BensTestData_6functions.xlsx");
			System.out.println("the result is"+sel.toString());
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel, null);
			Robot r=new Robot();
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_V);
			r.keyRelease(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_V);
			Thread.sleep(10000);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

	/**
	 * @author: cigniti
	 * @description This function maps the functions
	 * 
	 */

	public void MapFunctions()
	{
		checkPageIsReady();
		try {
			JSClick(JobMapping.Mapfunctions,"performing click on Mapfunctions");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function clicks on Manage Functions
	 * 
	 */

	public void clickManageFunctions() throws Throwable
	{

		try{

			List<WebElement> ele=Driver.findElements(JobMapping.maplist);
			System.out.println(ele.size());
			for(int i=0;i<ele.size();i++)
			{
				ele.get(i).click();

				Driver.findElement(By.xpath("//span[contains(.,'Compile analysis of losses and reserves, evaluates product line performance, calculating financial projections and recommends financial structure "
						+ "policies.')]")).click();
			}
			JSClick(JobMapping.SaveChanges, "performing click on save changes");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	/**
	 * @author: cigniti
	 * @description This function defines the industry
	 * 
	 */
	public void DefineIndustry() throws Throwable
	{
		try
		{
			isApplicationReady();
			checkPageIsReady();
			JSClick(JobMapping.DefineIndustry,"performing click Define industries");
			JSClick(JobMapping.ManageSearch,"performing click on industry search");
			JSClick(JobMapping.SelectSearch,"performing click on select search industry");
			JSClick(JobMapping.NextButton, "performing click on next button ");
			JSClick(JobMapping.Guideme, "performing click on Guide me button");
			JSClick(JobMapping.Regionalsales, "Performing click on Regional sales");
			JSClick(JobMapping.Continue, "Performing click on continue button");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	/**
	 * @author: cigniti
	 * @description This function selects the job
	 * 
	 */
	public void selectJob() throws Throwable
	{
		try
		{
			isApplicationReady();
			checkPageIsReady();
			Waittime();
			dragAndDrop(JobMapping.jobManager1,JobMapping.destJobManager1 , "dragging job manager");
			JSClick(JobMapping.typeCareer, "Performing click on type of career button");
			JSClick(JobMapping.optCareer, "Performing click on type of career option");
			System.out.println("in drag n drop");
			Waittime();
			dragAndDrop(JobMapping.RegionalsalesManager2,JobMapping.destRegionalJobManager2 , "dragging job manager");
			JSClick(JobMapping.ContinueButton, "Performing click on continue button");
			JSClick(JobMapping.NextButton_Tooltip, "Performing click on next button of tool tip");
			JSClick(JobMapping.NextButton5_Tooltip, "Performing click on final next button of tool tip");
			JSClick(JobMapping.Careerpath_Radio, "Performing click on final next button of tool tip");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	/**
	 * @author: cigniti
	 * @description This function selects the jobbank
	 * 
	 */

	public void selectJobbank() throws Throwable
	{
		try
		{
			isApplicationReady();
			checkPageIsReady();
			Waittime();
			dragAndDrop(JobMapping.HeadAcc1,JobMapping.dest_HeadAcc1 , "dragging job manager");
			JSClick(JobMapping.typeCareer, "Performing click on type of career button");
			JSClick(JobMapping.optCareer, "Performing click on type of career option");
			Waittime();
			dragAndDrop(JobMapping.coordinator,JobMapping.dest_keyAccadmin2 , "dragging job manager");
			JSClick(JobMapping.ContinueButton, "Performing click on continue button");
			JSClick(JobMapping.NextButton_Tooltip, "Performing click on next button of tool tip");
			JSClick(JobMapping.NextButton5_Tooltip, "Performing click on final next button of tool tip");
			JSClick(JobMapping.Careerpath_Radio, "Performing click on final next button of tool tip");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}


	/**
	 * @author: cigniti
	 * @description This function selects Job bank keyaccount and performs required operations
	 * 
	 */
	public void selectJobbankkeyacc() throws Throwable
	{
		try
		{
			isApplicationReady();
			checkPageIsReady();
			dragAndDrop(JobMapping.keyAccountAdmin1,JobMapping.dest_keyAccountAdmin1 , "dragging job manager");
			JSClick(JobMapping.typeCareer, "Performing click on type of career button");
			JSClick(JobMapping.optCareer, "Performing click on type of career option");
			Waittime();
			dragAndDrop(JobMapping.ManagerSales2,JobMapping.dest_Managersales2 , "dragging job manager");
			JSClick(JobMapping.ContinueButton, "Performing click on continue button");
			JSClick(JobMapping.NextButton_Tooltip, "Performing click on next button of tool tip");
			JSClick(JobMapping.NextButton5_Tooltip, "Performing click on final next button of tool tip");
			JSClick(JobMapping.PeerTestContinue, "Performing click on final next button of tool tip");
			DynamicWait(JobMapping.No_Radio);
			elementVisibleTime(JobMapping.No_Radio);
			Waittime();
			click(JobMapping.No_Radio, "Performing click on no radio button of tool tip");
			dragAndDrop(JobMapping.keyaccountsales1,JobMapping.keyaccountsales2 , "dragging sales support sales");
			DynamicWait(JobMapping.Peers_Next);
			elementVisibleTime(JobMapping.Peers_Next);
			Waittime();
			JSClick(JobMapping.Peers_Next, "Performing click on peers next ");
			DynamicWait(JobMapping.No_Radio);
			elementVisibleTime(JobMapping.No_Radio);
			Waittime();
			click(JobMapping.No_Radio, "Performing click on no radio button of tool tip");
			dragAndDrop(JobMapping.Regionalsales1,JobMapping.Regionalsales2 , "dragging key account sales");
			DynamicWait(JobMapping.Peers_Next);
			elementVisibleTime(JobMapping.Peers_Next);
			Waittime();
			JSClick(JobMapping.Peers_Next, "Performing click on peers next ");
			DynamicWait(JobMapping.No_Radio);
			elementVisibleTime(JobMapping.No_Radio);
			Waittime();
			click(JobMapping.No_Radio, "Performing click on no radio button of tool tip");
			dragAndDrop(JobMapping.keyaccountsales4,JobMapping.keyaccountsales3 , "dragging sales support sales");
			DynamicWait(JobMapping.Peers_Next);
			elementVisibleTime(JobMapping.Peers_Next);
			Waittime();
			JSClick(JobMapping.Peers_Next, "Performing click on peers next ");
			DynamicWait(JobMapping.But_Illtake);
			elementVisibleTime(JobMapping.But_Illtake);
			Waittime();
			JSClick(JobMapping.But_Illtake, "Performing click on i ill take here button ");
			JSClick(JobMapping.Tick1, "Performing click on first tick button ");
			JSClick(JobMapping.Tick2, "Performing click on second tick button ");
			Waittime();
			dragAndDrop(JobMapping.salessupport1,JobMapping.salessupport2 , "dragging sales support");
			dragAndDrop(JobMapping.keyaccountsales4,JobMapping.keyaccountsales5 , "dragging sales support");
			Waittime();
			dragAndDrop(JobMapping.AccManager,JobMapping.destAccManager , "dragging acc manager");
			JSClick(JobMapping.AcctList, "Performing click on type of career button");
			JSClick(JobMapping.optAccList, "Performing click on type of career option");
			JSClick(JobMapping.ArrowCareerpath, "Performing click on Arrow career path");
			JSClick(JobMapping.Addcareerpath, "Performing click on add career path");
			dragAndDrop(JobMapping.AccAnalyst,JobMapping.destAccAnalyst , "dragging acc manager");
			JSClick(JobMapping.DeleteCareerButton, "Performing click on delete career path");
			JSClick(JobMapping.DeleteCareerOption, "Performing click on delete career path");
			JSClick(JobMapping.AssignAnchorbutton, "Performing click on Arrow career path");
			JSClick(JobMapping.SelectAnchor, "Performing click on Arrow career path");	
			JSClick(JobMapping.changeAnchor, "select anchor");
			JSClick(JobMapping.AssignAnchorbutton, "Performing click on Arrow career path");
			JSClick(JobMapping.RenameAcc, "click on rename button");
			type(JobMapping.Renameclick,"Life","Rename the carrer path");	
			JSClick(JobMapping.Clicktick, "click on tick button");
			JSClick(JobMapping.savebutton, "click on save button");
			JSClick(JobMapping.OKbutton, "click on save button");
			JSClick(JobMapping.Button_HayGrades, "Performing click onHaygrads button");
			Waittime();
			JSClick(JobMapping.But_Haycontinue, "Performing click continue  button");
			Waittime();
			JSClick(JobMapping.But_Haycontinue, "Performing click continue  button");
			Waittime();
			click(JobMapping.Radio_AnchorJob, "Performing click on anchorjob radio");
			Waittime();
			JSClick(JobMapping.Next_Anchor, "Performing click continue  button");
			Waittime();
			click(JobMapping.Radio_AnchorJob, "Performing click on anchorjob radio");
			Waittime();
			JSClick(JobMapping.Next_Anchor, "Performing click continue  button");
			Waittime();
			click(JobMapping.Radio_AnchorJob, "Performing click on anchorjob radio");
			Waittime();
			JSClick(JobMapping.Next_Anchor, "Performing click continue  button");
			Waittime();
			JSClick(JobMapping.Button_OK_Anchor, "Performing click continue  button");
			Waittime();
			JSClick(JobMapping.Button_OK_Anchor, "Performing click continue  button");
			Waittime();
			JSClick(JobMapping.ExportDropdown, "Performing click on Export Dropdown ");
			JSClick(JobMapping.ExportButton, "Performing click on Export button");
			Waittime();

		}

		catch(Exception e)
		{
			System.out.println(e);
		}

	}



}
