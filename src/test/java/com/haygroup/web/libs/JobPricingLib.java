package com.haygroup.web.libs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.CreateJobDescriptionPage;
import com.haygroup.web.page.JobPricingPage;

public class JobPricingLib extends HayGroupCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");


	/**
	 * @author: cigniti
	 * @description This function Enters job description
	 * 
	 */
	public void Enterjobdescription(String Jobtext)  {
		try {
			type(JobPricingPage.Jobtext, Jobtext, "Enter job title");
			click(JobPricingPage.Jobselect,"Jobtitle");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}


	/**
	 * @author: cigniti
	 * @description This function Clicks on Workmanager popup
	 * 
	 */
	public void ClickWorkmanagerpopup()  {
		try {
			click(JobPricingPage.Modalheader,"Workmanager popup is dispalyed");
			Thread.sleep(5000);
			click(JobPricingPage.Yesbtn, "Yes button");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}

	}

	/**
	 * @author: cigniti
	 * @description This function Clicks on pricejob
	 * 
	 */
	public void ClickPriceJob (String Grade)  {
		try {
			Waittime();
			isApplicationReady();
			click(JobPricingPage.selectgrade," Selectgrade");
			type(JobPricingPage.search, Grade, "Typed sucessfully");
			click(JobPricingPage.selectdrop, "selected dropvalue");
			JSClick(JobPricingPage.Jobfamily,"Jobfamily");
			JSClick(JobPricingPage.SubJobfamily,"Subfamily selected");
			JSClick(JobPricingPage.Pricejobbtn, "PriceJob button");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}

	}	


	/**
	 * @author: cigniti
	 * @description This function adds the location
	 * 
	 */
	public void AddLoction(String location) 
	{
		try {
			Waittime();
			isApplicationReady();
			JSClick(JobPricingPage.Addlocation, "Addlocaion");
			type(JobPricingPage.selectlocationtxt,location,"Typelocation");
			click(JobPricingPage.Searchtext,"Address");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function removes the added location
	 * 	 
	 *  
	 */
	public void removeLocation(String removeLocation){

		try {
			click(JobPricingPage.removeLocation(removeLocation), "Remove Location");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author: cigniti
	 * @description This function clicks on update
	 * 	 
	 *  
	 */
	public void Clickupdate() 
	{
		try {
			JSClick(JobPricingPage.updatebtn, "Updated the location details");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	/**
	 * @author: cigniti
	 * @description This function clicks on save button
	 * 	 
	 *  
	 */
	public void ClickSave() 
	{
		try {
			JSClick(JobPricingPage.Savescenariobtn, "Saved the job summary details");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * @author: cigniti
	 * @description This function gets Scenario Name
	 * 	 
	 *  
	 */
	String scenarioName;

	public String getScenarioName() {
		return scenarioName;
	}

	/**
	 * @author: cigniti
	 * @description This function sets Scenario Name
	 * 	 
	 *  
	 */
	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}

	/**
	 * @author: cigniti
	 * @description This function Click on Save this price job
	 * 	 
	 *  
	 */
	public void ClickSavethispricejob(String scenarioName) 
	{
		try {
			Waittime();
			isApplicationReady();
			type(JobPricingPage.scenarioname,scenarioName,"Enterscenarioname");		
			click(JobPricingPage.city, "Select city name");
			click(JobPricingPage.Employeename,"Select the employeename");
			setScenarioName(scenarioName);
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}

	}

	/**
	 * @author: cigniti
	 * @description This function Click on pricejobSave
	 * 	 
	 *  
	 */
	public void ClickpricejobSave() 
	{
		try {
			Waittime();

			JSClick(JobPricingPage.Savebtn, "Savebutton");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}


	}
	/**
	 * @author: cigniti
	 * @description This function Click on close
	 * 	 
	 *  
	 */

	public void clickClose() 
	{
		try {
			Waittime();
			JSClick(JobPricingPage.closeButton, "Close button");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	String salSimulator;

	/**
	 * @author: cigniti
	 * @description This function gets Salary Simulator
	 * 	 
	 *  
	 */
	public String getSalSimulator() {
		return salSimulator;
	}

	/**
	 * @author: cigniti
	 * @description This function Click on set Salary Simulator
	 * 	 
	 *  
	 */
	public void setSalSimulator(String salSimulator) {
		this.salSimulator = salSimulator;
	}

	/**
	 * @author: cigniti
	 * @description This function Click on Salarysimulator
	 * 	 
	 *  
	 */
	public void ClickSalarysimulator(String salscenarioname,String EmpName ) 
	{
		try {
			isApplicationReady();
			JSClick(JobPricingPage.Salarysimulator, "Salary Simulatir");
			type(JobPricingPage.TotalRemuneration, "60000" , "Enter TotalRemuneration");
			type(JobPricingPage.TotalCashatTarget, "61000" , "Enter TotalCashatTarget");
			type(JobPricingPage.BaseSalary, "62000" , "Enter BaseSalary");
			JSClick(JobPricingPage.Calculatebutton, "Calcualtion");
			Waittime();
			JSClick(JobPricingPage.popupsavebutton, "Save button");
			type(JobPricingPage.salaryscenarioname, salscenarioname, "Enter scenario name");
			setSalSimulator(salscenarioname);
			click(JobPricingPage.Enteremployeename,"EmployeeName");
			type(JobPricingPage.searchemployeename, EmpName, "Searchemployeename");
			JSClick(JobPricingPage.salarysavebutton,"Save button");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}

	}
	/**
	 * @author: cigniti
	 * @description This function Click on home
	 * 	 
	 *  
	 */
	public void Clickhome() 
	{
		try {
			isApplicationReady();
			JSClick(JobPricingPage.Home, "Homeicon");
			JSClick(JobPricingPage.MyPeople,"MyPeoplemodule");
			JSClick(JobPricingPage.MyPeopleEmployeename, "Employeename");
			Waittime();
			JavascriptExecutor jse = (JavascriptExecutor)Driver;
			jse.executeScript("window.scrollBy(0,250)", "");
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}


	}
	/**
	 * @author: cigniti
	 * @description This function verifies the scenario name 	 
	 *  
	 */

	public void verifyScenarioName(String scenarioName) throws InterruptedException{
		int matchCounter =0;
		List<WebElement> scenarioNames = Driver.findElements(JobPricingPage.lblScenarioNames);
		Thread.sleep(4000);
		for(int i=0;i<scenarioNames.size();i++){

			if(scenarioNames.get(i).getText().contains(scenarioName)){
				scenarioNames.get(i).click();
				matchCounter++;				

			}		
		}
		System.out.println("Match counter after loop"+matchCounter);
		if(matchCounter>=1){
			extentTestLogger.pass("Successfully verified Scenario name in the home page");
		}
		else{
			extentTestLogger.fail("Failed to find scenario name in home page");
		}
	}
	/**
	 * @author: cigniti
	 * @description This function verifiessalarySimulation	 
	 *  
	 */


	public void verifysalarySimulation(String ScenarioName1){
		int matchCounter =0;
		List<WebElement> salscenarioNames = Driver.findElements(JobPricingPage.lblsalarysimulation);
		for(int i=0;i<salscenarioNames.size();i++){
			if(salscenarioNames.get(i).getText().contains(ScenarioName1)){
				matchCounter++;
			}		
		}
		if(matchCounter>=1){
			extentTestLogger.pass("Successfully verified salary Scenario name in the home page");
		}
		else{
			extentTestLogger.fail("Failed to find salary scenario name in home page");
		}
	}

}






