package com.haygroup.web.libs;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.DashboardPage;

public class DashboardLib extends HayGroupCommonLib {

	/**
	 * @author: cigniti
	 * @description This function performs the Select Company functionality
	 * 
	 */
	public void selectCompany() {
		try {
			click(DashboardPage.btnSelectCompany, "Select A Company Button");
			waitForVisibilityOfElement(DashboardPage.headerSelectAccount,
					"Account Header", 20);
			click(DashboardPage.ddlSelectACompany, "Select A Company DDL");
			click(DashboardPage.selectAccount, "Select Account");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This method will navigate to the People Tab
	 * 
	 */

	public void navigateToPeopleTab(String tabName) {
		checkPageIsReady();
		try {
			Waittime();
			JSClick(DashboardPage.dashBoardTabs(tabName), "Click on " + tabName
					+ " Tab");
			waitForVisibilityOfElement(DashboardPage.headerPeopleAdmin,
					"People Admin Header", 20);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	/**
	 * @author: cigniti
	 * @description This function navigates to the Styles and climate tab and
	 *              takes the tabName as the parameter
	 * 
	 */
	public void navigateToStylesAndClimateTab(String tabName) {
		checkPageIsReady();
		try {
			JSClick(DashboardPage.dashBoardTabs(tabName), "Click on " + tabName
					+ " Tab");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author: cigniti
	 * @description This function navigates to the Journey tab and takes the
	 *              tabName as the parameter
	 * 
	 */
	public void navigateToJourneyeTab(String tabName) {
		checkPageIsReady();
		try {
			JSClick(DashboardPage.dashBoardTabs(tabName), "Click on " + tabName
					+ " Tab");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author :cigniti
	 * @description This function navigates to the Job Description tab and takes
	 *              the tabName as the parameter
	 * 
	 */
	public void navigateToJobDescriptionTab(String tabName) {
		checkPageIsReady();
		try {
			waitTillElementToBeClickble(DashboardPage.dashBoardTabs(tabName),
					tabName + " Tab");
			JSClick(DashboardPage.dashBoardTabs(tabName), tabName + " Tab");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	/**
	 * @author :cigniti
	 * @description this function does the create user details functionality on
	 *              the application by taking the below parameters
	 *
	 */
	public String createUserDetails(String firstName, String lastName,
			String email) {
		String userFullName = "";
		try {
			checkPageIsReady();
			isApplicationReady();
			Thread.sleep(2000);
			Waittime();
			click(DashboardPage.ddl_SelectAnAction, "Select An Action Dropdown");
			click(DashboardPage.addPeopleOptions, "Select Manual Add option");
			waitForVisibilityOfElement(DashboardPage.headerManualAdd,
					"Manual Add header", 20);
			type(DashboardPage.userFirstName, firstName, "User First Name");
			type(DashboardPage.userLastName, lastName, "User First Name");
			type(DashboardPage.emailAddress, email, "User First Name");
			waitTillElementToBeClickble(DashboardPage.btn_Save_userDetails,
					"btn_Save_userDetails");
			click(DashboardPage.btn_Save_userDetails,
					"User details Save button");
			waitTillElementToBeClickble(DashboardPage.ddl_SelectAnAction,
					"Select An Action Dropdown");
			userFullName = firstName + " " + lastName;
			Thread.sleep(3000);

		} catch (Exception e) {
			// TODO: handle exception
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userFullName;

	}

	/**
	 * @author :cigniti
	 * @description this function iterates through all list of users on the dash
	 *              board and verifies the existence of the created user on the
	 *              users list
	 * 
	 */
	public By verifyUserDetails(String userFullName) {
		WebElement element = null;
		By userNameXpath = null;
		By generatedUserXpath = null;
		checkPageIsReady();
		try {

			int ElementsSize = Driver.findElements(
					By.xpath(".//*[@id='people-table']/tbody/tr/td/a")).size();

			for (int i = 1; i <= ElementsSize; i++) {
				userNameXpath = By.xpath(".//*[@id='people-table']/tbody/tr["
						+ i + "]/td/a");
				element = Driver.findElement(userNameXpath);
				String text = element.getText();
				if (text.equalsIgnoreCase(userFullName)) {
					generatedUserXpath = By
							.xpath(".//*[@id='people-table']/tbody/tr[" + i
									+ "]/td/a/parent::td/input");
					try {
						JSClick(generatedUserXpath, "Selected User");
					} catch (Throwable e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


					break;
				}
			}
		} catch (StaleElementReferenceException e) {
			// TODO: handle exception
			verifyUserDetails(userFullName);
		}

		return userNameXpath;

	}

	/**
	 * @author :cigniti
	 * @description This function takes the locator as an argument and selects
	 *              the particular user on the grid
	 * 
	 */
	public void selectUser(By userNameXpath) {

		try {
			waitForVisibilityOfElement(userNameXpath, "App Settings button", 30);
			JSClick(userNameXpath, "App Settings button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author :cigniti
	 * @description This function performs the Edit operation on the application
	 *
	 * 
	 */
	public void editUserDetails(String userFullName, String timeZone) {
		try {
			checkPageIsReady();
			try {
				waitForVisibilityOfElement(DashboardPage.btn_UserEdit,
						"User Edit Button", 30);
				JSClick(DashboardPage.btn_UserEdit, "User Edit Button");
				waitForVisibilityOfElement(DashboardPage.ddl_TimeZone,
						"Select Time Zone", 30);
				waitTillElementToBeClickble(DashboardPage.ddl_TimeZone, "Select Time Zone");
				click(DashboardPage.ddl_TimeZone, "Select Time Zone");
				waitTillElementToBeClickble(DashboardPage.headerProfile,
						"Profile Header");
				click(DashboardPage.text_TimeZone, "Select Time Zone");

				JSClick(DashboardPage.btn_Done, "Done Button");
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			checkPageIsReady();
		} catch (Exception e) {

		}
	}

	/**
	 * @author :cigniti
	 * @description This function performs the App Settings selection process on
	 *              the application
	 * 
	 */
	public void selectAppSettings() {
		checkPageIsReady();
		try {
			JSClick(DashboardPage.btn_AppSettings, "App Settings button");
			waitForVisibilityOfElement(DashboardPage.headerAppSettings,
					"App Settings Page", 30);
			Waittime();
			click(DashboardPage.radioBtnOn_StyleAndClimate,
					"StyleAndClimate Button");
			click(DashboardPage.radioBtnOn_Journey, "Journey Button");
			click(DashboardPage.journeyDateTextBox, "Click on Journey");
			waitTillElementToBeClickble(DashboardPage.todayDate, "Today's date");
			click(DashboardPage.todayDate, "Today's date");
			click(DashboardPage.radioBtnOn_JobDescription,
					"Click on Job Description");
			click(DashboardPage.radioBtnOn_JobMapping, "Click on Job Mapping");
			click(DashboardPage.btn_ApplySettings,
					"Click on App Settings button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author :cigniti
	 * @description This perform the Admin changes for all tabs, this takes User
	 *              Name as the parameter
	 *
	 */
	public void verifyAdminChanges(String userFullName) {
		checkPageIsReady();
		try {
			waitForVisibilityOfElement(DashboardPage.btn_Admin_Add,
					"Admin Add Button", 30);
			JSClick(DashboardPage.btn_Admin_Add, "Admin Add Button");
			waitForVisibilityOfElement(DashboardPage.headerSelectEmployee,
					"Select Employee Header", 30);
			type(DashboardPage.txt_EmployeeName, userFullName,
					"Enter User name in text box");
			click(DashboardPage.selectEmployee, "select Employee Button");
			click(DashboardPage.btn_Submit, "Submit Button");
			checkPageIsReady();
			By closeAdminBtn = By.xpath("//h3[contains(text(),'" + userFullName
					+ "')]/following-sibling::a/img");
			waitTillElementToBeClickble(closeAdminBtn, "Close Button ");
			JSClick(closeAdminBtn, "Close Admin Button");
			waitForVisibilityOfElement(DashboardPage.headerConfirmation,
					"Confirmation Header", 30);
			Waittime();
			click(DashboardPage.btn_Yes, "Yes Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		checkPageIsReady();
	}

	/**
	 * @author :cigniti
	 * @description This perform the click operation to download usage report
	 *             
	 *
	 */

	public void clickCompanyDownloadUsageReprot() {
		String browserName = getBrowser();
		try {
			checkPageIsReady();
			Waittime();
			waitTillElementToBeClickble(DashboardPage.companyDownloadReport,
					"Company");
			JSClick(DashboardPage.companyDownloadReport,
					"Company Download Usage Report Button");
			checkPageIsReady();
			if (browserName.equalsIgnoreCase("firefox")) {
				checkPageIsReady();
				Thread.sleep(20000);
				checkPageIsReady();
			}

			else if (browserName.equalsIgnoreCase("ie")) {
				pressTab();
			}

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author :cigniti
	 * @description This perform the click operation to download StylesAndClimateusage report
	 *             
	 *
	 */

	public void clickStylesAndClimateUsageReprot() {
		String browserName = getBrowser();
		try {
			Waittime();
			waitTillElementToBeClickble(
					DashboardPage.stylesAndClimateDownloadReport, "styles");
			JSClick(DashboardPage.stylesAndClimateDownloadReport,
					"Styles & Climate Download Usage Report Button");
			checkPageIsReady();
			if (browserName.equalsIgnoreCase("firefox")) {
				checkPageIsReady();
				Thread.sleep(20000);
				checkPageIsReady();
			}

			else if (browserName.equalsIgnoreCase("ie")) {
				pressTab();
			}

		}

		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author :cigniti
	 * @description This perform the click operation to download JourneyDownloadHr report
	 *             
	 *
	 */

	public void clickJourneyDownloadHrReprot() {
		String browserName = getBrowser();
		try {
			Waittime();
			waitTillElementToBeClickble(DashboardPage.journeyDownloadHrReport,
					"styles");
			JSClick(DashboardPage.journeyDownloadHrReport,
					"Journey Download Hr Report Button");
			checkPageIsReady();
			if (browserName.equalsIgnoreCase("firefox")) {
				checkPageIsReady();
				Thread.sleep(30000);
				checkPageIsReady();
			}

			else if (browserName.equalsIgnoreCase("ie")) {
				pressTab();
			}

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author :cigniti
	 * @description This function performs excel write operation by taking the
	 *              below parameters
	 * 
	 *	 
	 */
	public void sendUserToExcel(String SheetName, String ColumnName,
			int rowNumber, String Data) throws Throwable {
		TestData.setCellData(SheetName, ColumnName, rowNumber, Data);

	}

	/**
	 * @author :cigniti
	 * @description This function performs click operation on privacy policy pop up
	 *             
	 * 
	 *	 
	 */

	public void clickPrivacyPolicy() throws Throwable {


		isApplicationReady();
		JSClick(DashboardPage.chkboxPrivacyPolicy,
				"select Privacy Policy Checkbox");
		waitForVisibilityOfElement(DashboardPage.chkboxPrivacyPolicy,
				"Successfully changed", IFrameworkConstant.WAIT_TIME_MEDIUM);


	}

	/**
	 * @author :cigniti
	 * @description This function performs click operationj on accept and continue button
	 *              
	 * 
	 *	 
	 */
	public void clickAcceptAndContinue() throws Throwable {

		isApplicationReady();
		JSClick(DashboardPage.btn_AcceptandContinue,
				"Click Accept and Continue button");
		waitForVisibilityOfElement(DashboardPage.btn_AcceptandContinue,
				"Successfully changed", IFrameworkConstant.WAIT_TIME_MEDIUM);

	}

	/**
	 * @author: cigniti
	 * @description This function navigates to the home page
	 *               
	 * 
	 */

	public void navigateToHomePage() throws Throwable {
		Driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		isApplicationReady();
		waitTillElementToBeClickble(DashboardPage.homeicon,
				"Click on Home page icon");

		JSClick(DashboardPage.homeicon, "Click on Home page icon");

	}

	/**
	 * @author: cigniti
	 * @description This function verifies the navigation to particular page 
	 *              
	 * 
	 */
	public void verifyNavigation(String URLcontent) throws Throwable {
		checkPageIsReady();

		Thread.sleep(15000);
		String actURL = getCurrentURL();
		System.out.println("Actual URL Is ***** " + actURL);
		System.out.println("Expected URL Is ***** " + URLcontent);

		if (actURL.contains(URLcontent)) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

	}

	/**
	 * @author: cigniti
	 * @description This function clicks on pop up
	 *              
	 *              	 
	 */
	public void clickPopup() throws Throwable {

		isApplicationReady();
		JSClick(DashboardPage.btn_PopupYes, "Click Yes on popup");

	}

}
