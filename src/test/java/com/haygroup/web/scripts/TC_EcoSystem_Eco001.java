package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.EcoSystemLib;

public class TC_EcoSystem_Eco001 extends EcoSystemLib{


	@DataProvider
	public Object[][] getTestDataFor_languagechange() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_languagechange")
	public void EcoSystemlanguagechange(Hashtable<String, String> data) {
		try {
			String userID = "";
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.languagechange");		

				/*launchAppURL*/

				launchApplication("EcoSystem",data.get("AppURL"));

				/* Login to the application */
				if (this.testParameters.browser.equals("chrome")){
					userID = data.get("UserNameChrome");					
				}
				
				else if (this.testParameters.browser.equals("firefox")){
					userID = data.get("UserNameFirefox");					
				}
				
				else if (this.testParameters.browser.equals("ie")){
					userID = data.get("UserNameIe");	
				}
				
				loginToApplication(userID, data.get("Password"));

				/*click on settings*/
				settingspagebutton();

			/*	click on settings*/
				languagechange(data.get("Language1"));

				checkPageIsReady();

			/*	 Login to the application */
				
				if (this.testParameters.browser.equals("chrome")){
					userID = data.get("UserNameChrome");					
				}
				
				else if (this.testParameters.browser.equals("firefox")){
					userID = data.get("UserNameFirefox");					
				}
				
				else if (this.testParameters.browser.equals("ie")){
					userID = data.get("UserNameIe");	
				}
				loginToHaygroupApplication(userID, data.get("Password"));

				
				/*click on settings*/
				settingspagebutton();
				
				languagechangecheck("verifytext");
				
				verifylanguageChange(data.get("Language1"));

				/*click on settings*/
				languagechange(data.get("Language2"));

				checkPageIsReady();


			}									
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {

		//Logout from the application 
	

		
	}
}
