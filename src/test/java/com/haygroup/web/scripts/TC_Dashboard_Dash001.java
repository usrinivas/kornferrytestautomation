package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;











import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.DashboardLib;
import com.haygroup.web.libs.JobDescriptionLib;
import com.haygroup.web.page.CreateJobDescriptionPage;
import com.haygroup.web.page.DashboardPage;
import com.haygroup.web.page.HomePage;

/*This Test is To test Dashboard functionality*/
public class TC_Dashboard_Dash001 extends DashboardLib {
	public String userFullName = "";
	public String email = "";
	public String Environment=null;
	@DataProvider
	public Object[][] getTestDataFor_verifyDashboard() {
		//----------------for jenking global environment variables	
		Environment = System.getenv("Environment");
		System.out.println("the environmental variable from jernkins is "+Environment);
//-------------------------------------------------------------
		return TestUtil.getData("VerifyDashboard", TestData, "Dashboard");
	}

	@Test(dataProvider = "getTestDataFor_verifyDashboard", priority=1)
	public void verifyDashboardFunctionality(Hashtable<String, String> data) {
		try {String Env=null;
		Environment = System.getenv("Environment");
		System.out.println("the environmental variable from jernkins is "+Environment);
		if(Environment!=null){
			String Appurl=launchURL(data);
			launchApplication("Dashboard",Appurl);
		}
		else{
			//launchApplication("Dashboard",data.get("AppURL"));
			launchApplication("Dashboard",data.get("StagingURL").trim());
		}
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Verify Dashboard");	

				
					
				/* Login to the application */
				//launchApplication("Dashboard",Appurl);
				loginToApplication(data.get("UserName"), data.get("Password"));

				assertTextMatching(HomePage.selectregion(data.get("SelectRegion")), data.get("SelectRegion"),						
						"Employee Record Details Header");				
				//Select Region as Job Description 
				selectRegion(data.get("SelectRegion"));
				navigateToPeopleTab(data.get("NavigationTabs_Dashboard"));
				// create user
				email = data.get("Email")+generateRandomNumber()+"@gmail.com";
				userFullName= createUserDetails(data.get("FirstName"), data.get("LastName")+generateRandomNumber(),email);
				//verify user is created successfully
				verifyUserDetails(userFullName);
				//select App Settings for the user
				selectAppSettings();
				// verify user name in the list
				By userNameXpath =  verifyUserDetails(userFullName);
				// select the user 
				selectUser(userNameXpath);
				// edit the user details
				editUserDetails(userFullName,data.get("TimeZone"));
				//navigate to Styles and Climate Tab
				navigateToStylesAndClimateTab(data.get("StylesAndClimateTab"));
				// verify the Admin changes for Styles and climate tab
				verifyAdminChanges(userFullName);
				//navigate to Journey tab
				navigateToJourneyeTab(data.get("JourneyTab"));
				// verify admin changes for the Journey Tab
				verifyAdminChanges(userFullName);
				//navigate to Job Description Tab
				navigateToJobDescriptionTab(data.get("JDTab"));
				// verify admin changes for the Job Description Tab
				verifyAdminChanges(userFullName);
				//send user data to excel sheet

			}
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		/* Logout from the application */
		logOutFromApplication();

	}
}
