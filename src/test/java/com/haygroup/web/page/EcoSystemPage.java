package com.haygroup.web.page;

import org.openqa.selenium.By;

public class EcoSystemPage {

	public static By btnSettings;
	public static By ddnLanguageChange;
	public static By btnContact;
	public static By chkUser;
	public static By txaMsg;
	public static By btnSend;
	public static By verifyText;
	public static By ddnLocation;
	public static By ddnCurrency;
	public static By ddnTimezone;
	public static By radSendEmailnotification;
	public static By ddnWeekstartingday;
	public static By ddnWeekendday;
	public static By ddnTeammeetingday;
	public static By ddnTeammeetingtime;
	public static By radDoNotSendEmailnotification;
	public static By radDailybasisMyplannotification;
	public static By radWeekbasisMyplannotification;
	public static By languageIcon;
	public static By btnClose;
	public static By txtMyprofile;
	public static By txtMailnotification;
	public static By verifylanguageChange;
	public static By ddnLocationText;
	public static By ddnLocationselect;
	public static By ddnLocationselect2;
	
	static {

		btnSettings=By.xpath(".//*[@id='header-region']/div/nav/div/div/div[3]/button[2]");
		ddnLanguageChange=By.cssSelector(".language-select>div>div>select");
		btnContact = By.id("admin-contact");
		chkUser = By.cssSelector("#contact-admin-modal-body>ul>div:nth-child(1) input");
		txaMsg = By.id("info");
		btnSend = By.xpath("//button[text()='Send']");
		verifyText = By.xpath("//div[@id='bootstrap-alert-admin-contact']");
		verifylanguageChange=By.xpath("//button[@class='btn btn-primary pull-right']");
		ddnLocation = By.cssSelector(".select2-container");
		ddnCurrency = By.cssSelector(".currency-select>div>div>select");
		ddnTimezone=By.cssSelector(".timezone-select>div>div>select");
		txtMailnotification=By.xpath("//h3[@class='title' and text()='Email Notification Preferences']");
		radSendEmailnotification=By.cssSelector("#notification-preferences-region>div>ul>li:nth-child(1)>label>input");
		radDoNotSendEmailnotification=By.cssSelector("#notification-preferences-region>div>ul>li:nth-child(2)>label>input");
		radDailybasisMyplannotification=By.cssSelector("#my-plan-region>div>div>:nth-child(1)>ul>:nth-child(1)>label>input");
		radWeekbasisMyplannotification=By.cssSelector("#my-plan-region>div>div>:nth-child(1)>ul>:nth-child(2)>label>input");
		ddnWeekstartingday=By.cssSelector(".js-week-start-region>div>div>select");
		ddnWeekendday=By.cssSelector(".js-week-end-region>div>div>select");
		ddnTeammeetingday=By.cssSelector(".js-team-meeting-day-region>div>div>select");
		ddnTeammeetingtime=By.cssSelector(".js-team-meeting-time-region>div>div>select");
		btnClose=By.xpath("//button[@class='close js-close-modal']");
		txtMyprofile=By.cssSelector("#main-region>div>h3:nth-child(1)");
		ddnLocationText=By.cssSelector("#select2-drop>:nth-child(1) input");
		ddnLocationselect=By.cssSelector("#select2-drop>:nth-child(2)>:nth-child(1)");
		ddnLocationselect2=By.cssSelector("#select2-drop>:nth-child(2)>:nth-child(3)");
			
	}
	public static By verifySuccessMessage(int msgCounter){

		By successElement = By.xpath("(//div[contains(text(),'Successfully Changed')])["+msgCounter+"]");
		return successElement;

	}
	public static By inputLanguage(String language){

		return languageIcon = By.name(language);
	}

}
