package com.gallop.support;

public class GallopEnumerators {

	public GallopEnumerators() {
		
	}
	
	public final class OS {

	    public static final String WINDOWS = "windows";
	    public static final String IOS = "ios";
	    public static final String ANDROID = "android";

	    private OS() { }
	}

}
