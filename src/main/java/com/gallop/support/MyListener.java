package com.gallop.support;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.Logs;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.WebDriverEventListener;

import com.gallop.accelerators.ActionEngine;
import com.gallop.accelerators.TestEngine;
import com.gallop.support.IFrameworkConstant;

/**
 * Class Deprecated and implemented the same in TestEngineWeb class to log Browser logs 
 * @deprecated 
 * @author 
 *
 */
@Deprecated
public class MyListener extends AbstractWebDriverEventListener { 
//extends TestEngineWeb implements WebDriverEventListener{ 

	
	@Override
	public void beforeFindBy(By by, WebElement element, WebDriver driver){
		
		WebElement elem = driver.findElement(by);
		// draw a border around the found element
		if (driver instanceof JavascriptExecutor) {
		   ((JavascriptExecutor)driver).executeScript("arguments[0].style.border='4px solid yellow'", elem);
		   //((JavascriptExecutor)driver).executeScript("arguments[0].setAttribute('style', arguments[1]);", elem, "color: yellow; border: 2px solid yellow;"); 
		}
	}

	@Override
	public void beforeNavigateTo(String url, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterNavigateTo(String url, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNavigateBack(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterNavigateBack(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNavigateForward(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterNavigateForward(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNavigateRefresh(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterNavigateRefresh(WebDriver driver) {
		// TODO Auto-generated method stub
		try{
			//captureBrowserConsoleLogs(driver);	
		}catch(Throwable t)
		{
			System.out.println(t.getMessage());
		}		
	}

	@Override
	public void afterFindBy(By by, WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeClickOn(WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterClickOn(WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		try{
			//captureBrowserConsoleLogs(driver);	
		}catch(Throwable t)
		{
			System.out.println(t.getMessage());
		}
				
	}

	@Override
	public void beforeChangeValueOf(WebElement element, WebDriver driver,
			CharSequence[] keysToSend) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterChangeValueOf(WebElement element, WebDriver driver,
			CharSequence[] keysToSend) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeScript(String script, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterScript(String script, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onException(Throwable throwable, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}
/*	
	public boolean captureBrowserConsoleLogs(WebDriver driver) throws Throwable
			{
		if (IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOGS) {
			String consoleErrors = "";
			int currLineNo = 0;
			Logs logs = driver.manage().logs();

			LogEntries logEntries = logs.get(LogType.BROWSER);  //Use LogType.DRIVER to capture all Selenium WebDriver console logs which will be useful for script developers 
			Writer writer = null;
			try {
				writer = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOG_FILEPATH), "utf-8"));
				for (LogEntry logEntry : logEntries) {
					if (currLineNo >= intLoggerCurrLogLineNo) {
						consoleErrors = consoleErrors + currLineNo + "::" + logEntry.getMessage() + "\r\n";
						writer.append(consoleErrors);
						System.out.println(consoleErrors);
					}
					currLineNo++;
				}

			} catch (IOException ex) {
				// report
			} finally {
				try {
					writer.close();
				} catch (Exception ex) { //ignore 
				}
				intLoggerCurrLogLineNo = currLineNo;
				if(consoleErrors!=""){
					if(IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOG_ONERROR_FAIL){
						reporter.failureReport("BROWSER CONSOLE ERRORS FOUND", consoleErrors , Driver);						
					}
					else if(IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOG_ONERROR_WARN)
					{
						reporter.warningReport("BROWSER CONSOLE ERRORS FOUND", consoleErrors);
					}					
				}
			}
		}
		return true;
	}
*/
}

